package ee.taltech.monopoly.gamelogic.cards;

import ee.taltech.monopoly.gamelogic.Board;
import ee.taltech.monopoly.gamelogic.Player;
import ee.taltech.monopoly.gamelogic.StaticStringKeeper;
import ee.taltech.monopoly.gamelogic.street.purchasable.PurchasableStreet;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * Store community chest and chance cards and implement drawing a card.
 */
public class CardDeck {
    /*
     * List of all question cards
     */
    private final List<Card> communityChest = Arrays.asList(
            new Card(Questions.QUESTION1, Arrays.asList(Questions.Q1_ANSWER1, Questions.Q1_ANSWER2, Questions.Q1_ANSWER3, Questions.Q1_CORRECT_ANSWER), Questions.Q1_CORRECT_ANSWER),
            new Card(Questions.QUESTION2, Arrays.asList(Questions.Q2_ANSWER1, Questions.Q2_ANSWER2, Questions.Q2_ANSWER3, Questions.Q2_CORRECT_ANSWER), Questions.Q2_CORRECT_ANSWER),
            new Card(Questions.QUESTION3, Arrays.asList(Questions.Q3_ANSWER1, Questions.Q3_ANSWER2, Questions.Q3_ANSWER3, Questions.Q3_CORRECT_ANSWER), Questions.Q3_CORRECT_ANSWER),
            new Card(Questions.QUESTION4, Arrays.asList(Questions.Q4_ANSWER1, Questions.Q4_ANSWER2, Questions.Q4_ANSWER3, Questions.Q4_CORRECT_ANSWER), Questions.Q4_CORRECT_ANSWER),
            new Card(Questions.QUESTION5, Arrays.asList(Questions.Q5_ANSWER1, Questions.Q5_ANSWER2, Questions.Q5_ANSWER3, Questions.Q5_CORRECT_ANSWER), Questions.Q5_CORRECT_ANSWER),
            new Card(Questions.QUESTION6, Arrays.asList(Questions.Q6_ANSWER1, Questions.Q6_ANSWER2, Questions.Q6_ANSWER3, Questions.Q6_CORRECT_ANSWER), Questions.Q6_CORRECT_ANSWER),
            new Card(Questions.QUESTION7, Arrays.asList(Questions.Q7_ANSWER1, Questions.Q7_ANSWER2, Questions.Q7_ANSWER3, Questions.Q7_CORRECT_ANSWER), Questions.Q7_CORRECT_ANSWER),
            new Card(Questions.QUESTION8, Arrays.asList(Questions.Q8_ANSWER1, Questions.Q8_ANSWER2, Questions.Q8_ANSWER3, Questions.Q8_CORRECT_ANSWER), Questions.Q8_CORRECT_ANSWER),
            new Card(Questions.QUESTION9, Arrays.asList(Questions.Q9_ANSWER1, Questions.Q9_ANSWER2, Questions.Q9_ANSWER3, Questions.Q9_CORRECT_ANSWER), Questions.Q9_CORRECT_ANSWER),
            new Card(Questions.QUESTION10, Arrays.asList(Questions.Q10_ANSWER1, Questions.Q10_ANSWER2, Questions.Q10_ANSWER3, Questions.Q10_CORRECT_ANSWER), Questions.Q10_CORRECT_ANSWER),
            new Card(Questions.QUESTION11, Arrays.asList(Questions.Q11_ANSWER1, Questions.Q11_ANSWER2, Questions.Q11_ANSWER3, Questions.Q11_CORRECT_ANSWER), Questions.Q11_CORRECT_ANSWER),
            new Card(Questions.QUESTION12, Arrays.asList(Questions.Q12_ANSWER1, Questions.Q12_ANSWER2, Questions.Q12_ANSWER3, Questions.Q12_CORRECT_ANSWER), Questions.Q12_CORRECT_ANSWER),
            new Card(Questions.QUESTION13, Arrays.asList(Questions.Q13_ANSWER1, Questions.Q13_ANSWER2, Questions.Q13_ANSWER3, Questions.Q13_CORRECT_ANSWER), Questions.Q13_CORRECT_ANSWER),
            new Card(Questions.QUESTION14, Arrays.asList(Questions.Q14_ANSWER1, Questions.Q14_ANSWER2, Questions.Q14_ANSWER3, Questions.Q14_CORRECT_ANSWER), Questions.Q14_CORRECT_ANSWER),
            new Card(Questions.QUESTION15, Arrays.asList(Questions.Q15_ANSWER1, Questions.Q15_ANSWER2, Questions.Q15_ANSWER3, Questions.Q15_CORRECT_ANSWER), Questions.Q15_CORRECT_ANSWER)
    );
    private final List<Reward> communityChestRewards = Arrays.asList(Reward.GO_TO_GO,
            Reward.COLLECT_X_EAP,
            Reward.GET_X_EAP_FROM_EVERY_PLAYER);
    private final List<Punishment> communityChestPunish = Arrays.asList(Punishment.GO_TO_JAIL,
            Punishment.PAY_X_EAP);
    private final List<Reward> chanceRewards = Arrays.asList(Reward.GO_TO_GO,
            Reward.GO_TO_X_STREET,
            Reward.GO_TO_NEXT_UTILITY,
            Reward.GO_TO_NEXT_RAILROAD,
            Reward.COLLECT_X_EAP);
    private final List<Punishment> chancePunish = Arrays.asList(Punishment.GO_TO_JAIL,
            Punishment.PAY_X_EAP,
            Punishment.GO_BACK_THREE_TIMES,
            Punishment.PAY_EVERY_PLAYER_X_EAP);
    private Card currentCard;
    private Reward reward;
    private Punishment punish;
    private int randomCardForChance;

    /*
     * Set current card.
     */
    public void setCurrentCard(Card currentCard) {
        this.currentCard = currentCard;
    }

    /*
     * Set current reward.
     */
    public void setReward(Reward reward) {
        this.reward = reward;
    }

    /*
     * Set current punishment.
     */
    public void setPunish(Punishment punish) {
        this.punish = punish;
    }

    /*
     * Keeps street type as enum.
     */
    public enum Type {
        CHANCE, COMMUNITY
    }

    /*
     * Keeps punishments as enum.
     */
    public enum Punishment {
        GO_TO_JAIL, PAY_X_EAP, GO_BACK_THREE_TIMES, PAY_EVERY_PLAYER_X_EAP, TEST_ONLY
    }

    /*
     * Keeps rewards as enum.
     */
    public enum Reward {
        GO_TO_GO, COLLECT_X_EAP, GET_X_EAP_FROM_EVERY_PLAYER, GO_TO_X_STREET,
        GO_TO_NEXT_UTILITY, GO_TO_NEXT_RAILROAD, TEST_ONLY
    }

    /*
     * Sets random values for rewards and punishments.
     * Also randomCardForChance for Type.CHANCE to determine if the player receives a reward or punishment.
     */
    public void setStuff(Type type) {
        if (type.equals(Type.CHANCE)) {
            randomCardForChance = getRandomInt(Arrays.asList(1, 2, 3, 4));
            reward = getRandomItemRew(chanceRewards);
            punish = getRandomItemPun(chancePunish);
        } else if (type.equals(Type.COMMUNITY)) {
            currentCard = getRandomCard(communityChest);
            reward = getRandomItemRew(communityChestRewards);
            punish = getRandomItemPun(communityChestPunish);
        }
    }

    /*
     * Return random Card from card list.
     */
    public Card drawCommunityCard() {
        setStuff(Type.COMMUNITY);
        return currentCard;
    }

    /*
     * Calls quePunish or queReward based on random 50/50 chance determined in the setStuff.
     */
    public String drawChanceCard(Player player, Board board) {
        setStuff(Type.CHANCE);
        if (randomCardForChance % 2 == 0) {
            return queReward(reward, player, board);
        } else {
            return quePunish(punish, player, board);
        }
    }

    /**
     * Return queReward or quePunish depending on given boolean.
     *
     * @param player player who receives the punishment or reward.
     * @param board playing board, needed for technical reason.
     * @param isCorrect boolean that determines return.
     * @return String, informing the player of what has happened.
     */
    public String communityResult(Player player, Board board, boolean isCorrect) {
        if (isCorrect) {
            return queReward(reward, player, board);
        } else {
            return quePunish(punish, player, board);
        }

    }

    /**
     * Depending on the reward argument, a player receives certain beneficial bonuses,
     * e.g gains money or something.
     *
     * @param reward argument that sets the received reward.
     * @param player player who receives the reward.
     * @param board board argument is need for some technical reason.
     * @return String, informing the player of what has happened.
     */
    public String queReward(Reward reward, Player player, Board board) {
        int currentLoc = player.getLocation(board).getIndex();
        switch (reward) {
            case GO_TO_GO: // done
                player.setLocation(board.getStreetAt(0));
                player.increasePlayerBalance(200);
                return StaticStringKeeper.GO_TO_GO_OUTPUT_MESSAGE;
            case COLLECT_X_EAP: // done
                int randomNumber = getRandomInt(Arrays.asList(25, 50, 100, 150, 200));
                player.increasePlayerBalance(randomNumber);
                return StaticStringKeeper.getCollectXEapOutputMessage(randomNumber);
            case GET_X_EAP_FROM_EVERY_PLAYER: // done
                int rewardMoney = getRandomInt(Arrays.asList(25, 50));
                for (Player pp : board.getOtherPlayers()) {
                    pp.reducePlayerBalance(rewardMoney);
                    player.increasePlayerBalance(rewardMoney);
                }
                return StaticStringKeeper.getGetXEapFromEveryPlayerOutputMessage(rewardMoney * (board.getOtherPlayers().size()));
            case GO_TO_NEXT_UTILITY:
                int newLocationU = currentLoc < 12 || currentLoc >= 28 ? 12 : 28;
                player.setLocation(board.getStreetAt(newLocationU));
                ((PurchasableStreet) player.getLocation(board)).payRent(player, 6, board);
                if (currentLoc >= newLocationU) {
                    player.increasePlayerBalance(200);
                    return StaticStringKeeper.GO_TO_NEXT_UTILITY_OUTPUT_MESSAGE_PASS_GO;
                }
                return StaticStringKeeper.GO_TO_NEXT_UTILITY_OUTPUT_MESSAGE;
            case GO_TO_NEXT_RAILROAD:
                int newLocationR = currentLoc == 2 || currentLoc == 36 ? 5
                        : currentLoc == 7 ? 15
                        : currentLoc == 17 || currentLoc == 22 ? 25 : 35;
                player.setLocation(board.getStreetAt(newLocationR));
                ((PurchasableStreet) player.getLocation(board)).payRent(player, 2, board);
                if (currentLoc >= newLocationR) {
                    player.increasePlayerBalance(200);
                    return StaticStringKeeper.GO_TO_NEXT_RAILROAD_OUTPUT_MESSAGE_PASS_GO;
                }
                return StaticStringKeeper.GO_TO_NEXT_RAILROAD_OUTPUT_MESSAGE;
            case GO_TO_X_STREET:  // done
                int randomNumberS = getRandomInt(Arrays.asList(2, 4, 6));
                board.move(player, randomNumberS);
                if (currentLoc >= player.getLocation(board).getIndex()) {
                    player.increasePlayerBalance(200);
                    return StaticStringKeeper.GO_TO_X_STREET_OUTPUT_MESSAGE_PASS_GO;
                }
                return StaticStringKeeper.GO_TO_X_STREET_OUTPUT_MESSAGE;
        }
        return null;
    }

    /**
     * Depending on the punish argument,
     * a player receives has to perform certain action that are not beneficial to the player.
     *
     * @param punish argument that sets the received punishment.
     * @param player player who receives the punishment.
     * @param board board argument is need for some technical reason.
     * @return String, informing the player of what has happened.
     */
    public String quePunish(Punishment punish, Player player, Board board) {
        switch (punish) {
            case GO_TO_JAIL:
                return player.enterJail(board, true);
            case PAY_X_EAP: // done
                int randomNumber = getRandomInt(Arrays.asList(25, 50, 100, 150));
                player.reducePlayerBalance(randomNumber);
                return StaticStringKeeper.getPayXEapOutputMessage(randomNumber);
            case GO_BACK_THREE_TIMES: // done
                board.move(player, -3);
                return StaticStringKeeper.GO_BACK_3_STEPS_OUTPUT_MESSAGE;
            case PAY_EVERY_PLAYER_X_EAP: // done
                for (Player pp : board.getOtherPlayers()) {
                    pp.increasePlayerBalance(50);
                    player.reducePlayerBalance(50);
                }
                return StaticStringKeeper.getPayXEapToEveryPlayerOutputMessage(50 * board.getOtherPlayers().size());
        }
        return null;
    }

    /*
     * Return random item from given list.
     */
    public static Punishment getRandomItemPun(List<Punishment> items) {
        return items.get(new Random().nextInt(items.size()));
    }

    /*
     * Return random item from given list.
     */
    public static Reward getRandomItemRew(List<Reward> items) {
        return items.get(new Random().nextInt(items.size()));
    }

    /*
     * Return random item from given list.
     */
    public static Integer getRandomInt(List<Integer> items) {
        return items.get(new Random().nextInt(items.size()));
    }

    /*
     * Return random item from given list.
     */
    public static Card getRandomCard(List<Card> items) {
        return items.get(new Random().nextInt(items.size()));
    }
}

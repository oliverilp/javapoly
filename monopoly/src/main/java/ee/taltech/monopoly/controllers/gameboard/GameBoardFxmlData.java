package ee.taltech.monopoly.controllers.gameboard;

import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;

import java.util.HashMap;
import java.util.Map;

public class GameBoardFxmlData {
    @FXML
    protected TilePane tilePane0;
    @FXML
    protected TilePane tilePane1;
    @FXML
    protected TilePane tilePane2;
    @FXML
    protected TilePane tilePane3;
    @FXML
    protected TilePane tilePane4;
    @FXML
    protected TilePane tilePane5;
    @FXML
    protected TilePane tilePane6;
    @FXML
    protected TilePane tilePane7;
    @FXML
    protected TilePane tilePane8;
    @FXML
    protected TilePane tilePane9;
    @FXML
    protected TilePane tilePane10;
    @FXML
    protected TilePane tilePane11;
    @FXML
    protected TilePane tilePane12;
    @FXML
    protected TilePane tilePane13;
    @FXML
    protected TilePane tilePane14;
    @FXML
    protected TilePane tilePane15;
    @FXML
    protected TilePane tilePane16;
    @FXML
    protected TilePane tilePane17;
    @FXML
    protected TilePane tilePane18;
    @FXML
    protected TilePane tilePane19;
    @FXML
    protected TilePane tilePane20;
    @FXML
    protected TilePane tilePane21;
    @FXML
    protected TilePane tilePane22;
    @FXML
    protected TilePane tilePane23;
    @FXML
    protected TilePane tilePane24;
    @FXML
    protected TilePane tilePane25;
    @FXML
    protected TilePane tilePane26;
    @FXML
    protected TilePane tilePane27;
    @FXML
    protected TilePane tilePane28;
    @FXML
    protected TilePane tilePane29;
    @FXML
    protected TilePane tilePane30;
    @FXML
    protected TilePane tilePane31;
    @FXML
    protected TilePane tilePane32;
    @FXML
    protected TilePane tilePane33;
    @FXML
    protected TilePane tilePane34;
    @FXML
    protected TilePane tilePane35;
    @FXML
    protected TilePane tilePane36;
    @FXML
    protected TilePane tilePane37;
    @FXML
    protected TilePane tilePane38;
    @FXML
    protected TilePane tilePane39;

    @FXML
    protected Region streetColorRegion1;
    @FXML
    protected Region streetColorRegion3;
    @FXML
    protected Region streetColorRegion5;
    @FXML
    protected Region streetColorRegion6;
    @FXML
    protected Region streetColorRegion8;
    @FXML
    protected Region streetColorRegion9;
    @FXML
    protected Region streetColorRegion11;
    @FXML
    protected Region streetColorRegion12;
    @FXML
    protected Region streetColorRegion13;
    @FXML
    protected Region streetColorRegion14;
    @FXML
    protected Region streetColorRegion15;
    @FXML
    protected Region streetColorRegion16;
    @FXML
    protected Region streetColorRegion18;
    @FXML
    protected Region streetColorRegion19;
    @FXML
    protected Region streetColorRegion21;
    @FXML
    protected Region streetColorRegion23;
    @FXML
    protected Region streetColorRegion24;
    @FXML
    protected Region streetColorRegion25;
    @FXML
    protected Region streetColorRegion26;
    @FXML
    protected Region streetColorRegion27;
    @FXML
    protected Region streetColorRegion28;
    @FXML
    protected Region streetColorRegion29;
    @FXML
    protected Region streetColorRegion31;
    @FXML
    protected Region streetColorRegion32;
    @FXML
    protected Region streetColorRegion34;
    @FXML
    protected Region streetColorRegion35;
    @FXML
    protected Region streetColorRegion37;
    @FXML
    protected Region streetColorRegion39;

    @FXML
    protected HBox housesContainer1;
    @FXML
    protected HBox housesContainer3;
    @FXML
    protected HBox housesContainer5;
    @FXML
    protected HBox housesContainer6;
    @FXML
    protected HBox housesContainer8;
    @FXML
    protected HBox housesContainer9;
    @FXML
    protected VBox housesContainer11;
    @FXML
    protected VBox housesContainer12;
    @FXML
    protected VBox housesContainer13;
    @FXML
    protected VBox housesContainer14;
    @FXML
    protected VBox housesContainer15;
    @FXML
    protected VBox housesContainer16;
    @FXML
    protected VBox housesContainer18;
    @FXML
    protected VBox housesContainer19;
    @FXML
    protected HBox housesContainer21;
    @FXML
    protected HBox housesContainer23;
    @FXML
    protected HBox housesContainer24;
    @FXML
    protected HBox housesContainer25;
    @FXML
    protected HBox housesContainer26;
    @FXML
    protected HBox housesContainer27;
    @FXML
    protected HBox housesContainer28;
    @FXML
    protected HBox housesContainer29;
    @FXML
    protected VBox housesContainer31;
    @FXML
    protected VBox housesContainer32;
    @FXML
    protected VBox housesContainer34;
    @FXML
    protected VBox housesContainer35;
    @FXML
    protected VBox housesContainer37;
    @FXML
    protected VBox housesContainer39;

    @FXML
    protected StackPane streetStackPane1;
    @FXML
    protected StackPane streetStackPane3;
    @FXML
    protected StackPane streetStackPane5;
    @FXML
    protected StackPane streetStackPane6;
    @FXML
    protected StackPane streetStackPane8;
    @FXML
    protected StackPane streetStackPane9;
    @FXML
    protected StackPane streetStackPane11;
    @FXML
    protected StackPane streetStackPane12;
    @FXML
    protected StackPane streetStackPane13;
    @FXML
    protected StackPane streetStackPane14;
    @FXML
    protected StackPane streetStackPane15;
    @FXML
    protected StackPane streetStackPane16;
    @FXML
    protected StackPane streetStackPane18;
    @FXML
    protected StackPane streetStackPane19;
    @FXML
    protected StackPane streetStackPane21;
    @FXML
    protected StackPane streetStackPane23;
    @FXML
    protected StackPane streetStackPane24;
    @FXML
    protected StackPane streetStackPane25;
    @FXML
    protected StackPane streetStackPane26;
    @FXML
    protected StackPane streetStackPane27;
    @FXML
    protected StackPane streetStackPane28;
    @FXML
    protected StackPane streetStackPane29;
    @FXML
    protected StackPane streetStackPane31;
    @FXML
    protected StackPane streetStackPane32;
    @FXML
    protected StackPane streetStackPane34;
    @FXML
    protected StackPane streetStackPane35;
    @FXML
    protected StackPane streetStackPane37;
    @FXML
    protected StackPane streetStackPane39;

    @FXML
    protected StackPane rootStackPane;
    @FXML
    protected HBox hBox;
    @FXML
    protected StackPane stackPane;
    @FXML
    protected GridPane gridPane;
    @FXML
    protected GridPane leftGridPane;
    @FXML
    protected VBox buttonsVBox;
    @FXML
    protected Pane snackBarPane;
    @FXML
    protected VBox rightPlayersInfoVBox;

    @FXML
    protected ImageView imageView;
    @FXML
    protected WebView webView;
    @FXML
    protected Label currentPlayerName;
    @FXML
    protected Label currentStreetName;
    @FXML
    protected JFXButton purchaseStreetButton;
    @FXML
    protected JFXButton purchaseHouseButton;
    @FXML
    protected JFXButton moveButton;
    @FXML
    protected JFXButton endTurnButton;
    @FXML
    protected StackPane leftDiceStackPane;
    @FXML
    protected StackPane rightDiceStackPane;

    @SuppressWarnings("DuplicatedCode")
    public Map<Integer, TilePane> getTiles() {
        return new HashMap<>() {{
            put(0, tilePane0);
            put(1, tilePane1);
            put(2, tilePane2);
            put(3, tilePane3);
            put(4, tilePane4);
            put(5, tilePane5);
            put(6, tilePane6);
            put(7, tilePane7);
            put(8, tilePane8);
            put(9, tilePane9);
            put(10, tilePane10);
            put(11, tilePane11);
            put(12, tilePane12);
            put(13, tilePane13);
            put(14, tilePane14);
            put(15, tilePane15);
            put(16, tilePane16);
            put(17, tilePane17);
            put(18, tilePane18);
            put(19, tilePane19);
            put(20, tilePane20);
            put(21, tilePane21);
            put(22, tilePane22);
            put(23, tilePane23);
            put(24, tilePane24);
            put(25, tilePane25);
            put(26, tilePane26);
            put(27, tilePane27);
            put(28, tilePane28);
            put(29, tilePane29);
            put(30, tilePane30);
            put(31, tilePane31);
            put(32, tilePane32);
            put(33, tilePane33);
            put(34, tilePane34);
            put(35, tilePane35);
            put(36, tilePane36);
            put(37, tilePane37);
            put(38, tilePane38);
            put(39, tilePane39);
        }};
    }

    @SuppressWarnings("DuplicatedCode")
    public Map<Integer, Region> getStreetColorRegions() {
        return new HashMap<>() {{
            put(1, streetColorRegion1);
            put(3, streetColorRegion3);
            put(5, streetColorRegion5);
            put(6, streetColorRegion6);
            put(8, streetColorRegion8);
            put(9, streetColorRegion9);
            put(11, streetColorRegion11);
            put(12, streetColorRegion12);
            put(13, streetColorRegion13);
            put(14, streetColorRegion14);
            put(15, streetColorRegion15);
            put(16, streetColorRegion16);
            put(18, streetColorRegion18);
            put(19, streetColorRegion19);
            put(21, streetColorRegion21);
            put(23, streetColorRegion23);
            put(24, streetColorRegion24);
            put(25, streetColorRegion25);
            put(26, streetColorRegion26);
            put(27, streetColorRegion27);
            put(28, streetColorRegion28);
            put(29, streetColorRegion29);
            put(31, streetColorRegion31);
            put(32, streetColorRegion32);
            put(34, streetColorRegion34);
            put(35, streetColorRegion35);
            put(37, streetColorRegion37);
            put(39, streetColorRegion39);
        }};
    }

    @SuppressWarnings("DuplicatedCode")
    public Map<Integer, Pane> getHousesContainers() {
        return new HashMap<>() {{
            put(1, housesContainer1);
            put(3, housesContainer3);
            put(5, housesContainer5);
            put(6, housesContainer6);
            put(8, housesContainer8);
            put(9, housesContainer9);
            put(11, housesContainer11);
            put(12, housesContainer12);
            put(13, housesContainer13);
            put(14, housesContainer14);
            put(15, housesContainer15);
            put(16, housesContainer16);
            put(18, housesContainer18);
            put(19, housesContainer19);
            put(21, housesContainer21);
            put(23, housesContainer23);
            put(24, housesContainer24);
            put(25, housesContainer25);
            put(26, housesContainer26);
            put(27, housesContainer27);
            put(28, housesContainer28);
            put(29, housesContainer29);
            put(31, housesContainer31);
            put(32, housesContainer32);
            put(34, housesContainer34);
            put(35, housesContainer35);
            put(37, housesContainer37);
            put(39, housesContainer39);
        }};
    }

    @SuppressWarnings("DuplicatedCode")
    public Map<Integer, StackPane> getStreetStackPanes() {
        return new HashMap<>() {{
            put(1, streetStackPane1);
            put(3, streetStackPane3);
            put(5, streetStackPane5);
            put(6, streetStackPane6);
            put(8, streetStackPane8);
            put(9, streetStackPane9);
            put(11, streetStackPane11);
            put(12, streetStackPane12);
            put(13, streetStackPane13);
            put(14, streetStackPane14);
            put(15, streetStackPane15);
            put(16, streetStackPane16);
            put(18, streetStackPane18);
            put(19, streetStackPane19);
            put(21, streetStackPane21);
            put(23, streetStackPane23);
            put(24, streetStackPane24);
            put(25, streetStackPane25);
            put(26, streetStackPane26);
            put(27, streetStackPane27);
            put(28, streetStackPane28);
            put(29, streetStackPane29);
            put(31, streetStackPane31);
            put(32, streetStackPane32);
            put(34, streetStackPane34);
            put(35, streetStackPane35);
            put(37, streetStackPane37);
            put(39, streetStackPane39);
        }};
    }

    public VBox getVBox() {
        return buttonsVBox;
    }
}

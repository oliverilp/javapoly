package com.example.test.spring;

import com.example.test.spring.gamelogic.Player;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import reactor.core.publisher.Flux;
import reactor.core.publisher.UnicastProcessor;

/**
 * The entry point of the Spring Boot application.
 */
@SpringBootApplication
public class Application extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    UnicastProcessor<Boolean> joinGamePublisher() {
        return UnicastProcessor.create();
    }

    @Bean
    Flux<Boolean> joinGameFlux(UnicastProcessor<Boolean> publisher) {
        return publisher
            .replay(30)
            .autoConnect();
    }

    @Bean
    UnicastProcessor<Integer> startGamePublisher() {
        return UnicastProcessor.create();
    }

    @Bean
    Flux<Integer> startGameFlux(UnicastProcessor<Integer> publisher) {
        return publisher
            .replay(30)
            .autoConnect();
    }
    @Bean
    UnicastProcessor<ChatMessage> messagePublisher() {
        return UnicastProcessor.create();
    }

    @Bean
    Flux<ChatMessage> messagesFlux(UnicastProcessor<ChatMessage> publisher) {
        return publisher
            .replay(30)
            .autoConnect();
    }
}

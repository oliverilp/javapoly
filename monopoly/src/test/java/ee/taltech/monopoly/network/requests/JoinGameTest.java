package ee.taltech.monopoly.network.requests;

import ee.taltech.monopoly.gamelogic.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class JoinGameTest {
    Player player = new Player();
    JoinGame joinGame = new JoinGame();

    @BeforeEach
    void setUp() {
        joinGame = new JoinGame("id", player);
    }

    @Test
    void getPlayer() {
        assertEquals(player, joinGame.getPlayer());
    }
}

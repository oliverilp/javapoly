package ee.taltech.monopoly.controllers.gameboard;

import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.TilePane;

public class PlayerViewModel {
    StackPane playerIcon;
    TilePane currentTileLocation;
    Label balanceLabel;

    public PlayerViewModel(StackPane playerIcon, TilePane currentLocation) {
        this.playerIcon = playerIcon;
        this.currentTileLocation = currentLocation;
    }

    public StackPane getPlayerIcon() {
        return playerIcon;
    }

    public TilePane getCurrentTileLocation() {
        return currentTileLocation;
    }

    public void setCurrentTileLocation(TilePane currentTileLocation) {
        this.currentTileLocation = currentTileLocation;
    }

    public Label getBalanceLabel() {
        return balanceLabel;
    }

    public void setBalanceLabel(Label balanceLabel) {
        this.balanceLabel = balanceLabel;
    }
}

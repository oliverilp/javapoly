package ee.taltech.monopoly.gamelogic;

public class Dice {
    private static final int MIN_SINGLE_DICE_VALUE = 1;
    private static final int MAX_SINGLE_DICE_VALUE = 6;

    /*
     * Return a random number in a range 1-6, used for moving.
     */
    public static int rollSingle() {
        return (int) (Math.random() * (MAX_SINGLE_DICE_VALUE + 1 - MIN_SINGLE_DICE_VALUE)) + MIN_SINGLE_DICE_VALUE;
    }

    /*
     * Return a random number in a range 2-12, used for moving.
     */
    public static int rollDouble() {
        return rollSingle() + rollSingle();
    }
}

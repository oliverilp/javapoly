package ee.taltech.monopoly.gamelogic.cards;

public final class Questions {
    /*
     * All the questions and answer meant for creating cards.
     */
    public static final String QUESTION1 = "Mis on Ago lemmik toit?";
    public static final String Q1_CORRECT_ANSWER = "Pelmeenid";
    public static final String Q1_ANSWER1 = "Makaronid";
    public static final String Q1_ANSWER2 = "Juustukook";
    public static final String Q1_ANSWER3 = "Pizza";

    public static final String QUESTION2 = "Mis on Margus Kruusi lemmik koeratõug?";
    public static final String Q2_ANSWER1 = "Terjer";
    public static final String Q2_ANSWER2 = "Snautser";
    public static final String Q2_CORRECT_ANSWER = "Dalmaatsia";
    public static final String Q2_ANSWER3 = "Retriiver";

    public static final String QUESTION3 = "Mis järgmistest andmestruktuuridest on vaikimisi sorteeritud?";
    public static final String Q3_ANSWER1 = "HashSet";
    public static final String Q3_ANSWER2 = "LinkedSet";
    public static final String Q3_CORRECT_ANSWER = "TreeSet";
    public static final String Q3_ANSWER3 = "ArrayList";

    public static final String QUESTION4 = "Mida tähendab JRE Java programmeerimise kontekstis?";
    public static final String Q4_ANSWER1 = "Java Reader Error";
    public static final String Q4_ANSWER2 = "Java Rule Ending";
    public static final String Q4_CORRECT_ANSWER = "Java Runtime Environment";
    public static final String Q4_ANSWER3 = "Java Running Engine";

    public static final String QUESTION5 = "Mis on õigeim viis Javas objektide loomiseks?";
    public static final String Q5_ANSWER1 = "class myMonopoly = new Monopoly();";
    public static final String Q5_ANSWER2 = "new myMonopoly = MyMonopoly;";
    public static final String Q5_CORRECT_ANSWER = "MyMonopoly myMonopoly = new MyMonopoly();";
    public static final String Q5_ANSWER3 = "class MyMonopoly = new Monopoly();";

    public static final String QUESTION6 = "Milline järgmistest väidetest on vale?";
    public static final String Q6_ANSWER1 = "Klassil võib olla mitu konstruktorit.";
    public static final String Q6_ANSWER2 = "Klassil võib olla mitu alamklassi.";
    public static final String Q6_CORRECT_ANSWER = "Klassil võib olla mitu ülemklassi.";
    public static final String Q6_ANSWER3 = "Klass võib implementeerida mitu liidest.";

    public static final String QUESTION7 = "Mis prinditakse konsooli?\n" +
            "System.out.println('u' + 'n' + 'i' + 'c' + 'o' + 'r' + 'n');";
    public static final String Q7_ANSWER1 = "uni66";
    public static final String Q7_ANSWER2 = "unicorn";
    public static final String Q7_CORRECT_ANSWER = "766";
    public static final String Q7_ANSWER3 = "76orn";

    public static final String QUESTION8 = "Milline märksõna takistab muutuja väärtuse muutmist?";
    public static final String Q8_ANSWER1 = "enum";
    public static final String Q8_ANSWER2 = "private";
    public static final String Q8_CORRECT_ANSWER = "final";
    public static final String Q8_ANSWER3 = "static";

    public static final String QUESTION9 = "Vali sobiv andmetüüp sellele väärtusele: \"1\"";
    public static final String Q9_ANSWER1 = "int";
    public static final String Q9_ANSWER2 = "integer";
    public static final String Q9_CORRECT_ANSWER = "String";
    public static final String Q9_ANSWER3 = "char";

    public static final String QUESTION10 = "Mis on kõigi IT tudengite lemmik aine?";
    public static final String Q10_ANSWER1 = "Diskmata 1";
    public static final String Q10_ANSWER2 = "Programmeerimise põhikursus";
    public static final String Q10_CORRECT_ANSWER = "Robotite Programmeerimine";
    public static final String Q10_ANSWER3 = "Programmeerimise algkursus";

    public static final String QUESTION11 = "Mis mängul põhineb põhineb lambda.ee symbol?";
    public static final String Q11_ANSWER1 = "Dark Souls";
    public static final String Q11_ANSWER2 = "Monopoly";
    public static final String Q11_CORRECT_ANSWER = "Half-Life";
    public static final String Q11_ANSWER3 = "World of Warcraft";

    public static final String QUESTION12 = "Mida vihkab Edmnund Laugasson?";
    public static final String Q12_ANSWER1 = "Windowsit";
    public static final String Q12_ANSWER2 = "Õpilasi";
    public static final String Q12_CORRECT_ANSWER = "Võõrsõnade kasutamist";
    public static final String Q12_ANSWER3 = "H.P Lovecrafti novelli \"Charles Dexter Wardi juhtum\"";

    public static final String QUESTION13 = "Mis firma riistvara õpitakse kasutama aines \"Arvutivõrgud\"?";
    public static final String Q13_ANSWER1 = "Apple";
    public static final String Q13_ANSWER2 = "Microsoft";
    public static final String Q13_CORRECT_ANSWER = "Cisco";
    public static final String Q13_ANSWER3 = "Space X";

    public static final String QUESTION14 = "Mis on programmeerija parim sõber?";
    public static final String Q14_ANSWER1 = "Vastava keele ametlik dokumentatsioon";
    public static final String Q14_ANSWER2 = "Vastava keele õpikud";
    public static final String Q14_CORRECT_ANSWER = "StackOverflow";
    public static final String Q14_ANSWER3 = "Vastava keele loengud";

    public static final String QUESTION15 = "Mis prinditakse konsooli?\n" +
            "System.out.println(Math.min(Double.MIN_VALUE, -1));";
    public static final String Q15_ANSWER1 = "0";
    public static final String Q15_ANSWER2 = "Väga väike number";
    public static final String Q15_CORRECT_ANSWER = "-1";
    public static final String Q15_ANSWER3 = "Väga suur number";
}

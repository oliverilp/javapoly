module.exports = {
  title: 'Monopoly',
  description: 'Monopoly project',
  plugins: [
    'vuepress-plugin-mermaidjs'
  ],
  themeConfig: {
    nav: [
      { text: 'Changelog', link: '/changelog' },
    ],
    sidebar: 'auto'
  },
}

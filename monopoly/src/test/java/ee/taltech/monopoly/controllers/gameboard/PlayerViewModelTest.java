package ee.taltech.monopoly.controllers.gameboard;

import javafx.scene.layout.StackPane;
import javafx.scene.layout.TilePane;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PlayerViewModelTest {
    private PlayerViewModel playerViewModel;
    private StackPane stackPane;
    private TilePane currentTileLocation;

    @BeforeEach
    void setUp() {
        stackPane = new StackPane();
        currentTileLocation = new TilePane();
        playerViewModel = new PlayerViewModel(stackPane, currentTileLocation);
    }

    @Test
    void getPlayerIcon() {
        assertEquals(stackPane, playerViewModel.getPlayerIcon());
    }

    @Test
    void getCurrentTileLocation() {
        assertEquals(currentTileLocation, playerViewModel.getCurrentTileLocation());
    }

    @Test
    void setCurrentTileLocation() {
        TilePane newTilePane = new TilePane();
        playerViewModel.setCurrentTileLocation(newTilePane);
        assertEquals(newTilePane, playerViewModel.getCurrentTileLocation());
    }

    @Test
    void getBalanceLabel() {
        assertNull(playerViewModel.getBalanceLabel());
    }
}

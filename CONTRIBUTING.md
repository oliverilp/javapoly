# Contributing guidelines

## Style conventions

Please prefix all commits with issue number. For instance commit for issue nr 7 should be `#7: commit message`.
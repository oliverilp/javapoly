package ee.taltech.monopoly.controllers.gameboard;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GameBoardFxmlDataTest {
    private GameBoardFxmlData gameBoardFxmlData;

    @BeforeEach
    void setUp() {
        gameBoardFxmlData = new GameBoardFxmlData();
    }

    @Test
    void getTiles() {
        assertNotNull(gameBoardFxmlData.getTiles());
    }

    @Test
    void getStreetColorRegions() {
        assertNotNull(gameBoardFxmlData.getStreetColorRegions());
    }

    @Test
    void getHousesContainers() {
        assertNotNull(gameBoardFxmlData.getHousesContainers());
    }

    @Test
    void getStreetStackPanes() {
        assertNotNull(gameBoardFxmlData.getStreetStackPanes());
    }

    @Test
    void getVBox() {
        assertNull(gameBoardFxmlData.getVBox());
    }
}

package ee.taltech.monopoly.gamelogic.street;

import ee.taltech.monopoly.gamelogic.Board;
import ee.taltech.monopoly.gamelogic.Player;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;

class StreetTest {
    Board board = new Board();
    Street street = new Street() {
        @Override
        public String getName() {
            return null;
        }

        @Override
        public int getIndex() {
            return 0;
        }
    };
    Player player = new Player("Name", "id", board);

    @Test
    void getStreetGroup() {
        assertNull(street.getStreetGroup());
    }

    @Test
    void getOwner() {
        assertNull(street.getOwner(board));
    }

    @Test
    void canBuy() {
        assertFalse(street.canBuy(player, board));
    }
}

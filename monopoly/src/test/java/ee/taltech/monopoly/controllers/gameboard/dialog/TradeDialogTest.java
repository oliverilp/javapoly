package ee.taltech.monopoly.controllers.gameboard.dialog;

import ee.taltech.monopoly.gamelogic.Board;
import ee.taltech.monopoly.gamelogic.Player;
import javafx.embed.swing.JFXPanel;
import javafx.scene.layout.StackPane;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class TradeDialogTest {
    JFXPanel panel = new JFXPanel();
    StackPane rootStackPane;
    Player player1;
    Player player2;
    TradeDialog tradeDialog1;
    TradeDialog tradeDialog2;

    @BeforeEach
    void setUp() {
        Board.initialize("ID");
        player1 = new Player("Name1", "ID1", Board.getInstance());
        player2 = new Player("Name2", "ID2", Board.getInstance());
        Board.getInstance().getAllPlayers().add(player1);
        Board.getInstance().getAllPlayers().add(player2);
        rootStackPane = new StackPane();
        tradeDialog1 = new TradeDialog(rootStackPane, player1, player2);
        tradeDialog2 = new TradeDialog(rootStackPane, player1, player2, new HashMap<>(), new HashMap<>(), 0, 0);
    }

    @Test
    void tradeResult() {
//        tradeDialog2.tradeResult(false);
//        tradeDialog2.tradeResult(true);
    }

    @Test
    void makeOfferDialog() {
        tradeDialog1.makeOfferDialog();
    }

    @Test
    void showOfferDialog() {
        tradeDialog2.showOfferDialog();
    }
}

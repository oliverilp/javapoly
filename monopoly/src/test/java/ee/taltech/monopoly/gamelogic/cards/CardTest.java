package ee.taltech.monopoly.gamelogic.cards;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CardTest {
    Card cardEmpty = new Card();
    Card card = new Card("Question", Arrays.asList("banana", "orange", "answer"), "answer");

    @Test
    void getQuestion() {
        assertEquals("Question", card.getQuestion());
    }

    @Test
    void getAnswer() {
        assertEquals("answer", card.getAnswer());
    }

    @Test
    void getAnswerChoices() {
        assertEquals(Arrays.asList("banana", "orange", "answer"), card.getAnswerChoices());
    }
}

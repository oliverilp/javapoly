package ee.taltech.monopoly.ai;

import ee.taltech.monopoly.gamelogic.Board;
import ee.taltech.monopoly.gamelogic.Player;
import ee.taltech.monopoly.gamelogic.street.purchasable.RegularStreet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


class DecideStreetPurchasingTest {
    Board board = new Board();
    AIPlayer player;
    Player player2;
    Player player3;
    RegularStreet regularStreet = (RegularStreet) board.getStreetAt(3);

    RegularStreet regularStreet2 = (RegularStreet) board.getStreetAt(39);
    RegularStreet regularStreet3 = (RegularStreet) board.getStreetAt(37);

    RegularStreet regularStreet4 = (RegularStreet) board.getStreetAt(11);
    RegularStreet regularStreet5 = (RegularStreet) board.getStreetAt(13);
    RegularStreet regularStreet6 = (RegularStreet) board.getStreetAt(14);

    @BeforeEach
    void setUp() {
        player = new AIPlayer("ai", "234", board);
        player2 = new Player("H", "123", board);
        player3 = new Player("C", "343", board);
        board.setAllPlayers(Arrays.asList(player, player2, player3));
    }

    @Test
    void decideNoMoney() {
        DecideStreetPurchasing decideStreetPurchasing = new DecideStreetPurchasing(player, regularStreet2, board);
        player.reducePlayerBalance(1300);
        assertFalse(decideStreetPurchasing.decide());
    }

    @Test
    void decideTrue() {
        DecideStreetPurchasing decideStreetPurchasing = new DecideStreetPurchasing(player, regularStreet, board);
        player.setLocation(regularStreet);
        regularStreet.purchaseStreet(player, board);
        assertTrue(decideStreetPurchasing.decide());
    }

    @Test
    void decideFalse() {
        DecideStreetPurchasing decideStreetPurchasing = new DecideStreetPurchasing(player, regularStreet2, board);
        player.setLocation(regularStreet2);
        regularStreet3.purchaseStreet(player2, board);
        player.reducePlayerBalance(1000);
        System.out.println(player.getPlayerBalance() );
        assertFalse(decideStreetPurchasing.decide());
    }

    @Test
    void decideFalse2() {
        DecideStreetPurchasing decideStreetPurchasing = new DecideStreetPurchasing(player, regularStreet4, board);
        regularStreet5.purchaseStreet(player2, board);
        regularStreet6.purchaseStreet(player3, board);
        player.reducePlayerBalance(1000);
        assertFalse(decideStreetPurchasing.decide());
    }
}

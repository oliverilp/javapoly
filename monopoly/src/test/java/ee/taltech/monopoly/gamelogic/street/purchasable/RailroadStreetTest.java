package ee.taltech.monopoly.gamelogic.street.purchasable;

import ee.taltech.monopoly.gamelogic.Board;
import ee.taltech.monopoly.gamelogic.Player;
import ee.taltech.monopoly.gamelogic.street.StreetGroup;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RailroadStreetTest {
    RailroadStreet railroadStreetEmpty = new RailroadStreet();
    RailroadStreet railroadStreet = new RailroadStreet("Name", 25, StreetGroup.RAILROAD, 5);
    RailroadStreet railroadStreet2 = new RailroadStreet("Name2", 50, StreetGroup.RAILROAD, 15);
    Board board = new Board();
    Player owner = new Player("Name1", "id_1_", board);
    Player payer = new Player("Name2", "id_2_", board);

    @BeforeEach
    void setup() {
        board.getStreets().set(5, railroadStreet);
        board.getStreets().set(15, railroadStreet2);

        if (board.getAllPlayers().isEmpty()) {
            board.setAllPlayers(Arrays.asList(owner, payer));
        }
    }

    @Test
    void getRent() {
        assertEquals(0, railroadStreet.getRent(payer, 2, board));
        railroadStreet.purchaseStreet(owner, board);
        assertEquals(25, railroadStreet.getRent(payer, 2, board));
        railroadStreet2.purchaseStreet(owner, board);
        assertEquals(50, railroadStreet.getRent(payer, 2, board));
    }
}

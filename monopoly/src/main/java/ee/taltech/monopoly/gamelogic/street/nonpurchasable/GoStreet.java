package ee.taltech.monopoly.gamelogic.street.nonpurchasable;

public class GoStreet extends NonPurchasableStreet {
    public GoStreet() {}

    /**
     * Creates NonPurchasable street based on the real Go street.
     *
     * @param index location on board.
     */
    public GoStreet(int index) {
        name = "Go";
        this.index = index;
    }
}

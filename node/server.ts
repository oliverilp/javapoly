const app = require('express')();
const bodyParser = require('body-parser');

// app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
// app.use(bodyParser.raw());

const http = require('http').createServer(app);

// const config = require('config');

enum GamePhase {
    PLAYERS_GATHERING = 'PLAYERS_GATHERING',
    IN_PROGRESS = 'IN_PROGRESS'
}

interface Game {
    phase: GamePhase
    players: string[]
    state: string
}

const game: Game = {
    phase: GamePhase.PLAYERS_GATHERING,
    players: [],
    state: ''
};

/**
 * Add the current player to the game and return all players
 */
app.post('/join-game/', function (req, res) {
    const playerName: string = req.query.name;
    game.players.push(playerName);
    console.log(`Player ${playerName} joined`);
    res.json(game.players);
});

/**
 * Set game.phase to IN_PROGRESS, this means that new players cannot join and the game begins
 */
app.get('/players/', function (req, res) {
    console.log('Game started');
    res.json({players: game.players, phase: game.phase});
});

app.post('/start-game/', function (req, res) {
    game.phase = GamePhase.IN_PROGRESS;
    console.log('Game started');
    res.json(game.players);
});


http.listen(3000, function () {
    console.log('listening on *:3000');
});

package ee.taltech.monopoly.gamelogic.street.nonpurchasable;

import ee.taltech.monopoly.gamelogic.Board;
import ee.taltech.monopoly.gamelogic.Player;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class GoToJailStreetTest {
    Board board = new Board();
    Player player = new Player("Name", "id", board);
    GoToJailStreet goToJailStreet = new GoToJailStreet();

    @Test
    void onEnterAndPayFine() {
        assertEquals(0, player.getJailCounter());
        assertFalse(player.isInJail());
        player.enterJail(board, false);
        assertEquals(3, player.getJailCounter());
        assertTrue(player.isInJail());
        assertEquals(1500, player.getPlayerBalance());
        player.payJailFine();
        assertEquals(1450, player.getPlayerBalance());
        assertEquals(0, player.getJailCounter());
        assertFalse(player.isInJail());
    }
}

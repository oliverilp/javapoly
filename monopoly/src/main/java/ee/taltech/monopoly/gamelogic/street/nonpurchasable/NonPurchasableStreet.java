package ee.taltech.monopoly.gamelogic.street.nonpurchasable;

import ee.taltech.monopoly.gamelogic.street.Street;
import ee.taltech.monopoly.gamelogic.street.StreetGroup;

import java.util.Objects;

public abstract class NonPurchasableStreet implements Street {
    protected String name;
    protected StreetGroup streetGroup = StreetGroup.OTHER;
    protected int index;

    @Override
    public StreetGroup getStreetGroup() {
        return streetGroup;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getIndex() {
        return index;
    }

    @Override
    public String toString() {
        return "(" + index + ") " + name;
    }

    /**
     * Function that compares 2 objects and returns if they are the same.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NonPurchasableStreet that = (NonPurchasableStreet) o;
        return Objects.equals(name, that.name) &&
                streetGroup == that.streetGroup;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, streetGroup);
    }
}

package ee.taltech.monopoly.ai.webquery;

import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class QuestionSolverTest {
    @Test
    void answerQuestion() {
        final Set<String> possibleAnswers = Set.of("A", "B", "c");
        final String answer = QuestionSolver.answerQuestion("What is morally good", possibleAnswers);
        // Assert that a valid answer is returned
        assertTrue(possibleAnswers.contains(answer));
    }
}

package ee.taltech.monopoly;

import ee.taltech.monopoly.controllers.CreateLobbyController;
import ee.taltech.monopoly.controllers.JoinLobbyController;
import ee.taltech.monopoly.controllers.StartGameController;
import ee.taltech.monopoly.controllers.gameboard.GameBoardController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;

public class Game extends Application {
    private static Stage stage;
    private static Scene mainMenu;
    private static Scene joinLobby;
    private static Scene createLobby;
    private static Scene startGame;
    private static Scene gameBoard;

    @Override
    public void start(Stage stage) {
        Game.stage = stage;
        stage.getIcons().add(new Image(Game.class.getResourceAsStream("icon.png")));

        loadMainMenu();

        stage.setTitle("Javapoly");
        stage.setScene(mainMenu);
        stage.setMinWidth(1200);
        stage.setMinHeight(720);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

    private static Scene loadScene(String scenePath) {
        try {
            Parent root = FXMLLoader.load(Game.class.getResource(scenePath));
            Scene scene = new Scene(root);
            scene.getStylesheets().add(Game.class.getResource("styles.css").toString());

            return scene;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Stage getStage() {
        return stage;
    }

    private static void loadMainMenu() {
        mainMenu = loadScene("scenes/mainMenu.fxml");
    }

    public static Scene getJoinLobby() {
        return joinLobby;
    }

    public static void loadJoinLobby(Double width, Double height) {
        JoinLobbyController.setWidth(width);
        JoinLobbyController.setHeight(height);
        joinLobby = loadScene("scenes/joinLobby.fxml");
    }

    public static Scene getCreateLobby() {
        return createLobby;
    }

    public static void loadCreateLobby(Double width, Double height) {
        CreateLobbyController.setWidth(width);
        CreateLobbyController.setHeight(height);
        createLobby = loadScene("scenes/createLobby.fxml");
    }

    public static Scene getStartGame() {
        return startGame;
    }

    public static void loadStartGame(Double width, Double height) {
        StartGameController.setWidth(width);
        StartGameController.setHeight(height);
        startGame = loadScene("scenes/startGame.fxml");
    }

    public static Scene getGameBoard() {
        return gameBoard;
    }

    public static void loadGameBoard(Double width, Double height) {
        GameBoardController.setWidth(width);
        GameBoardController.setHeight(height);
        gameBoard = loadScene("scenes/gameBoard.fxml");
    }
}

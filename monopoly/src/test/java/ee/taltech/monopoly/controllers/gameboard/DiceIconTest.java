package ee.taltech.monopoly.controllers.gameboard;

import javafx.embed.swing.JFXPanel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DiceIconTest {
    private JFXPanel panel = new JFXPanel();

    @Test
    void getLeftIcons() {
        assertNotNull(DiceIcon.getLeftIcons());
    }

    @Test
    void getRightIcons() {
        assertNotNull(DiceIcon.getRightIcons());
    }
}

package ee.taltech.monopoly.network.viewmodels;

import ee.taltech.monopoly.gamelogic.Board;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BoardViewModelTest {
    BoardViewModel boardViewModel = new BoardViewModel();
    Board board = new Board();

    @BeforeEach
    void setUp() {
        boardViewModel = new BoardViewModel(board);
    }

    @Test
    void getBoard() {
        assertEquals(board, boardViewModel.getBoard());
    }
}

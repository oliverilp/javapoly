package ee.taltech.monopoly.gamelogic;

import ee.taltech.monopoly.gamelogic.street.Street;
import ee.taltech.monopoly.gamelogic.street.StreetGroup;
import ee.taltech.monopoly.gamelogic.street.purchasable.PurchasableStreet;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class Player {
    private String id;
    private String name;
    private Street location;
    private int playerBalance = 1500;
    private final Set<Integer> playerStreetsIndexes = new HashSet<>();
    private final Map<StreetGroup, Integer> playerStreetGroups = new HashMap<>();
    private PieceType pieceType;
    private PieceColor pieceColor;
    private boolean isInJail = false;
    private int jailCounter = 0;

    /**
     * Necessary for tests and Kryonet.
     */
    public Player() {
    }

    /**
     * This constructor creates a Player, used when players join games.
     *
     * @param name  player name.
     * @param id    player id.
     * @param board used to set the players starting position.
     */
    public Player(String name, String id, Board board) {
        this.name = name;
        this.id = id;
        location = board.getStreetAt(0);

        resetPlayerStreetGroups();
    }

    /*
     * Jail status.
     */
    public boolean isInJail() {
        return isInJail;
    }

    public void setInJail(boolean inJail) {
        isInJail = inJail;
    }

    public int getJailCounter() {
        return jailCounter;
    }

    public void setJailCounter(int jailCounter) {
        this.jailCounter = jailCounter;
    }

    /**
     * Used while the player is in jail.
     * Reduces jail sentence (jailCounter)
     * If counter reaches 0 then the player gets released (isInJail = false)
     */
    public void decreaseJailCounter() {
        jailCounter--;
        if (jailCounter <= 0) {
            isInJail = false;
            jailCounter = 0;
        }
    }

    /**
     * If the player enters the GoToJail Street or is sent to jail by the cardDeck then this is called.
     * then onEnter is called to send the player to the jail street and to give him the inJail status.
     *
     * @param board      board to set the players location.
     * @param isFromDeck determines the type of message sent to the player
     */
    public String enterJail(Board board, boolean isFromDeck) {
        this.setLocation(board.getStreetAt(10));
        this.setInJail(true);
        this.setJailCounter(3);
        return isFromDeck ? StaticStringKeeper.GO_TO_JAIL_DECK_OUTPUT_MESSAGE
                : StaticStringKeeper.GO_TO_JAIL_STREET_OUTPUT_MESSAGE;
    }

    public String getID() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Street getLocation(Board board) {
        return board.getStreetAt(location.getIndex());
    }

    public void setLocation(Street location) {
        this.location = location;
    }

    public int getPlayerBalance() {
        return playerBalance;
    }

    /*
     * Gets streets owned by player.
     */
    public Set<PurchasableStreet> getPlayerStreets(Board board) {
        return playerStreetsIndexes.stream()
                .map(index -> (PurchasableStreet) board.getStreetAt(index))
                .collect(Collectors.toSet());
    }

    /*
     * To get out of jail the player can pay a 50 € fine and his jail status is removed.
     */
    public void payJailFine() {
        if (isInJail && playerBalance >= 50) {
            reducePlayerBalance(50);
            isInJail = false;
            jailCounter = 0;
        }
    }


    public void addPlayerStreet(PurchasableStreet purchasableStreet) {
        playerStreetsIndexes.add(purchasableStreet.getIndex());
    }

    public void removePlayerStreet(PurchasableStreet purchasableStreet) {
        playerStreetsIndexes.remove(purchasableStreet.getIndex());
    }

    private void resetPlayerStreetGroups() {
        playerStreetGroups.put(StreetGroup.BROWN, 0);
        playerStreetGroups.put(StreetGroup.LIGHT_BLUE, 0);
        playerStreetGroups.put(StreetGroup.PINK, 0);
        playerStreetGroups.put(StreetGroup.ORANGE, 0);
        playerStreetGroups.put(StreetGroup.RED, 0);
        playerStreetGroups.put(StreetGroup.YELLOW, 0);
        playerStreetGroups.put(StreetGroup.GREEN, 0);
        playerStreetGroups.put(StreetGroup.DARK_BLUE, 0);
        playerStreetGroups.put(StreetGroup.UTILITY, 0);
        playerStreetGroups.put(StreetGroup.RAILROAD, 0);
    }

    /*
     * Return a HashMap of streetGroups based on the player's owned streets.
     */
    public Map<StreetGroup, Integer> getPlayerStreetGroups(Board board) {
        resetPlayerStreetGroups();
        for (Street street : getPlayerStreets(board)) {
            playerStreetGroups.put(street.getStreetGroup(), playerStreetGroups.get(street.getStreetGroup()) + 1);
        }

        return playerStreetGroups;
    }

    /*
     * Reduce player balance by given amount.
     */
    public void reducePlayerBalance(int amount) {
        if (getPlayerBalance() >= amount) {
            this.playerBalance = this.playerBalance - amount;
        }
    }

    /*
     * Increase player balance by given amount.
     */
    public void increasePlayerBalance(int amount) {
        this.playerBalance = this.playerBalance + amount;
    }

    public PieceType getPieceType() {
        return pieceType;
    }

    public void setPieceType(PieceType pieceType) {
        this.pieceType = pieceType;
    }

    public PieceColor getPieceColor() {
        return pieceColor;
    }

    public void setPieceColor(PieceColor pieceColor) {
        this.pieceColor = pieceColor;
    }

    @Override
    public String toString() {
        return name;
    }

    /**
     * Function that compares 2 objects and returns if they are the same.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return id.equals(player.id) &&
                name.equals(player.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}

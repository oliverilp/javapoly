package ee.taltech.monopoly.network.requests;

public class StartGame extends Request {
    public StartGame() {
        super();
    }

    public StartGame(String gameID) {
        super(gameID);
    }
}

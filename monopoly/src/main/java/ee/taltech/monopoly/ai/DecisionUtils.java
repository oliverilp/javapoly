package ee.taltech.monopoly.ai;

import ee.taltech.monopoly.gamelogic.Board;
import ee.taltech.monopoly.gamelogic.Player;
import ee.taltech.monopoly.gamelogic.street.Street;
import ee.taltech.monopoly.gamelogic.street.StreetGroup;
import ee.taltech.monopoly.gamelogic.street.purchasable.PurchasableStreet;

class DecisionUtils {
    /**
     * Calculate how many streets from the street group of `street`
     * are owned by `player` or have not yet been bought.
     */
    static int getNumberOfStreetsInStreetGroupNotOwnedByOtherPlayers(Board board, Street street, Player player) {
        final StreetGroup streetGroup = street.getStreetGroup();
        if (board.getStreets().stream()
                .filter(s -> s.getStreetGroup().equals(streetGroup))
                .anyMatch(s -> isStreetOwnedByOtherPlayers(player, (PurchasableStreet) s, board))) {
            return 0;
        }

        return player.getPlayerStreetGroups(board).get(streetGroup);
    }

    /**
     * Return true if the street is owned by somebody else than `player`,
     * false if the street is owned by the `player` or if it is not yet bought.
     */
    static boolean isStreetOwnedByOtherPlayers(Player player, PurchasableStreet street, Board board) {
        return street.getOwner(board) != null && !street.getOwner(board).equals(player);
    }
}

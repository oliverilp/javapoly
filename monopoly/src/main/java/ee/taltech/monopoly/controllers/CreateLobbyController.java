package ee.taltech.monopoly.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import ee.taltech.monopoly.Game;
import ee.taltech.monopoly.services.GameInitializer;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.VBox;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Create a new game and direct player to lobby.
 */
public class CreateLobbyController implements Initializable {
    private static Double width = 1050.0;
    private static Double height = 800.0;

    @FXML
    private JFXTextField playerNameTextField;
    @FXML
    private JFXButton joinButton;
    @FXML
    private VBox vBox;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        vBox.setPrefWidth(width);
        vBox.setPrefHeight(height);

        joinButton.setOnAction(e -> {
            GameInitializer.createGame(playerNameTextField.getText());
            Game.loadStartGame(vBox.getWidth(), vBox.getHeight());
            Game.getStage().setScene(Game.getStartGame());
        });
    }

    public static void setWidth(Double width) {
        CreateLobbyController.width = width;
    }

    public static void setHeight(Double height) {
        CreateLobbyController.height = height;
    }
}

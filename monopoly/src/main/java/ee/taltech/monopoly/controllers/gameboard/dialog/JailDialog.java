package ee.taltech.monopoly.controllers.gameboard.dialog;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import ee.taltech.monopoly.controllers.gameboard.GameBoardController;
import ee.taltech.monopoly.gamelogic.Board;
import ee.taltech.monopoly.gamelogic.Player;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;

public class JailDialog {
    public static void show(String heading, StackPane rootStackPane) {
        Player currentPlayer = Board.getInstance().getCurrentPlayer();

        JFXDialogLayout content = new JFXDialogLayout();
        content.setHeading(new Label(heading));
        Label titleLabel = new Label("Kas sa tahad oodata või maksta 50 tundi, et dekanaadist välja saada?");
        titleLabel.setPadding(new Insets(0, 0, 30, 0));
        content.setBody(titleLabel);

        JFXDialog dialog = new JFXDialog(rootStackPane, content, JFXDialog.DialogTransition.CENTER);
        dialog.setOverlayClose(false);

        JFXButton payButton = new JFXButton("MAKSA");
        payButton.setOnAction(e -> {
            currentPlayer.payJailFine();
            GameBoardController.getInstance().getVBox().setDisable(false);
            dialog.close();
        });

        JFXButton waitButton = new JFXButton("OOTA");
        waitButton.setOnAction(e -> {
            currentPlayer.decreaseJailCounter();
            if (!currentPlayer.isInJail()) {
                GameBoardController.getInstance().getVBox().setDisable(false);
            } else {
                GameBoardController.getInstance().endTurn();
            }
            dialog.close();
        });

        HBox buttonsContainer = new HBox(waitButton);
        buttonsContainer.setSpacing(10);
        if (currentPlayer.getPlayerBalance() >= 50) {
            buttonsContainer.getChildren().add(payButton);
        } else {
            titleLabel.setText("Sa pead nüüd ootama dekanaadis.");
        }
        buttonsContainer.setPadding(new Insets(0, 15, 15, 0));
        content.setActions(buttonsContainer);

        dialog.show();
    }
}

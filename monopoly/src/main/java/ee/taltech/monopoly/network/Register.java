package ee.taltech.monopoly.network;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.EndPoint;
import ee.taltech.monopoly.ai.AIPlayer;
import ee.taltech.monopoly.gamelogic.Board;
import ee.taltech.monopoly.gamelogic.PieceColor;
import ee.taltech.monopoly.gamelogic.PieceType;
import ee.taltech.monopoly.gamelogic.Player;
import ee.taltech.monopoly.gamelogic.cards.Card;
import ee.taltech.monopoly.gamelogic.cards.CardDeck;
import ee.taltech.monopoly.gamelogic.street.Street;
import ee.taltech.monopoly.gamelogic.street.StreetGroup;
import ee.taltech.monopoly.gamelogic.street.nonpurchasable.ChanceAndCommChestStreet;
import ee.taltech.monopoly.gamelogic.street.nonpurchasable.GoStreet;
import ee.taltech.monopoly.gamelogic.street.nonpurchasable.GoToJailStreet;
import ee.taltech.monopoly.gamelogic.street.nonpurchasable.JailStreet;
import ee.taltech.monopoly.gamelogic.street.nonpurchasable.NonPurchasableStreet;
import ee.taltech.monopoly.gamelogic.street.nonpurchasable.ParkingStreet;
import ee.taltech.monopoly.gamelogic.street.nonpurchasable.TaxStreet;
import ee.taltech.monopoly.gamelogic.street.purchasable.PurchasableStreet;
import ee.taltech.monopoly.gamelogic.street.purchasable.RailroadStreet;
import ee.taltech.monopoly.gamelogic.street.purchasable.RegularStreet;
import ee.taltech.monopoly.gamelogic.street.purchasable.UtilityStreet;
import ee.taltech.monopoly.network.requests.*;
import ee.taltech.monopoly.network.viewmodels.BoardViewModel;
import ee.taltech.monopoly.network.viewmodels.PlayersViewModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class Register {
    public static final int PORT = 5201;

    public static void register(EndPoint endPoint) {
        Kryo kryo = endPoint.getKryo();

        kryo.register(Map.class);
        kryo.register(HashMap.class);
        kryo.register(Set.class);
        kryo.register(HashSet.class);
        kryo.register(List.class);
        kryo.register(ArrayList.class);
        kryo.register(String.class);
        kryo.register(Integer.class);
        kryo.register(Collectors.class);

        //noinspection ArraysAsListWithZeroOrOneArgument
        kryo.register(Arrays.asList("").getClass());
        kryo.register(Collections.EMPTY_LIST.getClass());
        kryo.register(Collections.EMPTY_MAP.getClass());
        kryo.register(Collections.EMPTY_SET.getClass());
        kryo.register(Collections.singletonList("").getClass());
        kryo.register(Collections.singleton("").getClass());
        kryo.register(Collections.singletonMap("", "").getClass());

        kryo.register(Street.class);
        kryo.register(StreetGroup.class);
        kryo.register(NonPurchasableStreet.class);
        kryo.register(ChanceAndCommChestStreet.class);
        kryo.register(GoStreet.class);
        kryo.register(GoToJailStreet.class);
        kryo.register(JailStreet.class);
        kryo.register(ParkingStreet.class);
        kryo.register(TaxStreet.class);
        kryo.register(PurchasableStreet.class);
        kryo.register(RailroadStreet.class);
        kryo.register(RegularStreet.class);
        kryo.register(UtilityStreet.class);
        kryo.register(Card.class);
        kryo.register(CardDeck.class);
        kryo.register(CardDeck.Type.class);

        kryo.register(Player.class);
        kryo.register(AIPlayer.class);
        kryo.register(PieceType.class);
        kryo.register(PieceColor.class);
        kryo.register(Board.class);

        kryo.register(Request.class);
        kryo.register(GamePhase.class);
        kryo.register(PlayersViewModel.class);
        kryo.register(JoinGame.class);
        kryo.register(CreateGame.class);
        kryo.register(FetchPlayers.class);
        kryo.register(StartGame.class);
        kryo.register(FetchBoard.class);
        kryo.register(PushBoard.class);
        kryo.register(BoardViewModel.class);
        kryo.register(TradeRequest.class);
        kryo.register(TradeReply.class);

        for (int i = 0; i < 5; i++) {
            System.out.println();
        }
    }
}

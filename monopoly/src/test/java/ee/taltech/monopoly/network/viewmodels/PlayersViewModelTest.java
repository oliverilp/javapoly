package ee.taltech.monopoly.network.viewmodels;

import ee.taltech.monopoly.gamelogic.Player;
import ee.taltech.monopoly.network.GamePhase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

class PlayersViewModelTest {
    PlayersViewModel playersViewModel;

    @BeforeEach
    void setUp() {
        playersViewModel = new PlayersViewModel();
        List<Player> playerList = Arrays.asList(new Player(), new Player(), new Player());

        PlayersViewModel playersViewModel1 = new PlayersViewModel();
        playersViewModel1.setPhase(GamePhase.PLAYERS_GATHERING);
        playersViewModel1.setPlayerList(playerList);

        PlayersViewModel playersViewModel2 = new PlayersViewModel(playersViewModel1);
        assertEquals(playerList, playersViewModel2.getPlayerList());
        assertEquals(GamePhase.PLAYERS_GATHERING, playersViewModel2.getPhase());
    }

    @Test
    void getPhase() {
        assertEquals(GamePhase.PLAYERS_GATHERING, playersViewModel.getPhase());
    }

    @Test
    void setPhase() {
        playersViewModel.setPhase(GamePhase.IN_PROGRESS);
        assertEquals(GamePhase.IN_PROGRESS, playersViewModel.getPhase());
    }

    @Test
    void getPlayerList() {
        assertNotNull(playersViewModel.getPlayerList());
        assertTrue(playersViewModel.getPlayerList().isEmpty());
    }

    @Test
    void getPlayerNames() {
        assertTrue(playersViewModel.getPlayerNames().isEmpty());
    }

    @Test
    void setPlayerList() {
        List<Player> playerList = Arrays.asList(new Player(), new Player(), new Player());
        playersViewModel.setPlayerList(playerList);
        assertEquals(3, playersViewModel.getPlayerList().size());
    }
}

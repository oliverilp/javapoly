package ee.taltech.monopoly.network;

import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import ee.taltech.monopoly.controllers.gameboard.GameBoardController;
import ee.taltech.monopoly.controllers.gameboard.dialog.TradeDialog;
import ee.taltech.monopoly.gamelogic.Board;
import ee.taltech.monopoly.gamelogic.Player;
import ee.taltech.monopoly.network.requests.*;
import ee.taltech.monopoly.network.viewmodels.BoardViewModel;
import ee.taltech.monopoly.network.viewmodels.PlayersViewModel;
import javafx.application.Platform;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ServerApi {
    private static final ServerApi instance = new ServerApi();
    private final Client client;
    private volatile PlayersViewModel playersViewModel;
    private volatile BoardViewModel boardViewModel;
    private boolean waitingForTradeReply = false;

    public ServerApi() {
        client = new Client(1000000, 1000000);
        Register.register(client);

        client.addListener(new Listener() {
            public void received(Connection connection, Object object) {
                if (object instanceof PlayersViewModel) {
                    playersViewModel = (PlayersViewModel) object;
                }

                if (object instanceof BoardViewModel) {
                    boardViewModel = (BoardViewModel) object;
                }

                if (object instanceof TradeRequest) {
                    TradeRequest tradeRequest = (TradeRequest) object;
                    Platform.runLater(() -> {
                        TradeDialog tradeDialog = new TradeDialog(GameBoardController.getInstance().getRootStackPane(),
                                tradeRequest.getSendingPlayer(), tradeRequest.getReceivingPlayer(),
                                tradeRequest.getSenderStreets(), tradeRequest.getReceiverStreets(),
                                tradeRequest.getSenderMoney(), tradeRequest.getReceiverMoney());
                        tradeDialog.showOfferDialog();
                    });
                }

                if (object instanceof TradeReply && waitingForTradeReply) {
                    waitingForTradeReply = false;
                    TradeReply tradeReply = (TradeReply) object;
                    Platform.runLater(() -> GameBoardController.getInstance()
                            .getTradeDialog()
                            .tradeResult(tradeReply.isAccepted()));
                }
            }
        });

        client.start();

        try {
            // NB! change "193.40.255.29" to "localhost" for development
            client.connect(5000, "193.40.255.29", Register.PORT);
        } catch (IOException ex) {
            ex.printStackTrace();
            System.exit(1);
        }
    }

    private void sendAndWait(Object object) {
        client.sendTCP(object);
        while (playersViewModel == null) {
            Thread.onSpinWait();
        }
    }

    public List<Player> joinGame(String gameID, Player player) {
        sendAndWait(new JoinGame(gameID, player));

        List<Player> playerList = new ArrayList<>(playersViewModel.getPlayerList());
        playersViewModel = null;
        return playerList;
    }

    public void createGame(String gameID, Player player) {
        client.sendTCP(new CreateGame(gameID, player));
    }

    public PlayersViewModel fetchPlayers() {
        sendAndWait(new FetchPlayers(Board.getInstance().getGameID()));

        PlayersViewModel playersViewModelCopy = new PlayersViewModel(playersViewModel);
        playersViewModel = null;
        return playersViewModelCopy;
    }

    public void startGame() {
        client.sendTCP(new StartGame(Board.getInstance().getGameID()));
    }

    public Board fetchBoard() {
        client.sendTCP(new FetchBoard(Board.getInstance().getGameID()));
        while (boardViewModel == null) {
            Thread.onSpinWait();
        }

        Board board = boardViewModel.getBoard();
        boardViewModel = null;
        return board;
    }

    public void pushBoard(Board board) {
        client.sendTCP(new PushBoard(board.getGameID(), board));
    }

    public void tradeRequest(TradeRequest tradeRequest) {
        waitingForTradeReply = true;
        client.sendTCP(tradeRequest);
    }

    public void tradeReply(TradeReply tradeReply) {
        client.sendTCP(tradeReply);
    }

    public static ServerApi getInstance() {
        return instance;
    }
}

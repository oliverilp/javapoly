package ee.taltech.monopoly.controllers.gameboard;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXSnackbar;
import com.jfoenix.controls.JFXSnackbarLayout;
import ee.taltech.monopoly.Game;
import ee.taltech.monopoly.controllers.gameboard.dialog.CommChestDialog;
import ee.taltech.monopoly.controllers.gameboard.dialog.JailDialog;
import ee.taltech.monopoly.controllers.gameboard.dialog.SimpleDialog;
import ee.taltech.monopoly.controllers.gameboard.dialog.TradeDialog;
import ee.taltech.monopoly.gamelogic.Board;
import ee.taltech.monopoly.gamelogic.Dice;
import ee.taltech.monopoly.gamelogic.Player;
import ee.taltech.monopoly.gamelogic.cards.CardDeck;
import ee.taltech.monopoly.gamelogic.street.Street;
import ee.taltech.monopoly.gamelogic.street.nonpurchasable.GoToJailStreet;
import ee.taltech.monopoly.gamelogic.street.nonpurchasable.TaxStreet;
import ee.taltech.monopoly.gamelogic.street.purchasable.PurchasableStreet;
import ee.taltech.monopoly.gamelogic.street.purchasable.RegularStreet;
import ee.taltech.monopoly.network.ServerApi;
import ee.taltech.monopoly.services.BoardPollerThread;
import javafx.animation.*;
import javafx.application.Platform;
import javafx.concurrent.Worker;
import javafx.fxml.Initializable;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.web.WebEngine;
import javafx.util.Duration;

import java.net.URL;
import java.util.*;
import java.util.List;

public class GameBoardController extends GameBoardFxmlData implements Initializable {
    private static GameBoardController instance;

    private static Double width = 1050.0;
    private static Double height = 800.0;

    private Map<Integer, TilePane> tiles;
    private Map<Integer, Region> streetColorRegions;
    private Map<Integer, Pane> housesContainers;
    private Map<Integer, StackPane> streetStackPanes;
    private Map<String, PlayerViewModel> players = new HashMap<>();
    private Map<StackPane, Region> houseOverlayRegions = new HashMap<>();

    private static final Duration FADE_TRANSITION_DURATION = Duration.millis(500);
    private static final Duration SCALE_TRANSITION_DURATION = Duration.millis(250);
    private static final Duration SNACKBAR_VISIBLE_DURATION = Duration.millis(2500);

    private DiceAnimation diceAnimation;
    private TradeDialog tradeDialog;
    private List<JFXButton> tradeButtons = new ArrayList<>();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        instance = this;
        rootStackPane.setPrefWidth(width);
        rootStackPane.setPrefHeight(height);

        tiles = getTiles();
        streetColorRegions = getStreetColorRegions();
        housesContainers = getHousesContainers();
        streetStackPanes = getStreetStackPanes();
        diceAnimation = new DiceAnimation(leftDiceStackPane, rightDiceStackPane, SCALE_TRANSITION_DURATION);

        final Player currentPlayer = Board.getInstance().getCurrentPlayer();
        final Player localPlayer = Board.getInstance().getLocalPlayer();
        if (!currentPlayer.equals(localPlayer)) {
            buttonsVBox.setDisable(true);
            Platform.runLater(() -> {
                Thread poller = new BoardPollerThread();
                poller.start();
            });
        }

        initWebView();
        initAllPlayersData();
        addHBoxListeners();

        purchaseStreetButton.setOnAction(e -> purchaseStreet());
        purchaseHouseButton.setOnAction(e -> selectHouses());
        moveButton.setOnAction(e -> moveLocalPlayer());
        endTurnButton.setOnAction(e -> endTurn());
    }

    private void initWebView() {
        WebEngine engine = webView.getEngine();
        engine.load(Game.class.getResource("board_background.html").toString());

        engine.getLoadWorker().stateProperty().addListener((obs, oldState, newState) -> {
            if (newState == Worker.State.SUCCEEDED) {
                FadeTransition ft = new FadeTransition(FADE_TRANSITION_DURATION, webView);
                ft.setInterpolator(Interpolator.EASE_BOTH);
                ft.setFromValue(0.0);
                ft.setToValue(1.0);
                ft.play();
                webView.setVisible(true);
            }
        });
    }

    private void addHBoxListeners() {
        hBox.heightProperty().addListener((obs, oldVal, newVal) -> {
            double value = (Double) newVal;
            boolean condition = (hBox.getWidth() - leftGridPane.getWidth() - rightPlayersInfoVBox.getWidth()) >= value;
            setHBoxChildrenSize(value, condition);
        });

        hBox.widthProperty().addListener((obs, oldVal, newVal) -> {
            double value = (Double) newVal - leftGridPane.getWidth() - rightPlayersInfoVBox.getWidth();
            setHBoxChildrenSize(value, hBox.getHeight() >= value);
        });
    }

    private void setHBoxChildrenSize(Double value, boolean condition) {
        if (condition) {
            webView.setMaxWidth(value);
            webView.setMaxHeight(value);
            imageView.setFitWidth(value * 0.45);

            gridPane.setMaxWidth(value);
            gridPane.setMinWidth(value);
            gridPane.setMaxHeight(value);
        }
    }

    private void addPlayerInfo(PlayerViewModel playerViewModel, Player player) {
        StackPane stackPane = new StackPane();
        VBox insideRectangle = new VBox();
        insideRectangle.setAlignment(Pos.CENTER);
        VBox.setMargin(stackPane, new Insets(20, 0, 0, 0));

        Rectangle rectangle = new Rectangle(220, 80, Color.valueOf(player.getPieceColor().toString()));
        rectangle.setStyle("-fx-effect: dropshadow(gaussian, rgba(66,66,66, 0.4), 15, 0, 0, 0);");
        rectangle.setArcWidth(12);
        rectangle.setArcHeight(12);

        GridPane gridPane = new GridPane();
        gridPane.setAlignment(Pos.CENTER);
        ColumnConstraints column = new ColumnConstraints();
        column.setPercentWidth(40);
        gridPane.getColumnConstraints().addAll(column, column);
        stackPane.getChildren().addAll(rectangle, insideRectangle);
        insideRectangle.getChildren().addAll(gridPane);

        Label nameLabel = new Label(player.getName());
        GridPane.setHalignment(nameLabel, HPos.LEFT);
        Label balanceLabel = new Label(player.getPlayerBalance() + " tundi");
        GridPane.setHalignment(balanceLabel, HPos.RIGHT);
        playerViewModel.setBalanceLabel(balanceLabel);
        gridPane.add(nameLabel, 0, 0);
        gridPane.add(balanceLabel, 1, 0);

        if (!player.equals(Board.getInstance().getLocalPlayer())) {
            gridPane.setPadding(new Insets(0, 0, 10, 0));

            JFXButton button = new JFXButton("KAUPLE");
            button.setDisable(true);
            button.setDefaultButton(false);
            button.setOnAction(e -> {
                Player newPlayer = Board.getInstance().getPlayer(player);
                tradeDialog = new TradeDialog(rootStackPane, Board.getInstance().getLocalPlayer(), newPlayer);
                tradeDialog.makeOfferDialog();
            });
            insideRectangle.getChildren().add(button);
            tradeButtons.add(button);
        }

        rightPlayersInfoVBox.getChildren().add(stackPane);
    }

    public void startTurn() {
        Player currentPlayer = Board.getInstance().getCurrentPlayer();

        buttonsVBox.setDisable(currentPlayer.isInJail());
        purchaseStreetButton.setDisable(true);
        updatePurchaseHouseButton();
        moveButton.setDisable(false);
        endTurnButton.setDisable(true);
        tradeButtons.forEach(button -> button.setDisable(false));

        if (currentPlayer.isInJail()) {
            JailDialog.show(currentPlayer.getLocation(Board.getInstance()).getName(), rootStackPane);
        }
    }

    public void endTurn() {
        Player currentPlayer = Board.getInstance().getCurrentPlayer();
        updateLocalPlayerData(currentPlayer, currentPlayer.getLocation(Board.getInstance()));
        sendUpdates();
        endTurnButton.setDisable(true);
        buttonsVBox.setDisable(true);
        tradeButtons.forEach(button -> button.setDisable(true));

        diceAnimation.playEndAnimation();
    }

    private boolean canBuyHouses(Street street, Player player) {
        return street != null
                && street.getOwner(Board.getInstance()) != null
                && street.getOwner(Board.getInstance()).equals(player)
                && street.canBuyHouses(Board.getInstance());
    }

    private boolean canBuyAnyHouse(Player player) {
        return Board.getInstance().getStreets().stream()
                .anyMatch(street -> canBuyHouses(street, player));
    }

    private void setPlayerStreetColorRegion(Player player, Street street) {
        Region region = streetColorRegions.get(street.getIndex());
        region.setVisible(true);
        region.setStyle("-fx-background-color: " + player.getPieceColor().toString() + "; " +
                "-fx-background-radius: 30 30 0 0;");
    }

    private void purchaseStreet() {
        Player currentPlayer = Board.getInstance().getCurrentPlayer();
        Street currentStreet = currentPlayer.getLocation(Board.getInstance());

        if (currentStreet != null && currentStreet.canBuy(currentPlayer, Board.getInstance())) {
            ((PurchasableStreet) currentStreet).purchaseStreet(currentPlayer, Board.getInstance());
            setPlayerStreetColorRegion(currentPlayer, currentStreet);

            purchaseStreetButton.setDisable(true);
            updatePlayerBalance(currentPlayer);
            if (canBuyHouses(currentStreet, currentPlayer)) {
                purchaseHouseButton.setVisible(true);
            }
        }
    }

    private void hideHouseOverlay() {
        for (Map.Entry<StackPane, Region> entry : houseOverlayRegions.entrySet()) {
            entry.getKey().getChildren().remove(entry.getValue());
            entry.getValue().setVisible(false);
        }
        houseOverlayRegions.clear();
        buttonsVBox.setDisable(false);
    }

    public void updatePurchaseHouseButton() {
        purchaseHouseButton.setVisible(canBuyAnyHouse(Board.getInstance().getCurrentPlayer()));
    }

    private void purchaseHouse(Street street) {
        Player currentPlayer = Board.getInstance().getCurrentPlayer();

        if (canBuyHouses(street, currentPlayer)) {
            ((RegularStreet) street).buyHouse(Board.getInstance());
            Pane container = housesContainers.get(street.getIndex());
            container.getChildren().add(new Rectangle(14, 14, Color.BLACK));
            updatePlayerBalance(currentPlayer);
            if (!canBuyAnyHouse(currentPlayer)) {
                hideHouseOverlay();
                purchaseHouseButton.setVisible(false);
            } else if (!street.canBuyHouses(Board.getInstance())) {
                StackPane streetStackPane = streetStackPanes.get(street.getIndex());
                Region region = houseOverlayRegions.get(streetStackPane);
                streetStackPane.getChildren().remove(region);
                region.setVisible(false);
            }
        }
    }

    private boolean intersects(double x, double y) {
        return houseOverlayRegions.entrySet().stream()
                .anyMatch(entry -> entry.getKey().getBoundsInParent().intersects(x, y, 1, 1));
    }

    private void selectHouses() {
        Player currentPlayer = Board.getInstance().getCurrentPlayer();
        buttonsVBox.setDisable(true);
        rootStackPane.setOnMouseClicked(e -> {
            if (!intersects(e.getSceneX() - leftGridPane.getWidth(), e.getSceneY())) {
                hideHouseOverlay();
                rootStackPane.setOnMouseClicked(null);
            }
        });

        for (Street street : currentPlayer.getPlayerStreets(Board.getInstance())) {
            if (street.canBuyHouses(Board.getInstance())) {
                Region region = new Region();
                region.setStyle("-fx-background-color: white; -fx-opacity: 0.7");
                region.setOnMouseClicked(e -> purchaseHouse(street));

                StackPane streetStackPane = streetStackPanes.get(street.getIndex());
                streetStackPane.getChildren().add(0, region);
                houseOverlayRegions.put(streetStackPane, region);
            }
        }
    }

    private void initAllPlayersData() {
        List<Player> allPlayers = Board.getInstance().getAllPlayers();

        for (Player player : allPlayers) {
            IconCreator iconCreator = new IconCreator(player.getPieceColor().toString());
            StackPane icon = iconCreator.getIcon(player.getPieceType());
            PlayerViewModel playerViewModel = new PlayerViewModel(icon, tilePane0);
            players.put(player.getID(), playerViewModel);
            tilePane0.getChildren().add(icon);
            addPlayerInfo(playerViewModel, player);
        }

        currentStreetName.setText(Board.getInstance().getLocalPlayer().getLocation(Board.getInstance()).getName());
        currentPlayerName.setText(Board.getInstance().getCurrentPlayer().getName());
        purchaseStreetButton.setDisable(true);
        purchaseHouseButton.setVisible(false);
        endTurnButton.setDisable(true);
    }

    private void updatePlayerBalance(Player player) {
        PlayerViewModel playerViewModel = players.get(player.getID());
        playerViewModel.getBalanceLabel().setText(player.getPlayerBalance() + " tundi");
    }

    public void updateAllPlayersData() {
        currentPlayerName.setText(Board.getInstance().getCurrentPlayer().getName());
        List<Player> allPlayers = Board.getInstance().getAllPlayers();

        for (Player player : allPlayers) {
            updatePlayerLocation(player);
            updatePlayerBalance(player);

            for (Street street : player.getPlayerStreets(Board.getInstance())) {
                setPlayerStreetColorRegion(player, street);

                if (street instanceof RegularStreet) {
                    RegularStreet regularStreet = ((RegularStreet) street);
                    Pane container = housesContainers.get(regularStreet.getIndex());
                    int difference = regularStreet.getStreetBuildings().get("Houses") - container.getChildren().size();
                    for (int i = 0; i < difference; i++) {
                        container.getChildren().add(new Rectangle(14, 14, Color.BLACK));
                    }
                }
            }
        }
    }

    private void updatePlayerLocation(Player player) {
        PlayerViewModel playerViewModel = players.get(player.getID());
        playerViewModel.getCurrentTileLocation()
                .getChildren()
                .remove(playerViewModel.getPlayerIcon());
        tiles.get(player.getLocation(Board.getInstance()).getIndex())
                .getChildren()
                .add(playerViewModel.getPlayerIcon());
        playerViewModel.setCurrentTileLocation(tiles.get(player.getLocation(Board.getInstance()).getIndex()));
    }

    private void showSnackbar(String message) {
        JFXSnackbar snackbar = new JFXSnackbar(snackBarPane);
        JFXSnackbarLayout snackbarLayout = new JFXSnackbarLayout(message);
        snackbar.enqueue(new JFXSnackbar.SnackbarEvent(snackbarLayout, SNACKBAR_VISIBLE_DURATION));
    }

    private void payRent(Street street, Player player, int diceRoll) {
        if (street instanceof PurchasableStreet
                && street.getOwner(Board.getInstance()) != null
                && !street.getOwner(Board.getInstance()).equals(player)) {
            String msg = ((PurchasableStreet) street).payRent(player, diceRoll, Board.getInstance());
            showSnackbar(msg);
        }
    }

    private void payTax(Street street, Player player) {
        if (street instanceof TaxStreet) {
            String msg = ((TaxStreet) street).payTax(player);
            SimpleDialog.show(street.getName(), msg, rootStackPane);
        }
    }

    private void goToJailStreet(Street street, Player player) {
        if (street instanceof GoToJailStreet) {
            String msg = player.enterJail(Board.getInstance(), false);
            SimpleDialog.show(street.getName(), msg, rootStackPane);
        }
    }

    public void updateLocalPlayerData(Player localPlayer, Street street) {
        updatePlayerLocation(localPlayer);
        currentStreetName.setText(street.getName());
        updatePlayerBalance(localPlayer);
    }

    private int rollDice() {
        int diceRollLeft = Dice.rollSingle();
        int diceRollRight = Dice.rollSingle();
        diceAnimation.playBeginningAnimation(diceRollLeft, diceRollRight);

        return diceRollLeft + diceRollRight;
    }

    private void moveLocalPlayer() {
        Player currentPlayer = Board.getInstance().getCurrentPlayer();
        int diceRoll = rollDice();
        int indexBefore = currentPlayer.getLocation(Board.getInstance()).getIndex();
        Street destinationStreet = Board.getInstance().move(currentPlayer, diceRoll);
        int indexAfter = currentPlayer.getLocation(Board.getInstance()).getIndex();
        if (indexBefore >= indexAfter) {
            currentPlayer.increasePlayerBalance(200);
        }

        payRent(destinationStreet, currentPlayer, diceRoll);
        payTax(destinationStreet, currentPlayer);
        goToJailStreet(destinationStreet, currentPlayer);
        updateLocalPlayerData(currentPlayer, destinationStreet);
        purchaseStreetButton.setDisable(!destinationStreet.canBuy(currentPlayer, Board.getInstance()));
        moveButton.setDisable(true);
        endTurnButton.setDisable(false);

        CardDeck cardDeck = new CardDeck();
        if (destinationStreet.getName().equals("Juhus")) {
            String outcome = cardDeck.drawChanceCard(currentPlayer, Board.getInstance());
            SimpleDialog.show(destinationStreet.getName(), outcome, rootStackPane);
            updateLocalPlayerData(currentPlayer, destinationStreet);
        } else if (destinationStreet.getName().equals("Tunnikontroll")) {
            CommChestDialog.show(currentPlayer, cardDeck, rootStackPane, destinationStreet.getName());
        }
    }

    void sendUpdates() {
        Board.getInstance().incrementCurrentPlayer();
        ServerApi.getInstance().pushBoard(Board.getInstance());
        Thread poller = new BoardPollerThread();
        poller.start();
    }

    public StackPane getRootStackPane() {
        return rootStackPane;
    }

    public TradeDialog getTradeDialog() {
        return tradeDialog;
    }

    public static GameBoardController getInstance() {
        return instance;
    }

    public static void setWidth(Double width) {
        GameBoardController.width = width;
    }

    public static void setHeight(Double height) {
        GameBoardController.height = height;
    }
}

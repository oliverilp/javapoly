package ee.taltech.monopoly.controllers.gameboard;

import ee.taltech.monopoly.gamelogic.PieceType;
import javafx.scene.layout.StackPane;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class IconCreatorTest {
    private IconCreator iconCreator;

    @BeforeEach
    void setUp() {
        iconCreator = new IconCreator("#eeeeee");
    }

    @Test
    void getIcon() {
        assertNotNull(iconCreator.getIcon(PieceType.ARROW));
        assertNotNull(iconCreator.getIcon(PieceType.CAR));
        assertNotNull(iconCreator.getIcon(PieceType.KEY));
        assertNotNull(iconCreator.getIcon(PieceType.LOCK));
        assertNotNull(iconCreator.getIcon(PieceType.PIN));
        assertNotNull(iconCreator.getIcon(PieceType.SHOPPING_CART));
        assertNotNull(iconCreator.getIcon(PieceType.TRUCK));
        assertNotNull(iconCreator.getIcon(PieceType.AIRPLANE));
    }
}

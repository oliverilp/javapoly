package ee.taltech.monopoly.controllers.gameboard.dialog;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.JFXRadioButton;
import ee.taltech.monopoly.controllers.gameboard.GameBoardController;
import ee.taltech.monopoly.gamelogic.Board;
import ee.taltech.monopoly.gamelogic.Player;
import ee.taltech.monopoly.gamelogic.cards.Card;
import ee.taltech.monopoly.gamelogic.cards.CardDeck;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

public class CommChestDialog {
    private static void addRadioButtons(Card card, ToggleGroup group, JFXButton answerButton, VBox radioVBox) {
        for (String option : card.getAnswerChoices()) {
            JFXRadioButton radioButton = new JFXRadioButton(option);
            radioButton.setPadding(new Insets(15, 0, 0, 0));
            radioButton.setToggleGroup(group);
            radioButton.setOnAction(e -> answerButton.setDisable(false));
            radioVBox.getChildren().add(radioButton);
        }
    }

    private static Label getResponseLabel(boolean isCorrect) {
        Label label;
        if (isCorrect) {
            label = new Label("Su vastus on õige!");
            label.setTextFill(Color.GREEN);
        } else {
            label = new Label("Su vastus on vale!");
            label.setTextFill(Color.RED);
        }
        label.setPadding(new Insets(0, 0, 10, 0));
        return label;
    }

    private static void replaceAnswerButton(JFXDialog dialog, StackPane buttonContainer, JFXButton answerButton) {
        JFXButton okButton = new JFXButton("OK");
        okButton.setOnAction(e -> dialog.close());
        buttonContainer.getChildren().remove(answerButton);
        buttonContainer.getChildren().add(okButton);
    }

    public static void show(Player currentPlayer, CardDeck cardDeck, StackPane rootStackPane,
                            String heading) {
        Card card = cardDeck.drawCommunityCard();

        JFXDialogLayout content = new JFXDialogLayout();
        content.setHeading(new Label(heading));
        VBox radioVBox = new VBox();
        VBox contentVBox = new VBox();

        Label questionLabel = new Label(card.getQuestion());
        questionLabel.setTextFill(Color.BLACK);
        questionLabel.setPadding(new Insets(0, 0, 20, 0));

        contentVBox.getChildren().addAll(questionLabel, radioVBox);
        JFXButton answerButton = new JFXButton("VASTA");
        answerButton.setDisable(true);

        ToggleGroup group = new ToggleGroup();
        addRadioButtons(card, group, answerButton, radioVBox);

        content.setBody(contentVBox);
        JFXDialog dialog = new JFXDialog(rootStackPane, content, JFXDialog.DialogTransition.CENTER);
        dialog.setOverlayClose(false);
        StackPane buttonContainer = new StackPane(answerButton);
        buttonContainer.setPadding(new Insets(0, 15, 15, 0));

        answerButton.setOnAction(e -> {
            JFXRadioButton radioButton = (JFXRadioButton) group.getSelectedToggle();
            boolean isCorrect = radioButton.getText().equals(card.getAnswer());
            Label responseLabel = getResponseLabel(isCorrect);
            String outcome = cardDeck.communityResult(currentPlayer, Board.getInstance(), isCorrect);

            contentVBox.getChildren().addAll(responseLabel, new Label(outcome));
            replaceAnswerButton(dialog, buttonContainer, answerButton);

            dialog.setOverlayClose(true);
            radioVBox.setDisable(true);
            GameBoardController.getInstance().updateLocalPlayerData(currentPlayer,
                    currentPlayer.getLocation(Board.getInstance()));
        });

        content.setActions(buttonContainer);
        dialog.show();
    }
}

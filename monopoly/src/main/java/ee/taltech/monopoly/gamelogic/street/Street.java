package ee.taltech.monopoly.gamelogic.street;

import ee.taltech.monopoly.gamelogic.Board;
import ee.taltech.monopoly.gamelogic.Player;

public interface Street {

    /**
     * @return the StreetGroup that determines various things for other functions.
     */
    default StreetGroup getStreetGroup() {
        return null;
    }

    /*
     * Return player who owns street if it is a PurchasableStreet, else null by default.
     */
    default Player getOwner(Board board) {
        return null;
    }

    default boolean canBuy(Player player, Board board) {
        return false;
    }

    default boolean canBuyHouses(Board board) {
        return false;
    }

    default boolean canSellHouses(Board board) {
        return false;
    }

    String getName();

    int getIndex();
}

package ee.taltech.monopoly.network.requests;

import ee.taltech.monopoly.gamelogic.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class TradeRequestTest {
    TradeRequest tradeRequestEmpty = new TradeRequest();
    TradeRequest tradeRequest;
    Player player1 = new Player();
    Player player2 = new Player();
    Map<String, Integer> streets1 = new HashMap<>();
    Map<String, Integer> streets2 = new HashMap<>();

    @BeforeEach
    void setUp() {
        tradeRequest = new TradeRequest("id", player1, player2, streets1, streets2, 1, 2);
    }

    @Test
    void getPlayer1() {
        assertEquals(player1, tradeRequest.getSendingPlayer());
    }

    @Test
    void getPlayer2() {
        assertEquals(player2, tradeRequest.getReceivingPlayer());
    }

    @Test
    void getStreets1() {
        assertEquals(streets1, tradeRequest.getSenderStreets());
    }

    @Test
    void getStreets2() {
        assertEquals(streets2, tradeRequest.getReceiverStreets());
    }

    @Test
    void getMoney1() {
        assertEquals(1, tradeRequest.getSenderMoney());
    }

    @Test
    void getMoney2() {
        assertEquals(2, tradeRequest.getReceiverMoney());
    }
}

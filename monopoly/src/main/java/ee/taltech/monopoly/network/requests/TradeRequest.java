package ee.taltech.monopoly.network.requests;

import ee.taltech.monopoly.gamelogic.Player;

import java.util.Map;

/**
 * A proposal sent by `sendingPlayer` to `receivingPlayer`
 * to exchange `senderStreets` and `senderMoney` for
 * `receiverStreets` and `receiverMoney`.
 *
 * `senderStreets` & `receiverStreets`: map of Street name and Street index
 */
public class TradeRequest extends Request{
    Player sendingPlayer;
    Player receivingPlayer;
    Map<String, Integer> senderStreets;
    Map<String, Integer> receiverStreets;
    int senderMoney;
    int receiverMoney;

    public TradeRequest() {}

    public TradeRequest(String gameID, Player sendingPlayer, Player receivingPlayer,
                        Map<String, Integer> senderStreets, Map<String, Integer> receiverStreets,
                        int senderMoney, int receiverMoney) {
        super(gameID);
        this.sendingPlayer = sendingPlayer;
        this.receivingPlayer = receivingPlayer;
        this.senderStreets = senderStreets;
        this.receiverStreets = receiverStreets;
        this.senderMoney = senderMoney;
        this.receiverMoney = receiverMoney;
    }

    public Player getSendingPlayer() {
        return sendingPlayer;
    }

    public Player getReceivingPlayer() {
        return receivingPlayer;
    }

    public Map<String, Integer> getSenderStreets() {
        return senderStreets;
    }

    public Map<String, Integer> getReceiverStreets() {
        return receiverStreets;
    }

    public int getSenderMoney() {
        return senderMoney;
    }

    public int getReceiverMoney() {
        return receiverMoney;
    }
}

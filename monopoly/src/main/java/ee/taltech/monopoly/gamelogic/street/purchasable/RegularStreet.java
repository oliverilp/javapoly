package ee.taltech.monopoly.gamelogic.street.purchasable;

import ee.taltech.monopoly.gamelogic.Board;
import ee.taltech.monopoly.gamelogic.Player;
import ee.taltech.monopoly.gamelogic.street.StreetGroup;

import java.util.HashMap;
import java.util.Map;

public class RegularStreet extends PurchasableStreet {
    protected Map<String, Integer> streetBuildings = new HashMap<>();

    public RegularStreet() {}

    /**
     * Creates Purchasable street based on the real Regular Street streets.
     *
     * @param name street name.
     * @param price set price for street.
     * @param rent set rent for street.
     * @param streetGroup set StreetGroup.
     * @param index location on board.
     */
    public RegularStreet(String name, Integer price, Integer rent, StreetGroup streetGroup, int index) {
        this.name = name;
        this.price = price;
        this.rent = rent;
        this.streetGroup = streetGroup;
        this.streetBuildings.put("Houses", 0);
        this.index = index;
    }

    @Override
    public int getRent(Player payer, int diceRoll, Board board) {
        return streetBuildings.get("Houses") == 1 ? (int) (4 * rent * streetGroup.getRentMultiplier())
                : streetBuildings.get("Houses") == 2 ? (int) (12 * rent * streetGroup.getRentMultiplier())
                : streetBuildings.get("Houses") == 3 ? (int) (28 * rent * streetGroup.getRentMultiplier())
                : streetBuildings.get("Houses") == 4 ? (int) (34 * rent* streetGroup.getRentMultiplier())
                : rent;
    }

    public Map<String, Integer> getStreetBuildings() {
        return streetBuildings;
    }

    /*
     * Determines if the a house can be purchased for the street.
     * Has owner and owner has enough money and there are not more than 4 houses.
     */
    @Override
    public boolean canBuyHouses(Board board) {
        if (hasOwnerAndEnoughMoney(board) && notTooManyHouses()) {
            if (streetGroup.equals(StreetGroup.BROWN) || streetGroup.equals(StreetGroup.DARK_BLUE)) {
                return getOwner(board).getPlayerStreetGroups(board).get(streetGroup) == 2;
            } else {
                return getOwner(board).getPlayerStreetGroups(board).get(streetGroup) == 3;
            }
        }
        return false;
    }

    /*
     * Determines if the a house can be sold on the street.
     * Has owner and has at least 1 house.
     */
    @Override
    public boolean canSellHouses(Board board) {
        return getOwner(board) != null && streetBuildings.get("Houses") > 0;
    }

    private boolean notTooManyHouses() {
        return streetBuildings.get("Houses") < 4;
    }

    private boolean hasOwnerAndEnoughMoney(Board board) {
        return getOwner(board) != null && getOwner(board).getPlayerBalance() >= streetGroup.getBuildingPrice();
    }

    /*
     * Street receives a house if canBuyHouse returns true and player loses money determined by the buildingPrice.
     */
    @Override
    public void buyHouse(Board board) {
        if (canBuyHouses(board)) {
            streetBuildings.put("Houses", streetBuildings.get("Houses") + 1);
            getOwner(board).reducePlayerBalance(streetGroup.getBuildingPrice());
        }
    }

    /*
     * Sells a house if canSellHouses is true and the owner receives half of the buildingPrice.
     */
    @Override
    public void sellHouse(Board board) {
        if (canSellHouses(board)) {
            streetBuildings.replace("Houses", streetBuildings.get("Houses") - 1);
            getOwner(board).increasePlayerBalance(streetGroup.getBuildingPrice() / 2);
        }
    }
}

package ee.taltech.monopoly.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import ee.taltech.monopoly.Game;
import ee.taltech.monopoly.services.GameInitializer;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.VBox;

import java.net.URL;
import java.util.ResourceBundle;

public class JoinLobbyController implements Initializable {
    private static Double width = 1050.0;
    private static Double height = 800.0;

    @FXML
    private JFXButton joinButton;
    @FXML
    private JFXTextField playerNameTextField;
    @FXML
    private JFXTextField gameIDTextField;
    @FXML
    private VBox vBox;

    private void setValidID(String value) {
        gameIDTextField.setText(value.toUpperCase().replaceAll("[^0-9]", ""));
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        vBox.setPrefWidth(width);
        vBox.setPrefHeight(height);

        gameIDTextField.textProperty().addListener((observable, oldValue, newValue) -> setValidID(newValue));

        joinButton.setOnAction(e -> {
            GameInitializer.joinGame(gameIDTextField.getText(), playerNameTextField.getText());

            Game.loadStartGame(vBox.getWidth(), vBox.getHeight());
            Game.getStage().setScene(Game.getStartGame());
        });
    }

    public static void setWidth(Double width) {
        JoinLobbyController.width = width;
    }

    public static void setHeight(Double height) {
        JoinLobbyController.height = height;
    }
}

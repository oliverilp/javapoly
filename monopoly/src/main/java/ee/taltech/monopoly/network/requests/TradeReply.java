package ee.taltech.monopoly.network.requests;

public class TradeReply extends Request {
    private boolean isAccepted;

    public TradeReply() {
    }

    public TradeReply(String gameID, boolean isAccepted) {
        super(gameID);
        this.isAccepted = isAccepted;
    }

    public boolean isAccepted() {
        return isAccepted;
    }
}

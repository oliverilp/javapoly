package ee.taltech.monopoly.gamelogic.street.purchasable;

import ee.taltech.monopoly.gamelogic.Board;
import ee.taltech.monopoly.gamelogic.Player;
import ee.taltech.monopoly.gamelogic.street.StreetGroup;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class MockPurchasableStreetTest {
    PurchasableStreet purchasableStreet = new MockPurchasableStreet();
    Board board = new Board();
    Player owner = new Player("Name1", "id1", board);
    Player payer = new Player("Name2", "id2", board);

    @BeforeEach
    void setup() {
        if (board.getAllPlayers().isEmpty()) {
            board.setAllPlayers(Arrays.asList(owner, payer));
        }
    }

    @Test
    void buyHouseSellHouse() {
        purchasableStreet.buyHouse(board);
        purchasableStreet.sellHouse(board);
    }

    @Test
    void getRent() {
        purchasableStreet.setRent(2);
        purchasableStreet.getRent(owner, 2, board);
    }

    @Test
    void payRent() {
        purchasableStreet.setRent(2);
        purchasableStreet.payRent(payer, 2, board);
        assertEquals(1500, payer.getPlayerBalance());
        purchasableStreet.setOwner(owner);
        purchasableStreet.payRent(payer, 2, board);
        assertEquals(1498, payer.getPlayerBalance());
    }

    @Test
    void testEqualsAndHash() {
        PurchasableStreet purchasableStreetX = new RegularStreet("Name", 3, 3, StreetGroup.BROWN, 1);
        PurchasableStreet purchasableStreetY =new RegularStreet("Name", 3, 3, StreetGroup.BROWN, 1);
        assertTrue(purchasableStreetX.equals(purchasableStreetY)
                && purchasableStreetY.equals(purchasableStreetX));
        assertTrue(purchasableStreetX.hashCode() == purchasableStreetY.hashCode());
        assertFalse(purchasableStreet.equals(purchasableStreetX));
        assertFalse(purchasableStreet.equals(null));
    }
}

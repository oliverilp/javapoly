package ee.taltech.monopoly.network.requests;

import ee.taltech.monopoly.gamelogic.Player;

public class CreateGame extends Request {
    private Player player;

    public CreateGame() {
    }

    public CreateGame(String gameID, Player player) {
        super(gameID);
        this.player = player;
    }

    public Player getPlayer() {
        return player;
    }
}

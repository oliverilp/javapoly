package ee.taltech.monopoly.gamelogic;

import java.text.MessageFormat;

public final class StaticStringKeeper {
    /*
     * Possible reward outputs.
     */
    public static final String GO_TO_GO_OUTPUT_MESSAGE = "Mine otsekohe GO tänavale ja võta oma 200 tundi!";
    public static final String GO_TO_NEXT_UTILITY_OUTPUT_MESSAGE = "Sa otsustasid, et on aeg tegeleda millegi suuremaga ja liikusid edasi!";
    public static final String GO_TO_NEXT_UTILITY_OUTPUT_MESSAGE_PASS_GO = GO_TO_NEXT_UTILITY_OUTPUT_MESSAGE + " Vahepeal korjasid oma 200 tundi GO pealt ära.";
    public static final String GO_TO_NEXT_RAILROAD_OUTPUT_MESSAGE = "Sa otsustasid, et on aeg oma valikaine moodul ära teha ja liikusid edasi!";
    public static final String GO_TO_NEXT_RAILROAD_OUTPUT_MESSAGE_PASS_GO = GO_TO_NEXT_RAILROAD_OUTPUT_MESSAGE + " Mõõdudes GO tänavalt, võtsid oma 200!";
    public static final String GO_TO_X_STREET_OUTPUT_MESSAGE = "Sul pole aega paigal seista ja lippad edasi!";
    public static final String GO_TO_X_STREET_OUTPUT_MESSAGE_PASS_GO = GO_TO_GO_OUTPUT_MESSAGE + " Otse üle GO, teenid 200 tundi";

    private static final String COLLECT_X_EAP_OUTPUT_MESSAGE_OUTPUT_MESSAGE = "Sa just teenisid {0} tundi, sest sa tegid all-nighteri!";
    private static final String GET_X_EAP_FROM_EVERY_PLAYER_OUTPUT_MESSAGE = "Teised aitasid sind Java projektiga ja sa teenisid nende arvelt {0} tundi!";

    public static String getCollectXEapOutputMessage(int time) {
        return MessageFormat.format(COLLECT_X_EAP_OUTPUT_MESSAGE_OUTPUT_MESSAGE, time);
    }
    public static String getGetXEapFromEveryPlayerOutputMessage(int time) {
        return MessageFormat.format(GET_X_EAP_FROM_EVERY_PLAYER_OUTPUT_MESSAGE, time);
    }

    /*
     * Possible punishment outputs.
     */

    public static final String GO_TO_JAIL_DECK_OUTPUT_MESSAGE = "Sa jäid vahele spikerdamisega, mine otsekohe dekanaati! Häbi! >:(";
    public static final String GO_TO_JAIL_STREET_OUTPUT_MESSAGE = "Jäid iseseisva tööga hiljapeale ja otsustasid AnnaAbit kasutada, kahjuks jäid vahele. Mine otsekohe dekanaati!";
    public static final String GO_BACK_3_STEPS_OUTPUT_MESSAGE = "Koroona pärast on sul kuhjunud palju ülesandeid ja sa otsustad hoogu maha võtta. Sa lähed 3 sammu tagasi!";


    private static final String PAY_X_EAP_OUTPUT_MESSAGE = "Sa magad liiga tihti loengus ja kaotasid {0} tundi!";
    private static final String PAY_X_EAP_TO_EVERY_PLAYER_OUTPUT_MESSAGE = "Sa oled liiga lahke inimene ja raiskasid {0} tundi teiste inimeste proge ülesannete debuggimise peale!";


    public static String getPayXEapOutputMessage(int time) {
        return MessageFormat.format(PAY_X_EAP_OUTPUT_MESSAGE, time);
    }

    public static String getPayXEapToEveryPlayerOutputMessage(int time) {
        return MessageFormat.format(PAY_X_EAP_TO_EVERY_PLAYER_OUTPUT_MESSAGE, time);
    }

    /*
     * Pay rent messages.
     */

    public static final String NO_RENT_OUTPUT_MESSAGE = "You should not see this! Something is wrong.";

    private static final String PAY_RENT_OUTPUT_MESSAGE = "Sa just maksid {0} tundi oma kaasmängijale ''{1}''.";

    private static final String PAY_TAX_LOW_OUTPUT_MESSAGE = "Värskelt ülikooli tulnud, pole sa veel harjunud iseseisva õppimisega. Kaotad {0} tundi.";

    public static final String PAY_TAX_HIGH_OUTPUT_MESSAGE = "Elu on ikka täis kohustusi, pead kas tegelema tudengi organisatsiooniga või hoidma oma õe lapsi. Kaotad 200 tundi.";

    public static String getPayRentOutputMessage(int time, String name) {
        return MessageFormat.format(PAY_RENT_OUTPUT_MESSAGE, time, name);
    }

    public static String getPayTaxLowOutputMessage(int tax) {
        return MessageFormat.format(PAY_TAX_LOW_OUTPUT_MESSAGE, tax);
    }

    /*
     * Other messages.
     */

    public static final String FREE_PARKING_OUTPUT_MESSAGE = "Sa jõudsid õigel ajal kooli ja said parkimiskoha!";

    public static final String GO_ON_ENTER_OUTPUT_MESSAGE = "Sa jõudsid GO tänavale ja teenid 200 tundi!";

    public static final String TRADE_SUCCESS_MESSAGE = "Mängija ''{0}'' võttis pakkumise vastu!";
    public static final String TRADE_FAIL_MESSAGE = "Mängija ''{0}'' ei võtnud pakkumist vastu!";

    public static String getTradeSuccessMessage(String name) {
        return MessageFormat.format(TRADE_SUCCESS_MESSAGE, name);
    }

    public static String getTradeFailMessage(String name) {
        return MessageFormat.format(TRADE_FAIL_MESSAGE, name);
    }
}

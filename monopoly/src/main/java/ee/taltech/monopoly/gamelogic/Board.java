package ee.taltech.monopoly.gamelogic;

import ee.taltech.monopoly.gamelogic.cards.CardDeck;
import ee.taltech.monopoly.gamelogic.street.Street;
import ee.taltech.monopoly.gamelogic.street.StreetGroup;
import ee.taltech.monopoly.gamelogic.street.nonpurchasable.ChanceAndCommChestStreet;
import ee.taltech.monopoly.gamelogic.street.nonpurchasable.GoStreet;
import ee.taltech.monopoly.gamelogic.street.nonpurchasable.GoToJailStreet;
import ee.taltech.monopoly.gamelogic.street.nonpurchasable.JailStreet;
import ee.taltech.monopoly.gamelogic.street.nonpurchasable.ParkingStreet;
import ee.taltech.monopoly.gamelogic.street.nonpurchasable.TaxStreet;
import ee.taltech.monopoly.gamelogic.street.purchasable.PurchasableStreet;
import ee.taltech.monopoly.gamelogic.street.purchasable.RailroadStreet;
import ee.taltech.monopoly.gamelogic.street.purchasable.RegularStreet;
import ee.taltech.monopoly.gamelogic.street.purchasable.UtilityStreet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Holds the game state.
 */
public class Board {
    private static Board instance;

    private String gameID;
    public Board() {}

    public static void initialize(String gameID) {
        if (instance == null) {
            instance = new Board(gameID);
        }
    }
    /*
     * Stores all streets in the game in this list.
     */
    private final List<Street> streets = Arrays.asList(
            new GoStreet(0),
            new RegularStreet("Sissejuhatus erialasse", 60, 2, StreetGroup.BROWN, 1),
            new ChanceAndCommChestStreet("Tunnikontroll", CardDeck.Type.COMMUNITY, 2),
            new RegularStreet("Sissejuhatus infotehnoloogiasse", 60, 4, StreetGroup.BROWN, 3),
            new TaxStreet("Loomulik kadu", false, 4),
            new RailroadStreet("Valikaine 1", 200, StreetGroup.RAILROAD, 5),
            new RegularStreet("Diskmata 1", 100, 6, StreetGroup.LIGHT_BLUE, 6),
            new ChanceAndCommChestStreet("Juhus", CardDeck.Type.CHANCE, 7),
            new RegularStreet("Diskmata 2", 100, 6, StreetGroup.LIGHT_BLUE, 8),
            new RegularStreet("Kõrgem matemaatika", 120, 8, StreetGroup.LIGHT_BLUE, 9),
            new JailStreet(10),
            new RegularStreet("Algoritmid ja andmestruktuurid", 140, 10, StreetGroup.PINK, 11),
            new UtilityStreet("Robodex", 150, StreetGroup.UTILITY, 12),
            new RegularStreet("Andmebaasid", 140, 10, StreetGroup.PINK, 13),
            new RegularStreet("Andmebaasid 2", 160, 12, StreetGroup.PINK, 14),
            new RailroadStreet("Valikaine 2", 200, StreetGroup.RAILROAD, 15),
            new RegularStreet("OP süsteemid", 180, 14, StreetGroup.ORANGE, 16),
            new ChanceAndCommChestStreet("Tunnikontroll", CardDeck.Type.COMMUNITY, 17),
            new RegularStreet("Arvutivõrgud", 180, 14, StreetGroup.ORANGE, 18),
            new RegularStreet("Arvutid", 200, 16, StreetGroup.ORANGE, 19),
            new ParkingStreet(20),
            new RegularStreet("Matlab", 220, 18, StreetGroup.RED, 21),
            new ChanceAndCommChestStreet("Juhus", CardDeck.Type.CHANCE, 22),
            new RegularStreet("Füüsikaliste protsesside modelleerimine", 220, 18, StreetGroup.RED, 23),
            new RegularStreet("Arvutusmeetodid", 240, 20, StreetGroup.RED, 24),
            new RailroadStreet("Valikaine 3", 200, StreetGroup.RAILROAD, 25),
            new RegularStreet("Veebirakendused", 260, 22, StreetGroup.YELLOW, 26),
            new RegularStreet("Mobiilirakendused", 260, 22, StreetGroup.YELLOW, 27),
            new UtilityStreet("Laphack", 150, StreetGroup.UTILITY, 28),
            new RegularStreet("Kasutajaliidesed", 280, 24, StreetGroup.YELLOW, 29),
            new GoToJailStreet(30),
            new RegularStreet("Programmeerimise algkursus", 300, 26, StreetGroup.GREEN, 31),
            new RegularStreet("Programmeerimise erikursus", 300, 26, StreetGroup.GREEN, 32),
            new ChanceAndCommChestStreet("Tunnikontroll", CardDeck.Type.COMMUNITY, 33),
            new RegularStreet("Programmeerimise põhikursus", 320, 28, StreetGroup.GREEN, 34),
            new RailroadStreet("Valikaine 4", 200, StreetGroup.RAILROAD, 35),
            new ChanceAndCommChestStreet("Juhus", CardDeck.Type.CHANCE, 36),
            new RegularStreet("Robotite programmeerimine", 350, 35, StreetGroup.DARK_BLUE, 37),
            new TaxStreet("Muud kohustused", true, 38),
            new RegularStreet("Lõputöö", 400, 50, StreetGroup.DARK_BLUE, 39)
    );

    private List<Player> allPlayers = new ArrayList<>();

    private String currentPlayerID;

    public Board(String gameID) {
        this.gameID = gameID;
    }

    public List<Street> getStreets() {
        return streets;
    }

    public Street getStreetAt(int index) {
        return streets.get(index);
    }

    /**
     * Calculate players's new location based on current location and dice roll result.
     */
    public Street move(Player player, int steps) {

        int destinationIndex = player.getLocation(this).getIndex() + steps;
        if (destinationIndex >= streets.size()) {
            destinationIndex -= streets.size();
        }
        Street newStreet = streets.get(destinationIndex);
        player.setLocation(newStreet);
        return newStreet;
    }

    /**
     * Trade mechanic that switches money and streets between players.
     *
     * @param board    board for technical reasons
     * @param sendingPlayer  person who trades
     * @param receivingPlayer  other person who trades
     * @param senderStreets person1 offered streets
     * @param receiverStreets person2 offered streets
     * @param senderMoney   person 1 offered money
     * @param receiverMoney   person 2 offered money
     */
    public static void trade(Board board,
                             Player sendingPlayer, Player receivingPlayer,
                             List<PurchasableStreet> senderStreets, List<PurchasableStreet> receiverStreets,
                             int senderMoney, int receiverMoney) {
        for (PurchasableStreet purchasableStreet : senderStreets) {
            purchasableStreet.tradeStreet(board, receivingPlayer);
        }
        for (PurchasableStreet purchasableStreet : receiverStreets) {
            purchasableStreet.tradeStreet(board, sendingPlayer);
        }

        sendingPlayer.reducePlayerBalance(senderMoney);
        receivingPlayer.reducePlayerBalance(receiverMoney);
        sendingPlayer.increasePlayerBalance(receiverMoney);
        receivingPlayer.increasePlayerBalance(senderMoney);
    }

    public static Board getInstance() {
        return instance;
    }

    public static void setInstance(Board instance) {
        Board.instance = instance;
    }

    public List<Player> getAllPlayers() {
        return allPlayers;
    }

    public void setAllPlayers(List<Player> players) {
        allPlayers = players;
    }

    /*
     * Get list of players that does not include the LocalPlayer.
     */
    public List<Player> getOtherPlayers() {
        return allPlayers.stream()
                .filter(player -> !player.equals(LocalPlayer.getInstanceAsOptional().orElse(null)))
                .collect(Collectors.toList());
    }

    /*
     * Get player from allPlayers list by player instance.
     */
    public Player getPlayer(Player player) {
        return allPlayers.stream()
                .filter(p -> p.equals(player))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Player '" + player + "' not found | " + allPlayers));
    }

    /*
     * Get player from allPlayers list by playerID.
     */
    public Player getPlayer(String playerID) {
        return allPlayers.stream()
                .filter(p -> p.getID().equals(playerID))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Player with ID '" + playerID + "' not found | " + allPlayers));
    }

    /*
     * Get player whose turn it is currently.
     */
    public Player getCurrentPlayer() {
        return getPlayer(currentPlayerID);
    }

    /*
     * Set the current player.
     */
    public void setCurrentPlayer(Player player) {
        this.currentPlayerID = player.getID();
    }

    /*
     * Get localPlayer.
     */
    public Player getLocalPlayer() {
        return getPlayer(LocalPlayer.getInstance());
    }

    /*
     * Get next player in allPlayers list and set him to be the currentPlayer.
     */
    public void incrementCurrentPlayer() {
        int currentIndex = getAllPlayers().indexOf(getCurrentPlayer());
        int newIndex = currentIndex + 1 < getAllPlayers().size() ? currentIndex + 1 : 0;
        setCurrentPlayer(getAllPlayers().get(newIndex));
    }

    public String getGameID() {
        return gameID;
    }
}

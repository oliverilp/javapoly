package ee.taltech.monopoly.controllers.gameboard.dialog;

import ee.taltech.monopoly.gamelogic.Board;
import ee.taltech.monopoly.gamelogic.Player;
import javafx.embed.swing.JFXPanel;
import javafx.scene.layout.StackPane;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SimpleDialogTest {
    JFXPanel panel = new JFXPanel();

    @BeforeEach
    void setUp() {
        Board.initialize("ID");
        Player player = new Player("Name", "ID", Board.getInstance());
        Board.getInstance().getAllPlayers().add(player);
        Board.getInstance().setCurrentPlayer(player);
    }

    @Test
    void show() {
        assertNotNull(Board.getInstance().getCurrentPlayer());
        StackPane stackPane = new StackPane();
        SimpleDialog.show("Heading", "Boyd", stackPane);
    }
}

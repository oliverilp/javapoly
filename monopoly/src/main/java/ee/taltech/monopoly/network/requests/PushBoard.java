package ee.taltech.monopoly.network.requests;

import ee.taltech.monopoly.gamelogic.Board;

public class PushBoard extends Request {
    private Board board;

    public PushBoard() {
    }

    public PushBoard(String gameID, Board board) {
        super(gameID);
        this.board = board;
    }

    public Board getBoard() {
        return board;
    }
}

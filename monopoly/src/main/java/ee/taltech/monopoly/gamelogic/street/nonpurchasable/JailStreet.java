package ee.taltech.monopoly.gamelogic.street.nonpurchasable;

public class JailStreet extends NonPurchasableStreet {
    public JailStreet() {}

    /**
     * Creates NonPurchasable street based on the real Jail street.
     *
     * @param index location on board.
     */
    public JailStreet(int index) {
        name = "Dekanaat";
        this.index = index;
    }
}

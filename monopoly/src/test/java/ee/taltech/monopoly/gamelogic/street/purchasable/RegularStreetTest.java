package ee.taltech.monopoly.gamelogic.street.purchasable;

import ee.taltech.monopoly.gamelogic.Board;
import ee.taltech.monopoly.gamelogic.Player;
import ee.taltech.monopoly.gamelogic.street.StreetGroup;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

class RegularStreetTest {
    Player player;
    Board board = new Board();
    RegularStreet regularStreet;
    RegularStreet regularStreet2;
    RegularStreet regularStreet3;
    RegularStreet regularStreet4;
    RegularStreet regularStreet5;
    RegularStreet regularStreetEmpty = new RegularStreet();

    @BeforeEach
    void setUp() {
        board = new Board();
        regularStreet = new RegularStreet("Name", 60, 2, StreetGroup.BROWN, 1);
        regularStreet2 = new RegularStreet("Name2", 60, 4, StreetGroup.BROWN, 3);
        regularStreet3 = new RegularStreet("Name3", 60, 4, StreetGroup.RED, 21);
        regularStreet4 = new RegularStreet("Name4", 60, 4, StreetGroup.RED, 23);
        regularStreet5 = new RegularStreet("Name5", 60, 4, StreetGroup.RED, 24);

        board.getStreets().set(1, regularStreet);
        board.getStreets().set(3, regularStreet2);
        board.getStreets().set(21, regularStreet3);
        board.getStreets().set(23, regularStreet4);
        board.getStreets().set(24, regularStreet5);

        player = new Player("NameP", "id", board);

        if (board.getAllPlayers().isEmpty()) {
            board.getAllPlayers().add(player);
        }
    }

    @Test
    void canBuy() {
        assertTrue(regularStreet.canBuy(player, board));
    }
    @Test
    void getName() {
        assertEquals("Name", regularStreet.getName());
        assertEquals("Name5", regularStreet5.getName());
    }

    @Test
    void  getStreetGroup() {
        assertEquals(StreetGroup.BROWN, regularStreet.getStreetGroup());
        assertEquals(StreetGroup.RED, regularStreet5.getStreetGroup());
    }

    @Test
    void getPrice() {
        assertEquals(60, regularStreet.getPrice());
    }

    @Test
    void getOwner() {
        assertNull(regularStreet.getOwner(board));
        regularStreet.purchaseStreet(player, board);
        assertEquals(player, regularStreet.getOwner(board));
    }

    @Test
    void getRent() {
        assertEquals(2, regularStreet.getRent(player, 2, board));
        regularStreet.getStreetBuildings().put("Houses", 1);
        assertEquals(18, regularStreet.getRent(player, 2, board));
    }

    @Test
    void getStreetBuildings() {
        assertEquals(new HashMap<String, Integer>() {{put("Houses", 0);}}, regularStreet.getStreetBuildings());
        regularStreet.getStreetBuildings().put("Houses", 1);
        assertEquals(new HashMap<String, Integer>() {{put("Houses", 1);}}, regularStreet.getStreetBuildings());
    }

    @Test
    void isMortgage() {
        regularStreet.setMortgage(true);
        assertTrue(regularStreet.isMortgage());
    }

    @Test
    void canBuyHousesBrown() {
        assertFalse(regularStreet.canBuyHouses(board));
        regularStreet.purchaseStreet(player, board);
        assertFalse(regularStreet.canBuyHouses(board));
        regularStreet2.purchaseStreet(player, board);
        assertTrue(regularStreet.canBuyHouses(board));
    }

    @Test
    void canBuyHousesRed() {
        assertFalse(regularStreet3.canBuyHouses(board));
        regularStreet3.purchaseStreet(player, board);
        assertFalse(regularStreet3.canBuyHouses(board));
        regularStreet4.purchaseStreet(player, board);
        assertFalse(regularStreet3.canBuyHouses(board));
        regularStreet5.purchaseStreet(player, board);
        assertTrue(regularStreet3.canBuyHouses(board));
    }

    @Test
    void canSellHouses() {
        assertFalse(regularStreet.canSellHouses(board));
        assertFalse(regularStreet2.canSellHouses(board));
    }

    @Test
    void buyHouse() {
        regularStreet.purchaseStreet(player, board);
        regularStreet2.purchaseStreet(player, board);
        assertEquals(new HashMap<String, Integer>() {{put("Houses", 0);}}, regularStreet.getStreetBuildings());
        regularStreet.buyHouse(board);
        assertEquals(new HashMap<String, Integer>() {{put("Houses", 1);}}, regularStreet.getStreetBuildings());
        regularStreet.buyHouse(board);
        assertEquals(new HashMap<String, Integer>() {{put("Houses", 2);}}, regularStreet.getStreetBuildings());
    }


    @Test
    void sellHouse() {
        regularStreet.purchaseStreet(player, board);
        regularStreet2.purchaseStreet(player, board);
        regularStreet.getStreetBuildings().put("Houses", 2);
        assertEquals(new HashMap<String, Integer>() {{put("Houses", 2);}}, regularStreet.getStreetBuildings());
        regularStreet.sellHouse(board);
        assertEquals(new HashMap<String, Integer>() {{put("Houses", 1);}}, regularStreet.getStreetBuildings());
        regularStreet.sellHouse(board);
        assertEquals(new HashMap<String, Integer>() {{put("Houses", 0);}}, regularStreet.getStreetBuildings());
    }
}

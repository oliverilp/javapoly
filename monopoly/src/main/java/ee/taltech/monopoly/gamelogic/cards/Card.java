package ee.taltech.monopoly.gamelogic.cards;

import java.util.List;

public class Card {
    private String question;
    List<String> answerChoices;
    private String answer;

    public Card() {}

    /**
     * Creates a "question card" that holds all the necessary info about it inside it self.
     *
     * @param question Question given to the player.
     * @param answerChoices Multi choices given to the player.
     * @param answer Correct answer that is also included in the answerChoices.
     */
    public Card(String question, List<String> answerChoices, String answer) {
        this.question = question;
        this.answerChoices = answerChoices;
        this.answer = answer;
    }

    public String getQuestion() {
        return question;
    }

    public List<String> getAnswerChoices() {
        return answerChoices;
    }

    public String getAnswer() {
        return answer;
    }
}

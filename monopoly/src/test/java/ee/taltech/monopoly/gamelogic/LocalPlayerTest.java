package ee.taltech.monopoly.gamelogic;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class LocalPlayerTest {
    Board board = new Board();
    Player player = new Player("Name", "id", board);

    @Test
    void initializeAndGetInstance() {
        LocalPlayer.nullLocalPlayer();
        assertThrows(RuntimeException.class, LocalPlayer::getInstance);
        LocalPlayer.initialize("Name", "id");
        assertEquals(player, LocalPlayer.getInstance());
    }
}

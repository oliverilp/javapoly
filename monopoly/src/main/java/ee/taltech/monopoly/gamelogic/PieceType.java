package ee.taltech.monopoly.gamelogic;

import java.util.Arrays;
import java.util.List;

public enum PieceType {
    /*
     * This class holds player pieces that are used in the GUI to display players on the screen.
     */
    ARROW, CAR, KEY, LOCK, PIN, SHOPPING_CART, TRUCK, AIRPLANE;

    private static List<PieceType> list = Arrays.asList(
            ARROW, CAR, KEY, LOCK, PIN, SHOPPING_CART, TRUCK, AIRPLANE
    );

    public static PieceType get(int index) {
        index = index < list.size() ? index : list.size() - 1;
        return list.get(index);
    }
}

package ee.taltech.monopoly.gamelogic.street.nonpurchasable;

import ee.taltech.monopoly.gamelogic.cards.CardDeck;

public class ChanceAndCommChestStreet extends NonPurchasableStreet {
    private CardDeck.Type type;

    public ChanceAndCommChestStreet() {}

    /**
     * Creates NonPurchasable street based on the real monopoly chance and community chest street.
     *
     * @param name name of street.
     * @param type chance or community chest street.
     * @param index location on board.
     */
    public ChanceAndCommChestStreet(String name, CardDeck.Type type, int index) {
        this.name = name;
        this.type = type;
        this.index = index;
    }

    public CardDeck.Type getType() {
        return type;
    }
}

# Changelog

## v1.0.0

### Technical

### Architecture diagram

```mermaid
graph TB
    c1-->a2
    subgraph client
      gamelogic --> view
      serverapi --> gamelogic
      view --> gamelogic
      gamelogic --> serverapi
    end
    subgraph server
    http & vs
    end
    subgraph AI
    c1-->c2
    end
```

#### Starting the game

```mermaid
sequenceDiagram
    participant P1 as First player
    participant S as Server
    participant P2 as Second player    
    
    P1 ->> S: HTTP /api/new-game/init/ {playerName: "John"}
    S ->> P1: {gameId: "1234", playerId: "player1"}
    P1 -->> S: WS /ws/subscribe-to-game/1234/player1/

    P1 ->> P2: "Hey, game ID is 1234, come and join"
    P2 ->> S: HTTP /api/new-game/1234/join/ {playerName: "Alice"}
    S ->> P2: {playerId: "player2"}
    P2 -->> S: WS /ws/subscribe-to-game/1234/player2/
    S -->> P1: {event: "playerJoined", player: {id: "player2", name: "Alice"}}

    P1 ->> S: HTTP /api/new-game/1234/start/
    S -->> P2: {event: "gameStarted"}
```

#### Gamelogic

```mermaid
classDiagram
  Game --* GameList
  Game -- GameStatus
  Player --* Game

  class GameList {
    +initGame() Game
    +getGameById(String gameId) Game
  }
  class Game {
    GameStatus status
    +addPlayer(String name) Player
    +start()
  }
  class Player {
    String id
    String name
  }
  class GameStatus {
    NOT_STARTED
    STARTED
  }

  

```


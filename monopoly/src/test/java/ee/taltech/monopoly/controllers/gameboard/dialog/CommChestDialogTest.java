package ee.taltech.monopoly.controllers.gameboard.dialog;

import ee.taltech.monopoly.gamelogic.Board;
import ee.taltech.monopoly.gamelogic.Player;
import ee.taltech.monopoly.gamelogic.cards.CardDeck;
import javafx.embed.swing.JFXPanel;
import javafx.scene.layout.StackPane;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CommChestDialogTest {
    JFXPanel panel = new JFXPanel();

    @BeforeEach
    void setUp() {
    }

    @Test
    void show() {
        Board.initialize("ID");
        Player player = new Player("Name", "ID", Board.getInstance());
        CardDeck cardDeck = new CardDeck();
        StackPane stackPane = new StackPane();
        CommChestDialog.show(player, cardDeck, stackPane, "Heading");
    }
}

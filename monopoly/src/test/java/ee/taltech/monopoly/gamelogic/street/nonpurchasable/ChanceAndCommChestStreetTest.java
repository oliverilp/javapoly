package ee.taltech.monopoly.gamelogic.street.nonpurchasable;

import ee.taltech.monopoly.gamelogic.cards.CardDeck;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ChanceAndCommChestStreetTest {
    ChanceAndCommChestStreet chanceAndCommChestStreetEmpty = new ChanceAndCommChestStreet();
    ChanceAndCommChestStreet chanceAndCommChestStreet = new ChanceAndCommChestStreet("Name", CardDeck.Type.COMMUNITY, 2);
    ChanceAndCommChestStreet chanceAndCommChestStreetTwo = new ChanceAndCommChestStreet("Name", CardDeck.Type.CHANCE, 7);

    @Test
    void getType() {
        assertEquals(CardDeck.Type.COMMUNITY, chanceAndCommChestStreet.getType());
        assertEquals(CardDeck.Type.CHANCE, chanceAndCommChestStreetTwo.getType());
    }
}

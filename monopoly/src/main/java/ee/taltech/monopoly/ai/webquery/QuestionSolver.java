package ee.taltech.monopoly.ai.webquery;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.NavigableMap;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class QuestionSolver {
    private static final String BASE_URL = "https://www.bing.com/search?q=";

    /**
     * Given a `question` and answer options,
     * select the option that is most often referred to in search engine results.
     */
    public static String answerQuestion(String question, Collection<String> answers) {
        final NavigableMap<Integer, String> answerOccurrenceFrequency = new TreeMap<>();
        final String searchResults = getSearchResults(question);
        answers.parallelStream().forEach(
                a -> answerOccurrenceFrequency.put(countSubstringOccurrences(searchResults, a), a));
        return answerOccurrenceFrequency.pollLastEntry().getValue();
    }

    /**
     * Make a query to Bing search engine
     * and return the content of all paragraph elements on the page.
     */
    private static String getSearchResults(String query) {
        try {
            Document doc = Jsoup.connect(BASE_URL + URLEncoder.encode(query, StandardCharsets.UTF_8)).get();
            final Elements paragraphs = doc.getElementsByTag("p");
            return paragraphs.stream().map(p -> p.text() + "\n\n").collect(Collectors.joining());
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * Given strings `longString` and `substring`,
     * count how many times `substring` can be found in `longString`.
     * <p>
     * Inspired by https://stackoverflow.com/a/767910/9035706
     */
    private static int countSubstringOccurrences(String longString, String substring) {
        int lastIndex = 0;
        int count = 0;

        while (lastIndex != -1) {
            lastIndex = longString.indexOf(substring, lastIndex);
            if (lastIndex != -1) {
                count++;
                lastIndex += substring.length();
            }
        }
        return count;
    }
}

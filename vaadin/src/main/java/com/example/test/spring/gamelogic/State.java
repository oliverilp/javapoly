package com.example.test.spring.gamelogic;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service // TODO should be Singleton
public class State {
    private final List<Player> allPlayers = Collections.synchronizedList(new ArrayList<Player>());

    public synchronized List<Player> getAllPlayers() {
        return allPlayers;
    }

    public synchronized void addPlayer(Player player) {
        allPlayers.add(player);
    }
}

package ee.taltech.monopoly.controllers.gameboard;

import javafx.animation.Interpolator;
import javafx.animation.ScaleTransition;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;

public class DiceAnimation {
    private StackPane leftDiceStackPane;
    private StackPane rightDiceStackPane;
    private Duration scaleTransitionDuration;

    public DiceAnimation(StackPane leftDiceStackPane, StackPane rightDiceStackPane, Duration scaleTransitionDuration) {
        this.leftDiceStackPane = leftDiceStackPane;
        this.rightDiceStackPane = rightDiceStackPane;
        this.scaleTransitionDuration = scaleTransitionDuration;
    }

    private void setTransitionValues(ScaleTransition st, int from, int to) {
        st.setInterpolator(Interpolator.EASE_BOTH);
        st.setFromX(from);
        st.setFromY(from);
        st.setToX(to);
        st.setToY(to);
    }

    private void setDiceStackPane(StackPane diceStackPane, ImageView dice) {
        diceStackPane.setScaleX(0);
        diceStackPane.setScaleY(0);
        diceStackPane.getChildren().clear();
        diceStackPane.getChildren().add(dice);
    }

    public void playBeginningAnimation(int diceRollLeft, int diceRollRight) {
        ImageView diceLeft = DiceIcon.getLeftIcons().get(diceRollLeft);
        ImageView diceRight = DiceIcon.getRightIcons().get(diceRollRight);

        ScaleTransition diceLeftBeginning = new ScaleTransition(scaleTransitionDuration, leftDiceStackPane);
        setTransitionValues(diceLeftBeginning, 0, 1);
        ScaleTransition diceRightBeginning = new ScaleTransition(scaleTransitionDuration, rightDiceStackPane);
        setTransitionValues(diceRightBeginning, 0, 1);

        setDiceStackPane(leftDiceStackPane, diceLeft);
        setDiceStackPane(rightDiceStackPane, diceRight);

        diceLeftBeginning.play();
        diceRightBeginning.play();
    }

    public void playEndAnimation() {
        ScaleTransition diceLeftEnd = new ScaleTransition(scaleTransitionDuration, leftDiceStackPane);
        setTransitionValues(diceLeftEnd, 1, 0);
        diceLeftEnd.setOnFinished(e -> leftDiceStackPane.getChildren().clear());

        ScaleTransition diceRightEnd = new ScaleTransition(scaleTransitionDuration, rightDiceStackPane);
        setTransitionValues(diceRightEnd, 1, 0);
        diceRightEnd.setOnFinished(e -> rightDiceStackPane.getChildren().clear());

        diceLeftEnd.play();
        diceRightEnd.play();
    }
}

package ee.taltech.monopoly.controllers.gameboard.dialog;

import com.jfoenix.controls.*;
import ee.taltech.monopoly.controllers.gameboard.GameBoardController;
import ee.taltech.monopoly.gamelogic.Board;
import ee.taltech.monopoly.gamelogic.Player;
import ee.taltech.monopoly.gamelogic.StaticStringKeeper;
import ee.taltech.monopoly.gamelogic.street.Street;
import ee.taltech.monopoly.gamelogic.street.purchasable.PurchasableStreet;
import ee.taltech.monopoly.network.ServerApi;
import ee.taltech.monopoly.network.requests.TradeReply;
import ee.taltech.monopoly.network.requests.TradeRequest;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TradeDialog {
    private final StackPane rootStackPane;
    private final Player player1;
    private final Player player2;
    Map<String, Integer> streets1;
    Map<String, Integer> streets2;
    private int money1;
    private int money2;

    private VBox rootVBox;
    private JFXDialogLayout content;
    private JFXDialog dialog;
    private GridPane contentGridPane;

    public TradeDialog(StackPane rootStackPane, Player player1, Player player2) {
        this.rootStackPane = rootStackPane;
        this.player1 = player1;
        this.player2 = player2;
        streets1 = null;
        streets2 = null;
        money1 = 0;
        money2 = 0;
    }

    public TradeDialog(StackPane rootStackPane, Player player1, Player player2,
                       Map<String, Integer> streets1, Map<String, Integer> streets2,
                       int money1, int money2) {
        this.rootStackPane = rootStackPane;
        this.player1 = player1;
        this.player2 = player2;
        this.streets1 = streets1;
        this.streets2 = streets2;
        this.money1 = money1;
        this.money2 = money2;
    }

    private VBox getPlayerVBox(Player player, Map<String, Integer> streetList) {
        VBox playerVBox = new VBox();
        Label label = new Label("Mängija '" + player.getName() + "'");
        label.setPadding(new Insets(0, 0, 20, 0));
        playerVBox.getChildren().add(label);
        for (Street street : player.getPlayerStreets(Board.getInstance())) {
            JFXCheckBox checkBox = new JFXCheckBox(street.getName());
            checkBox.setPadding(new Insets(0, 0, 10, 0));
            playerVBox.getChildren().add(checkBox);
            streetList.put(street.getName(), street.getIndex());
        }

        return playerVBox;
    }

    private Map<String, Integer> getStreets(VBox playerVBox, Map<String, Integer> streetList) {
        Map<String, Integer> output = new HashMap<>();

        for (Node node : playerVBox.getChildren()) {
            if (node instanceof JFXCheckBox) {
                JFXCheckBox jfxCheckBox = (JFXCheckBox) node;
                if (jfxCheckBox.isSelected()) {
                    String text = jfxCheckBox.getText();
                    output.put(text, streetList.get(text));
                }
            }
        }

        return output;
    }

    private void selectCheckBoxes(VBox playerVBox, Map<String, Integer> streets) {
        for (Node node : playerVBox.getChildren()) {
            if (node instanceof JFXCheckBox) {
                JFXCheckBox jfxCheckBox = (JFXCheckBox) node;
                boolean value = streets.containsKey(jfxCheckBox.getText());
                jfxCheckBox.setSelected(value);
            }
        }
    }

    private void setMoneyFieldValues(JFXTextField moneyField, int leftPadding) {
        moneyField.setPadding(new Insets(30, 0, 0, leftPadding));
        moneyField.setPromptText("Tundide arv");
        moneyField.setLabelFloat(true);
        moneyField.setMaxWidth(100);
        moneyField.setMinWidth(100);
        moneyField.setText("0");
    }

    private void showTradingDialog(boolean canEdit) {
        Map<String, Integer> streetList1 = new HashMap<>();
        Map<String, Integer> streetList2 = new HashMap<>();

        content = new JFXDialogLayout();
        content.setHeading(new Label("Kauplemine"));
        rootVBox = new VBox();
        contentGridPane = new GridPane();
        contentGridPane.setAlignment(Pos.CENTER);

        ColumnConstraints column = new ColumnConstraints();
        column.setPercentWidth(50);
        contentGridPane.getColumnConstraints().addAll(column, column);
        RowConstraints row = new RowConstraints();
        contentGridPane.getRowConstraints().addAll(row, row);

        JFXTextField moneyField1 = new JFXTextField();
        setMoneyFieldValues(moneyField1, 0);
        JFXTextField moneyField2 = new JFXTextField();
        setMoneyFieldValues(moneyField2, 20);
        VBox playerVBox1 = getPlayerVBox(player1, streetList1);
        VBox playerVBox2 = getPlayerVBox(player2, streetList2);
        playerVBox2.setPadding(new Insets(0, 0, 0, 20));

        contentGridPane.add(playerVBox1, 0, 0);
        contentGridPane.add(playerVBox2, 1, 0);
        contentGridPane.add(moneyField1, 0, 1);
        contentGridPane.add(moneyField2, 1, 1);
        rootVBox.getChildren().add(contentGridPane);
        content.setBody(rootVBox);
        dialog = new JFXDialog(rootStackPane, content, JFXDialog.DialogTransition.CENTER);

        if (!canEdit) {
            dialog.setOverlayClose(false);
            contentGridPane.setMouseTransparent(true);
            contentGridPane.setFocusTraversable(false);
            if (streets1 != null && streets2 != null) {
                selectCheckBoxes(playerVBox1, streets1);
                selectCheckBoxes(playerVBox2, streets2);
            }
            moneyField1.setText(String.valueOf(money1));
            moneyField2.setText(String.valueOf(money2));
        }


        Pane buttonContainer = getButtonContainer(
                dialog, playerVBox1, playerVBox2, moneyField1, moneyField2, streetList1, streetList2, canEdit
        );
        buttonContainer.setPadding(new Insets(0, 15, 15, 0));
        content.setActions(buttonContainer);

        dialog.show();
    }

    private void addErrorLabel(String message) {
        Label errorLabel = new Label(message);
        errorLabel.setTextFill(Color.RED);
        errorLabel.setPadding(new Insets(20, 0, 0, 0));
        rootVBox.getChildren().add(errorLabel);
    }

    private Pane getButtonContainer(JFXDialog dialog,
                                    VBox playerVBox1, VBox playerVBox2,
                                    JFXTextField moneyField1, JFXTextField moneyField2,
                                    Map<String, Integer> streetList1, Map<String, Integer> streetList2,
                                    boolean canEdit) {
        if (canEdit) {
            JFXButton offerButton = new JFXButton("TEE PAKKUMINE");
            offerButton.setOnAction(e1 -> {
                rootVBox.getChildren().removeIf(node -> node instanceof Label);

                int money1Value;
                int money2Value;

                try {
                    money1Value = Integer.parseInt(moneyField1.getText());
                    money2Value = Integer.parseInt(moneyField2.getText());
                } catch (NumberFormatException ex) {
                    addErrorLabel("Palun sisestage õige tundide arv!");
                    return;
                }

                if (money1Value < 0 || money1Value > player1.getPlayerBalance()
                        || money2Value < 0 || money2Value > player2.getPlayerBalance()) {
                    addErrorLabel("Tundide arv ei tohi olla negatiivne ega suurem kui mängija tundide arv!");
                    return;
                }
                offerButton.setDisable(true);
                dialog.setOverlayClose(false);
                contentGridPane.setMouseTransparent(true);
                contentGridPane.setFocusTraversable(false);

                streets1 = getStreets(playerVBox1, streetList1);
                streets2 = getStreets(playerVBox2, streetList2);
                money1 = money1Value;
                money2 = money2Value;

                TradeRequest tradeRequest = new TradeRequest(Board.getInstance().getGameID(), player1, player2,
                        streets1, streets2,
                        money1Value, money2Value);
                ServerApi.getInstance().tradeRequest(tradeRequest);
            });
            return new StackPane(offerButton);
        } else {
            JFXButton acceptButton = new JFXButton("NÕUSTU");
            acceptButton.setOnAction(e -> tradeReply(dialog, true));
            JFXButton declineButton = new JFXButton("KEELDU");
            declineButton.setOnAction(e -> tradeReply(dialog, false));

            HBox buttonsContainer = new HBox(declineButton, acceptButton);
            buttonsContainer.setSpacing(10);
            return buttonsContainer;
        }
    }

    public void tradeResult(boolean isAccepted) {
        contentGridPane.setMouseTransparent(true);
        contentGridPane.setFocusTraversable(false);
        dialog.setOverlayClose(true);

        String message;
        if (isAccepted) {
            makeTrade();
            GameBoardController.getInstance().updatePurchaseHouseButton();
            message = StaticStringKeeper.getTradeSuccessMessage(player2.getName());
        } else {
            message = StaticStringKeeper.getTradeFailMessage(player2.getName());
        }
        Label messageLabel = new Label(message);
        messageLabel.setPadding(new Insets(20, 0, 0, 0));
        rootVBox.getChildren().add(messageLabel);

        JFXButton okButton = new JFXButton("OK");
        okButton.setOnAction(e -> dialog.close());
        StackPane buttonContainer = new StackPane(okButton);
        buttonContainer.setPadding(new Insets(0, 15, 15, 0));
        content.setActions(buttonContainer);
    }

    private void makeTrade() {
        List<PurchasableStreet> newStreets1 = streets1.values().stream()
                .map(index -> (PurchasableStreet) Board.getInstance().getStreetAt(index))
                .collect(Collectors.toList());
        List<PurchasableStreet> newStreets2 = streets2.values().stream()
                .map(index -> (PurchasableStreet) Board.getInstance().getStreetAt(index))
                .collect(Collectors.toList());
        Player newPlayer1 = Board.getInstance().getPlayer(player1);
        Player newPlayer2 = Board.getInstance().getPlayer(player2);

        Board.trade(Board.getInstance(), newPlayer1, newPlayer2, newStreets1, newStreets2, money1, money2);
        GameBoardController.getInstance().updateAllPlayersData();
    }

    private void tradeReply(JFXDialog dialog, boolean isAccepted) {
        dialog.close();
        TradeReply tradeReply = new TradeReply(Board.getInstance().getGameID(), isAccepted);
        ServerApi.getInstance().tradeReply(tradeReply);
    }

    public void makeOfferDialog() {
        showTradingDialog(true);
    }

    public void showOfferDialog() {
        showTradingDialog(false);
    }
}

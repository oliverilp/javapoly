package ee.taltech.monopoly.gamelogic;

import java.util.Arrays;
import java.util.List;

public enum PieceColor {
    /*
     * This class holds the colors for player pieces and street labels that are used in the GUI.
     */
    ORANGE("#ffb74d"), YELLOW("#fff59d"), INDIGO("#9fa8da"), BLUE("#90caf9"),
    WHITE("#eeeeee"), DEEP_ORANGE("#ffab91"), PINK("#f48fb1"), GRAY("#607985");

    private final String hexValue;

    PieceColor(final String hexValue) {
        this.hexValue = hexValue;
    }

    @Override
    public String toString() {
        return hexValue;
    }

    private static List<PieceColor> list = Arrays.asList(
            ORANGE, YELLOW, INDIGO, BLUE, WHITE, DEEP_ORANGE, PINK, GRAY
    );

    public static PieceColor get(int index) {
        index = index < list.size() ? index : list.size() - 1;
        return list.get(index);
    }
}

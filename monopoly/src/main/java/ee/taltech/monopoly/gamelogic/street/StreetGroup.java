package ee.taltech.monopoly.gamelogic.street;

public enum StreetGroup {
    BROWN(50, 2.3), LIGHT_BLUE(50, 2.1),
    PINK(100, 1.9), ORANGE(100, 1.7),
    RED(150, 1.5), YELLOW(150, 1.3),
    GREEN(200, 1.1), DARK_BLUE(200, 1),
    UTILITY(1, 1), RAILROAD(1, 1), OTHER(1, 1);

    /**
     * Price for buying building on this street.
     * Those marked 1 cannot have any houses on them.
     */
    private final int buildingPrice;

    private final double rentMultiplier;

    StreetGroup(int buildingPrice, double rentMultiplier) {
        this.buildingPrice = buildingPrice;
        this.rentMultiplier = rentMultiplier;
    }

    public int getBuildingPrice() {
        return buildingPrice;
    }

    public double getRentMultiplier() {
        return rentMultiplier;
    }
}

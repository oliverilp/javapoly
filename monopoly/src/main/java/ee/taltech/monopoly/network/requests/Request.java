package ee.taltech.monopoly.network.requests;

/**
 * Represents a request sent from client to server.
 * All subclasses must have a default no argument constructor.
 */
public abstract class Request {
    private String gameID;

    public Request() {
    }

    public Request(String gameID) {
        this.gameID = gameID;
    }

    public String getGameID() {
        return gameID;
    }
}

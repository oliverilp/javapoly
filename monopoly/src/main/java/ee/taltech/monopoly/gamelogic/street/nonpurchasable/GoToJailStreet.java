package ee.taltech.monopoly.gamelogic.street.nonpurchasable;

public class GoToJailStreet extends NonPurchasableStreet {
    public GoToJailStreet() {}

    /**
     * Creates NonPurchasable street based on the real Go To Jail street.
     *
     * @param index location on board.
     */
    public GoToJailStreet(int index) {
        name = "Mine dekanaati";
        this.index = index;
    }
}

package ee.taltech.monopoly.gamelogic.street.nonpurchasable;

public class ParkingStreet extends NonPurchasableStreet {
    public ParkingStreet() {}

    /**
     * Creates NonPurchasable street based on the real Free Parking street.
     *
     * @param index location on board.
     */
    public ParkingStreet(int index) {
        name = "Tasuta parkimine";
        this.index = index;
    }
}

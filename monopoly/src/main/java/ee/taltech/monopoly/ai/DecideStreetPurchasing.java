package ee.taltech.monopoly.ai;

import ee.taltech.monopoly.gamelogic.Board;
import ee.taltech.monopoly.gamelogic.street.StreetGroup;
import ee.taltech.monopoly.gamelogic.street.purchasable.PurchasableStreet;

import java.util.stream.Stream;

/**
 * Decide whether or not the given aiPlayer
 * should purchase currentStreet given the current situation on the board.
 * <p>
 * Several factors are being taken into account.
 * Their values are sum up and if the sum is greater than or equal to
 * the PURCHASE_THRESHOLD, an affirmative decision is made.
 */
class DecideStreetPurchasing {
    private static final int PURCHASE_THRESHOLD = 14;
    private static final double FINANCIAL_SITUATION_COEFFICIENT = 3;
    private static final double MONOPOLY_OVER_CURRENT_STREET_GROUP_COEFFICIENT = 8;
    private static final double MONOPOLY_OVER_OTHER_STREET_GROUPS_COEFFICIENT = 0.5;

    private final AIPlayer aiPlayer;
    private final PurchasableStreet currentStreet;
    private final Board board;

    DecideStreetPurchasing(AIPlayer aiPlayer, PurchasableStreet currentStreet, Board board) {
        this.aiPlayer = aiPlayer;
        this.currentStreet = currentStreet;
        this.board = board;
    }

    /**
     * Make decision regarding purchasing the street.
     *
     * @return purchase decision
     */
    public boolean decide() {
        if (currentStreet.getPrice() > aiPlayer.getPlayerBalance()) {
            return false;
        }
        final double score = FINANCIAL_SITUATION_COEFFICIENT * considerFinancialSituation()
                + MONOPOLY_OVER_CURRENT_STREET_GROUP_COEFFICIENT * considerPossibilityOfMonopolyOverCurrentStreetGroup()
                + MONOPOLY_OVER_OTHER_STREET_GROUPS_COEFFICIENT * considerPossibilityOfMonopolyOverOtherStreetGroups();
        System.out.println(score);
        return score >= PURCHASE_THRESHOLD;
    }

    /**
     * Calculate player's relative wealth in comparison to the street's price
     */
    private double considerFinancialSituation() {
        return Math.max(1, aiPlayer.getPlayerBalance() - aiPlayer.getPlayerBalance() * 0.2) / currentStreet.getPrice();
    }

    /**
     * The street is more valuable if there is a possibility to obtain all streets of the given group.
     * If any other player has already purchased a street from that group, return 0,
     * otherwise return the number of streets of that group owned by the aiPlayer.
     */
    private double considerPossibilityOfMonopolyOverCurrentStreetGroup() {
        final long numberOfStreetsInThisGroup = board.getStreets().stream()
                .filter(s -> s.getStreetGroup() == currentStreet.getStreetGroup()).count();
        return DecisionUtils.getNumberOfStreetsInStreetGroupNotOwnedByOtherPlayers(board, currentStreet, aiPlayer)
                / (double) numberOfStreetsInThisGroup;
    }

    /**
     * If there are other street groups over which the player can have monopolistic ownership,
     * these streets have a higher priority and it would be good to save money for these purchases.
     */
    private double considerPossibilityOfMonopolyOverOtherStreetGroups() {
        return Stream.of(StreetGroup.values()).parallel().filter(streetGroup -> {
            final long alreadyPurchasedByOthers = board.getStreets().stream()
                    // get only streets from the given group which are purchased by somebody else
                    .filter(street -> street.getStreetGroup().equals(streetGroup))
                    .filter(street -> street instanceof PurchasableStreet)
                    .filter(street -> DecisionUtils.isStreetOwnedByOtherPlayers(
                            aiPlayer, (PurchasableStreet) street, board)).count();
            // if nobody but the current player has purchased streets from that group,
            // it is possible to get monopoly.
            return alreadyPurchasedByOthers == 0;
        }).count();
    }

}

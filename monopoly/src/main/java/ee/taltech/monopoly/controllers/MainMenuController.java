package ee.taltech.monopoly.controllers;

import com.jfoenix.controls.JFXButton;
import ee.taltech.monopoly.Game;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.VBox;

import java.net.URL;
import java.util.ResourceBundle;

public class MainMenuController implements Initializable {
    @FXML
    private JFXButton createLobbyButton;
    @FXML
    private JFXButton joinLobbyButton;
    @FXML
    private VBox vBox;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        createLobbyButton.setOnAction(e -> {
            Game.loadCreateLobby(vBox.getWidth(), vBox.getHeight());
            Game.getStage().setScene(Game.getCreateLobby());
        });

        joinLobbyButton.setOnAction(e -> {
            Game.loadJoinLobby(vBox.getWidth(), vBox.getHeight());
            Game.getStage().setScene(Game.getJoinLobby());
        });
    }
}

package ee.taltech.monopoly.services;

import ee.taltech.monopoly.gamelogic.Board;
import ee.taltech.monopoly.gamelogic.LocalPlayer;
import ee.taltech.monopoly.network.GamePhase;
import ee.taltech.monopoly.network.ServerApi;
import ee.taltech.monopoly.network.viewmodels.PlayersViewModel;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.scene.control.ListView;

import java.util.UUID;

public class GameInitializer {
    private static boolean isRunning = true;

    public static void joinGame(String gameID, String name) {
        if (name.trim().isEmpty()) {
            name = "no_name";
        }

        String id = UUID.randomUUID().toString();
        Board.initialize(gameID);
        LocalPlayer.initialize(name.trim(), id);
        Board.getInstance().setAllPlayers(ServerApi.getInstance().joinGame(gameID, LocalPlayer.getInstance()));
    }

    public static void startGame() {
        ServerApi.getInstance().startGame();
    }

    public static void pollPlayers(ListView<String> playersListView) throws InterruptedException {
        while (isRunning) {
            System.out.println("Polling players");
            PlayersViewModel response = ServerApi.getInstance().fetchPlayers();
            Board.getInstance().setAllPlayers(response.getPlayerList());
            Platform.runLater(() ->
                    playersListView.setItems(FXCollections.observableArrayList(response.getPlayerNames())));

            if (response.getPhase().equals(GamePhase.IN_PROGRESS)) {
                Board.setInstance(ServerApi.getInstance().fetchBoard());
                isRunning = false;
            }

            Thread.sleep(200);
        }
    }

    public static void createGame(String playerName) {
        if (playerName.trim().isEmpty()) {
            playerName = "no_name";
        }

        final String gameID = String.valueOf(System.currentTimeMillis()).substring(7);
        Board.initialize(gameID);

        final String playerId = UUID.randomUUID().toString();
        LocalPlayer.initialize(playerName.trim(), playerId);

        ServerApi.getInstance().createGame(gameID, LocalPlayer.getInstance());
    }
}

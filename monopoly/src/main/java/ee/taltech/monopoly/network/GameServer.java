package ee.taltech.monopoly.network;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;
import ee.taltech.monopoly.ai.AIPlayer;
import ee.taltech.monopoly.gamelogic.Board;
import ee.taltech.monopoly.gamelogic.PieceColor;
import ee.taltech.monopoly.gamelogic.PieceType;
import ee.taltech.monopoly.gamelogic.Player;
import ee.taltech.monopoly.network.requests.*;
import ee.taltech.monopoly.network.viewmodels.BoardViewModel;
import ee.taltech.monopoly.network.viewmodels.PlayersViewModel;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class GameServer {
    private static final int MAX_PLAYERS_COUNT = 8;
    private final Map<String, PlayersViewModel> playersViewModels = new HashMap<>();
    private final Map<String, Board> boards = new HashMap<>();
    private final Map<String, Connection> playerConnections = new HashMap<>();
    private final Server server;
    private final String aiPlayerID = "ai";

    public GameServer() throws IOException {
        server = new Server(1000000, 1000000);
        Register.register(server);

        server.addListener(new Listener() {
            public void received(Connection connection, Object object) {

                Request request = null;
                String gameID = null;
                // `object` can also be Kryonet's own event, not our custom Request
                if (object instanceof Request) {
                    request = (Request) object;
                    gameID = request.getGameID();
                }

                if (request instanceof CreateGame) {
                    final CreateGame createGameRequest = (CreateGame) object;
                    final Player player = createGameRequest.getPlayer();
                    player.setPieceType(PieceType.get(0));
                    player.setPieceColor(PieceColor.get(0));
                    System.out.println("Player '" + player.getName() + "' (" + player.getID() + ") created a game with ID " + createGameRequest.getGameID());
                    PlayersViewModel viewModel = new PlayersViewModel(player);
                    playersViewModels.put(gameID, viewModel);
                    playerConnections.put(player.getID(), connection);
                }

                if (request instanceof JoinGame) {
                    final JoinGame joinGameRequest = (JoinGame) object;
                    final PlayersViewModel viewModel = playersViewModels.get(gameID);
                    // In case of invalid ID
                    if (viewModel == null) {
                        server.sendToTCP(connection.getID(), PlayersViewModel.ofInvalidID());
                        return;
                    }
                    if (viewModel.getPhase().equals(GamePhase.PLAYERS_GATHERING)
                            && viewModel.getPlayerList().size() < MAX_PLAYERS_COUNT) {
                        Player player = joinGameRequest.getPlayer();
                        int index = viewModel.getPlayerList().size();
                        player.setPieceType(PieceType.get(index));
                        player.setPieceColor(PieceColor.get(index));
                        System.out.println("Player '" + player.getName() + "' (" + player.getID() + ") joined the game.");

                        viewModel.getPlayerList().add(player);
                        playerConnections.put(player.getID(), connection);

                        server.sendToTCP(connection.getID(), viewModel);
                    }
                }

                if (request instanceof FetchPlayers) {
                    var viewModel = playersViewModels.get(request.getGameID());
                    if (viewModel == null) {
                        System.out.println("PlayerViewModel not found with ID: " + request.getGameID());
                    }
                    server.sendToTCP(connection.getID(), viewModel);
                }

                if (request instanceof StartGame) {
                    final PlayersViewModel viewModel = playersViewModels.get(request.getGameID());
                    final Board board = new Board(request.getGameID());
                    boards.put(gameID, board);

                    // create and configure AI
                    final Player aiPlayer = new AIPlayer("AI", aiPlayerID, board);
                    int index = viewModel.getPlayerList().size();
                    viewModel.getPlayerList().add(aiPlayer);
                    aiPlayer.setPieceType(PieceType.get(index));
                    aiPlayer.setPieceColor(PieceColor.get(index));

                    board.setAllPlayers(viewModel.getPlayerList());
                    board.setCurrentPlayer(viewModel.getPlayerList().get(0));
                    // TODO Remove the viewModel from the playersViewModels list
                    //  for it is no longer needed.
                    //  instead of marking game phase it could just be deleted
                    //  to indicate that nobody can join any more.
                    viewModel.setPhase(GamePhase.IN_PROGRESS);
                }

                if (request instanceof FetchBoard) {
                    final Board board = boards.get(gameID);
                    if (board ==null) {
                        System.out.println("Board not found, gameID: " + gameID );
                    }
                    server.sendToTCP(connection.getID(), new BoardViewModel(board));
                }

                if (request instanceof PushBoard) {
                    PushBoard pushBoard = (PushBoard) request;
                    final Board board = pushBoard.getBoard();
                    boards.put(gameID, board);

                    if (board.getCurrentPlayer().getID().equals(aiPlayerID)) {
                        try {
                            ((AIPlayer) board.getCurrentPlayer()).playTurn(board);
                        } catch (Exception e) {
                            e.printStackTrace();
                            board.incrementCurrentPlayer();
                        }
                    }
                    logPlayersInfo(board);
                }

                if (request instanceof TradeRequest) {
                    TradeRequest tradeRequest = (TradeRequest) object;

                    if (tradeRequest.getReceivingPlayer().getID().equals(aiPlayerID)) {
                        // TODO Add some real strategy here
                        final AIPlayer aiPlayer = (AIPlayer) boards.get(request.getGameID()).getAllPlayers()
                                .stream().filter(player -> player.getID().equals(aiPlayerID))
                                .findAny().get();
                        boolean isAccepted = aiPlayer.considerTrade(tradeRequest, boards.get(gameID));
                        TradeReply tradeReply = new TradeReply(tradeRequest.getGameID(), isAccepted);
                        server.sendToAllTCP(tradeReply);
                        return;
                    }

                    Connection c = playerConnections.get(tradeRequest.getReceivingPlayer().getID());
                    server.sendToTCP(c.getID(), tradeRequest);
                }

                if (request instanceof TradeReply) {
                    TradeReply tradeReply = (TradeReply) object;
                    server.sendToAllExceptTCP(connection.getID(), tradeReply);
                }
            }
        });

        server.bind(Register.PORT);
        server.start();
    }

    public void stop() {
        server.stop();
    }

    private void logPlayersInfo(Board board) {
        System.out.println("Current player: " + board.getCurrentPlayer());
        System.out.println("Updated players:");
        for (Player player : board.getAllPlayers()) {
            System.out.println(String.format("Name: %s; Balance: %s; Location: %s",
                    player.getName(), player.getPlayerBalance(), player.getLocation(board).getName()));
            System.out.println(player.getPlayerStreets(board));
        }
        System.out.println();
    }

    public static void main(String[] args) throws IOException {
        GameServer gameServer = new GameServer();
        System.out.println("Server started.");
    }
}

package ee.taltech.monopoly.network;

import ee.taltech.monopoly.gamelogic.Board;
import ee.taltech.monopoly.gamelogic.Player;
import ee.taltech.monopoly.network.requests.TradeReply;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;


class ServerApiTest {
    private static GameServer gameServer;
    Player player1 = new Player("Name123", "id_1_", Board.getInstance());


    @BeforeAll
    static void init() {
        Board.initialize("1234");

        Thread thread = new Thread(() -> {
            try {
                gameServer = new GameServer();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        thread.start();
    }

    @AfterAll
    static void after() {
        try {
            Thread.sleep(50);
            ServerApi.getInstance().joinGame("1234", new Player("name", "id1", Board.getInstance()));
            Thread.sleep(50);
            ServerApi.getInstance().startGame();
            Thread.sleep(50);
            ServerApi.getInstance().tradeReply(new TradeReply("1234", false));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        gameServer.stop();
    }

    @Test
    void CreateGame() {
        ServerApi.getInstance().createGame("1234", player1);
    }
}

package ee.taltech.monopoly.gamelogic;


import java.util.Optional;

public class LocalPlayer {
    /*
     * Holds the local player.
     */
    private static Player instance;

    /*
     * Only used for a test.
     */
    public static void nullLocalPlayer() {
        instance = null;
    }

    /*
     * Set LocalPlayer if there is none.
     */
    public static void initialize(String name, String id) {
        if (instance == null) {
            instance = new Player(name, id, Board.getInstance());
        }
    }

    /*
     * Get LocalPlayer.
     */
    public static Player getInstance() {
        if (instance == null) {
            throw new RuntimeException("LocalPlayer has not yet been initialized");
        }
        return instance;
    }

    public static Optional<Player> getInstanceAsOptional() {
        return Optional.ofNullable(instance);
    }

    public static void setInstance(Player instance) {
        LocalPlayer.instance = instance;
    }
}

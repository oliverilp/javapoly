package ee.taltech.monopoly.gamelogic.street.nonpurchasable;

import ee.taltech.monopoly.gamelogic.Board;
import ee.taltech.monopoly.gamelogic.Player;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TaxStreetTest {
    TaxStreet taxStreetLow = new TaxStreet("low", false, 4);
    TaxStreet taxStreetHigh = new TaxStreet("high", true, 38);
    TaxStreet taxStreetEmpty = new TaxStreet();
    Board board = new Board();
    Player player = new Player("Name", "id", board);

    @Test
    void onEnter() {
        assertEquals(1500, player.getPlayerBalance());
        taxStreetHigh.payTax(player);
        assertEquals(1300, player.getPlayerBalance());
        taxStreetLow.payTax(player);
        assertEquals(1170, player.getPlayerBalance());
    }
}

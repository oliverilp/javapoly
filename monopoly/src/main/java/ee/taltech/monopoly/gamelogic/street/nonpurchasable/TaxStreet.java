package ee.taltech.monopoly.gamelogic.street.nonpurchasable;

import ee.taltech.monopoly.gamelogic.Player;
import ee.taltech.monopoly.gamelogic.StaticStringKeeper;

public class TaxStreet extends NonPurchasableStreet {
    private boolean isHighTax;

    public TaxStreet() {}

    /**
     * Creates NonPurchasable street based on the real Tax Street street.
     *
     * @param name street name.
     * @param isHighTax boolean if the street is a low or high tax street.
     * @param index location on board.
     */
    public TaxStreet(String name, boolean isHighTax, int index) {
        this.name = name;
        this.isHighTax = isHighTax;
        this.index = index;
    }

    public String payTax(Player player) {
        int rent = isHighTax ? 200 : (int) (player.getPlayerBalance() * 0.1);
        player.reducePlayerBalance(rent);
        return isHighTax ? StaticStringKeeper.PAY_TAX_HIGH_OUTPUT_MESSAGE : StaticStringKeeper.getPayTaxLowOutputMessage(rent);
    }
}

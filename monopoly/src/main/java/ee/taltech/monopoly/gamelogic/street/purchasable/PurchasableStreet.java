package ee.taltech.monopoly.gamelogic.street.purchasable;

import ee.taltech.monopoly.gamelogic.Board;
import ee.taltech.monopoly.gamelogic.Player;
import ee.taltech.monopoly.gamelogic.StaticStringKeeper;
import ee.taltech.monopoly.gamelogic.street.Street;
import ee.taltech.monopoly.gamelogic.street.StreetGroup;

import java.util.Objects;

public abstract class PurchasableStreet implements Street {
    protected String name;
    protected String ownerID;
    protected int price;
    protected int rent;
    protected boolean isMortgage;
    protected StreetGroup streetGroup;
    protected int index;

    @Override
    public boolean canBuy(Player player, Board board) {
        return getOwner(board) == null && player.getPlayerBalance() >= price;
    }

    /**
     * If available, a player can purchase the street for a price that was set in the constructor.
     *
     * @param buyer player who buys the street.
     */
    public void purchaseStreet(Player buyer, Board board) {
        if (canBuy(buyer, board)) {
            buyer.reducePlayerBalance(price);
            buyer.addPlayerStreet(this);
            setOwner(buyer);
        }
    }

    /**
     * If owner != null, switches the street owner from current to newOwner.
     */
    public void tradeStreet(Board board, Player newOwner) {
        if (getOwner(board) != null) {
            getOwner(board).removePlayerStreet(this);
            setOwner(newOwner);
            newOwner.addPlayerStreet(this);
        }
    }

    /**
     * If owned by someone, a player who lands on a street has to payRent to the owner.
     * rent is determined by the getRent function.
     *
     * @param payer player that has to pay rent.
     * @param diceRoll players rolled dice, needed to calculate rent for some streets.
     * @param board needed for technical reasons.
     * @return String that informs the player of what has happened.
     */
    public String payRent(Player payer, int diceRoll, Board board) {
        if (getOwner(board) != null) {
            int rent = getRent(payer, diceRoll, board);
            getOwner(board).increasePlayerBalance(rent);
            payer.reducePlayerBalance(rent);
            return StaticStringKeeper.getPayRentOutputMessage(rent, getOwner(board).getName());
        }
        return StaticStringKeeper.NO_RENT_OUTPUT_MESSAGE;
    }

    public void buyHouse(Board board) {}

    public void sellHouse(Board board) {}

    public String getName() {
        return name;
    }

    @Override
    public int getIndex() {
        return index;
    }

    public int getPrice() {
        return price;
    }

    public int getRent(Player player, int dice_roll, Board board) {
        return rent;
    }

    public Player getOwner(Board board) {
        return ownerID == null ? null : board.getPlayer(ownerID);
    }

    public void setOwner(Player owner) {
        this.ownerID = owner.getID();
    }

    public StreetGroup getStreetGroup() {
        return streetGroup;
    }

    public void setRent(int rent) {
        this.rent = rent;
    }

    public boolean isMortgage() {
        return isMortgage;
    }

    public void setMortgage(boolean bool) {
        isMortgage = bool;
    }

    @Override
    public String toString() {
        return "(" + index + ") " + name;
    }

    /**
     * Function that compares 2 objects and returns if they are the same.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PurchasableStreet that = (PurchasableStreet) o;
        return price == that.price &&
                rent == that.rent &&
                isMortgage == that.isMortgage &&
                name.equals(that.name) &&
                Objects.equals(ownerID, that.ownerID) &&
                streetGroup == that.streetGroup;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, ownerID, price, rent, isMortgage, streetGroup);
    }
}

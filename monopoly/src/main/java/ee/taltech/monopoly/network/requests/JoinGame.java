package ee.taltech.monopoly.network.requests;

import ee.taltech.monopoly.gamelogic.Player;

public class JoinGame extends Request {
    private Player player;

    public JoinGame() {}

    public JoinGame(String gameID, Player player) {
        super(gameID);
        this.player = player;
    }

    public Player getPlayer() {
        return player;
    }
}

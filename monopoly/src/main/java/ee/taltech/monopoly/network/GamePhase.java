package ee.taltech.monopoly.network;

public enum GamePhase {
    PLAYERS_GATHERING, IN_PROGRESS
}

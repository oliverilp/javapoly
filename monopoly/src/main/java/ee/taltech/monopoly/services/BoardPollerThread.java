package ee.taltech.monopoly.services;

import ee.taltech.monopoly.controllers.gameboard.GameBoardController;
import ee.taltech.monopoly.gamelogic.Board;
import ee.taltech.monopoly.network.ServerApi;
import javafx.application.Platform;

public class BoardPollerThread extends Thread {
    private boolean isRunning = true;

    @Override
    public void run() {
        while (isRunning) {
            Board.setInstance(ServerApi.getInstance().fetchBoard());
            Platform.runLater(() -> GameBoardController.getInstance().updateAllPlayersData());

            if (Board.getInstance().getCurrentPlayer().equals(Board.getInstance().getLocalPlayer())) {
                Platform.runLater(() -> GameBoardController.getInstance().startTurn());
                isRunning = false;
            }

            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

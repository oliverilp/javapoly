package ee.taltech.monopoly.gamelogic;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StaticStringKeeperTest {

    @Test
    void getTradeSuccessMessage() {
        assertEquals("Mängija 'Peter' võttis pakkumise vastu!", StaticStringKeeper.getTradeSuccessMessage("Peter"));
    }

    @Test
    void getTradeFailMessage() {
        assertEquals("Mängija 'Sander' ei võtnud pakkumist vastu!", StaticStringKeeper.getTradeFailMessage("Sander"));
    }
}

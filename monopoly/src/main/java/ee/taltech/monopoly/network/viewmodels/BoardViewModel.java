package ee.taltech.monopoly.network.viewmodels;

import ee.taltech.monopoly.gamelogic.Board;

public class BoardViewModel {
    private Board board;

    public BoardViewModel() {}

    public BoardViewModel(Board board) {
        this.board = board;
    }

    public Board getBoard() {
        return board;
    }
}

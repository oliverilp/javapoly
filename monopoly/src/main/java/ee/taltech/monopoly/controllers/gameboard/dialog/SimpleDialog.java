package ee.taltech.monopoly.controllers.gameboard.dialog;

import com.jfoenix.controls.*;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.*;

public class SimpleDialog {
    public static void show(String heading, String bodyText, StackPane rootStackPane) {
        JFXDialogLayout content = new JFXDialogLayout();
        content.setHeading(new Label(heading));
        Label bodyLabel = new Label(bodyText);
        bodyLabel.setWrapText(true);
        content.setBody(bodyLabel);

        JFXDialog dialog = new JFXDialog(rootStackPane, content, JFXDialog.DialogTransition.CENTER);

        JFXButton button = new JFXButton("OK");
        button.setOnAction(e1 -> dialog.close());
        StackPane buttonContainer = new StackPane(button);
        buttonContainer.setPadding(new Insets(0, 15, 15, 0));
        content.setActions(buttonContainer);

        dialog.show();
    }
}

package ee.taltech.monopoly.ai;

import ee.taltech.monopoly.ai.webquery.QuestionSolver;
import ee.taltech.monopoly.gamelogic.Board;
import ee.taltech.monopoly.gamelogic.Dice;
import ee.taltech.monopoly.gamelogic.Player;
import ee.taltech.monopoly.gamelogic.cards.Card;
import ee.taltech.monopoly.gamelogic.cards.CardDeck;
import ee.taltech.monopoly.gamelogic.street.Street;
import ee.taltech.monopoly.gamelogic.street.purchasable.PurchasableStreet;
import ee.taltech.monopoly.network.requests.TradeRequest;

/**
 * A Player who's decisions are maid by the machine.
 */
public class AIPlayer extends Player {
    private static CardDeck cardDeck = new CardDeck();

    /**
     * Necessary for tests and Kryonet.
     */
    public AIPlayer() {
    }

    public AIPlayer(String name, String id, Board board) {
        super(name, id, board);
    }

    /**
     * Move and handle all decisions regarding one turn.
     */
    public void playTurn(Board board) {
        if (getLocation(board).canBuy(this, board)) {
            final PurchasableStreet currentStreet = (PurchasableStreet) getLocation(board);
            if (new DecideStreetPurchasing(this, currentStreet, board).decide()) {
                currentStreet.purchaseStreet(this, board);
            }
        }

        if (getLocation(board).getName().equals("Juhus")) {
            System.out.println("AI got a change card");
            cardDeck.drawChanceCard(this, board);
        } else if (getLocation(board).getName().equals("Tunnikontroll")) {
            final Card card = cardDeck.drawCommunityCard();
            final String aiAnswer = QuestionSolver.answerQuestion(card.getQuestion(), card.getAnswerChoices());
            final boolean isAnswerCorrect = aiAnswer.equals(card.getAnswer());
            final String resultMessage = cardDeck.communityResult(this, board, isAnswerCorrect);
            System.out.println("AI git a community card. As a result: " + resultMessage);
        }
        final int diceRoll = Dice.rollDouble();
        final Street destinationStreet = board.move(board.getCurrentPlayer(), diceRoll);
        // pay rent
        System.out.println("players: " + board.getAllPlayers());
        if (destinationStreet instanceof PurchasableStreet
                && destinationStreet.getOwner(board) != null
                && !destinationStreet.getOwner(board).equals(this)) {
            ((PurchasableStreet) destinationStreet).payRent(this, diceRoll, board);
        }
        board.incrementCurrentPlayer();
    }

    public boolean considerTrade(TradeRequest tradeRequest, Board board) {
        return new TradingDecider(
                tradeRequest.getReceivingPlayer(),
                board,
                tradeRequest.getSenderStreets().values(),
                tradeRequest.getSenderMoney(),
                tradeRequest.getReceiverStreets().values(),
                tradeRequest.getReceiverMoney()
        ).decide();
    }
}

package ee.taltech.monopoly.services;

import ee.taltech.monopoly.Game;
import javafx.application.Platform;
import javafx.scene.control.ListView;
import javafx.scene.layout.VBox;

public class PlayerPollerThread extends Thread {
    private ListView<String> playersListView;
    private VBox vBox;

    public PlayerPollerThread(ListView<String> playersListView, VBox vBox) {
        this.playersListView = playersListView;
        this.vBox = vBox;
    }

    @Override
    public void run() {
        try {
            GameInitializer.pollPlayers(playersListView);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            Platform.runLater(() -> {
                Game.loadGameBoard(vBox.getWidth(), vBox.getHeight());
                Game.getStage().setScene(Game.getGameBoard());
            });
        }
    }
}

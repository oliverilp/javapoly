package ee.taltech.monopoly.controllers;

import com.jfoenix.controls.JFXButton;
import ee.taltech.monopoly.gamelogic.Board;
import ee.taltech.monopoly.services.GameInitializer;
import ee.taltech.monopoly.services.PlayerPollerThread;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.VBox;

import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;

public class StartGameController implements Initializable {
    private static Double width = 1050.0;
    private static Double height = 800.0;

    @FXML
    private JFXButton createButton;
    @FXML
    private Label gameIDLabel;
    @FXML
    private ListView<String> playersListView;
    @FXML
    private VBox vBox;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        vBox.setPrefWidth(width);
        vBox.setPrefHeight(height);

        Thread poller = new PlayerPollerThread(playersListView, vBox);
        poller.start();

        gameIDLabel.setText(Board.getInstance().getGameID());
        playersListView.setFocusTraversable(false);
        createButton.setOnAction(e -> GameInitializer.startGame());
    }

    public static void setWidth(Double width) {
        StartGameController.width = width;
    }

    public static void setHeight(Double height) {
        StartGameController.height = height;
    }
}

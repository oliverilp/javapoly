package ee.taltech.monopoly.controllers.gameboard;

import ee.taltech.monopoly.Game;
import javafx.scene.image.ImageView;

import java.util.HashMap;
import java.util.Map;

public class DiceIcon {
    private static Map<Integer, ImageView> leftIconsMap = new HashMap<>();
    private static Map<Integer, ImageView> rightIconsMap = new HashMap<>();

    private static void generateIcons(Map<Integer, ImageView> icons) {
        if (icons.isEmpty()) {
            for (int i = 1; i <= 6; i++) {
                String path = String.format("dice/dice-six-faces-%s.png", i);
                icons.put(i, new ImageView(Game.class.getResource(path).toString()));
            }
        }
    }

    public static Map<Integer, ImageView> getLeftIcons() {
        generateIcons(leftIconsMap);
        return leftIconsMap;
    }

    public static Map<Integer, ImageView> getRightIcons() {
        generateIcons(rightIconsMap);
        return rightIconsMap;
    }
}

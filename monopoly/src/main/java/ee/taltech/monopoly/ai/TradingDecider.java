package ee.taltech.monopoly.ai;

import ee.taltech.monopoly.gamelogic.Board;
import ee.taltech.monopoly.gamelogic.Player;
import ee.taltech.monopoly.gamelogic.street.purchasable.PurchasableStreet;

import java.util.Collection;
import java.util.HashSet;
import java.util.stream.Collectors;

public class TradingDecider {
    private final Player player;
    private final Board board;
    private final Collection<PurchasableStreet> senderStreets;
    private final int senderMoney;
    private final Collection<PurchasableStreet> receiverStreets;
    private final int receiverMoney;

    public TradingDecider(Player player, Board board,
                          Collection<Integer>senderStreetIndexes, int senderMoney,
                          Collection<Integer> receiverStreetIndexes, int receiverMoney) {
        this.player = player;
        this.board = board;
        this.senderStreets = getPurchasableStreetsFromIndexes(senderStreetIndexes);
        this.senderMoney = senderMoney;
        this.receiverStreets = getPurchasableStreetsFromIndexes(receiverStreetIndexes);
        this.receiverMoney = receiverMoney;
    }

    /**
     * Decide whether or not to accept the trade.
     */
    public boolean decide() {
        // Do not sell streets over which you have monopoly or possibility of obtaining it
        if (receiverStreets.stream().anyMatch(this::hasPossibilityOfMonopoly)) {
            return false;
        }
        int senderGoodsValue = calculateStreetsTotalValue(senderStreets) + senderMoney;
        int receiverGoodsValue = calculateStreetsTotalValue(receiverStreets) + receiverMoney;
        // Accept the trade it you are being offered at least 1.5 times the neto value of your goods
        return senderGoodsValue >= 1.5 * receiverGoodsValue;
    }

    private boolean hasPossibilityOfMonopoly(PurchasableStreet street) {
        return DecisionUtils.getNumberOfStreetsInStreetGroupNotOwnedByOtherPlayers(board, street, player) == 0;
    }

    private int calculateStreetsTotalValue(Collection<PurchasableStreet> streets) {
        return streets.stream().mapToInt(PurchasableStreet::getPrice).sum();
    }

    /**
     * Convert street indexes into instances of PurchasableStreet.
     * Since only PurchasableStreets can be traded with, no type casting exceptions should occur.
     */
    private Collection<PurchasableStreet> getPurchasableStreetsFromIndexes(Collection<Integer> streetIndexes) {
        return streetIndexes.stream().map(
                streetIndex -> ((PurchasableStreet) board.getStreetAt(streetIndex)))
                .collect(Collectors.toCollection(HashSet::new));
    }
}

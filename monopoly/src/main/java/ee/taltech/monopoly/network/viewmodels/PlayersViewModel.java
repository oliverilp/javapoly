package ee.taltech.monopoly.network.viewmodels;

import ee.taltech.monopoly.gamelogic.Player;
import ee.taltech.monopoly.network.GamePhase;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class PlayersViewModel {
    private GamePhase phase = GamePhase.PLAYERS_GATHERING;
    private boolean connectionSuccessful;
    private List<Player> playerList = new ArrayList<>();

    public PlayersViewModel() {
    }

    /**
     * Used when a new game is created to instantly add the creator to the players list
     *
     * @param player the creator of the game
     */
    public PlayersViewModel(Player player) {
        playerList.add(player);
    }

    public static PlayersViewModel ofInvalidID() {
        final PlayersViewModel viewModel = new PlayersViewModel();
        viewModel.connectionSuccessful = false;
        return viewModel;
    }

    public PlayersViewModel(PlayersViewModel another) {
        this.phase = another.phase;
        this.playerList = another.playerList;
    }

    public GamePhase getPhase() {
        return phase;
    }

    public void setPhase(GamePhase phase) {
        this.phase = phase;
    }

    public List<Player> getPlayerList() {
        return playerList;
    }

    public List<String> getPlayerNames() {
        return playerList.stream()
                .map(Player::getName)
                .collect(Collectors.toList());
    }

    public void setPlayerList(List<Player> playerList) {
        this.playerList = playerList;
    }
}

package ee.taltech.monopoly.gamelogic;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BoardTest {
    Board board;
    Player player;
    Player playerTwo;

    @BeforeEach
    void setUp() {
        board = new Board();
        player = new Player("Name", "id1", board);
        playerTwo = new Player("Name2", "id2", board);
    }

    @Test
    void move() {
        player.setLocation(board.getStreetAt(39));
        board.move(player, 4);
        assertEquals(player.getLocation(board), board.getStreetAt(3));
    }

    @Test
    void getInstance() {
        Board.setInstance(board);
        assertEquals(board, Board.getInstance());
        Board.setInstance(new Board());

    }

    @Test
    void getAllPlayers() {
        board.setAllPlayers(Arrays.asList(player, playerTwo));
        assertEquals(Arrays.asList(player, playerTwo), board.getAllPlayers());
    }

    @Test
    void getOtherPlayers() {
    }

    @Test
    void getCurrentPlayer() {
        board.setAllPlayers(Arrays.asList(player, playerTwo));
        board.setCurrentPlayer(player);
        assertEquals(player, board.getCurrentPlayer());
    }

    @Test
    void getLocalPlayer() {
        LocalPlayer.nullLocalPlayer();
        LocalPlayer.initialize("Local", "id");
        board.setAllPlayers(Arrays.asList(player, playerTwo, LocalPlayer.getInstance()));
        assertEquals(LocalPlayer.getInstance(), board.getLocalPlayer());
        LocalPlayer.nullLocalPlayer();
    }

    @Test
    void incrementCurrentPlayer() {
        board.setAllPlayers(Arrays.asList(player, playerTwo));
        board.setCurrentPlayer(player);
        assertEquals(player, board.getCurrentPlayer());
        board.incrementCurrentPlayer();
        assertEquals(playerTwo, board.getCurrentPlayer());
    }
}

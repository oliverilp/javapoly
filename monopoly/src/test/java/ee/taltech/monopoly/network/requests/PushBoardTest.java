package ee.taltech.monopoly.network.requests;

import ee.taltech.monopoly.gamelogic.Board;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PushBoardTest {
    Board board = new Board();
    PushBoard pushBoard = new PushBoard();

    @BeforeEach
    void setUp() {
        pushBoard = new PushBoard("id", board);
    }

    @Test
    void getBoard() {
        assertEquals(board, pushBoard.getBoard());
    }
}

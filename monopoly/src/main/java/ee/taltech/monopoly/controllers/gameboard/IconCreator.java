package ee.taltech.monopoly.controllers.gameboard;

import ee.taltech.monopoly.gamelogic.PieceType;
import javafx.geometry.Insets;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.SVGPath;

public class IconCreator {
    private final double DEFAULT_SIZE = 0.7;
    private final String COLOR_PRIMARY = "#607985";
    private String colorSecondary;

    public IconCreator(String colorSecondary) {
        this.colorSecondary = colorSecondary;
    }

    public StackPane getIcon(PieceType pieceType) {
        switch (pieceType) {
            case ARROW:
                return arrow();
            case CAR:
                return car();
            case KEY:
                return key();
            case LOCK:
                return lock();
            case PIN:
                return pin();
            case SHOPPING_CART:
                return shoppingCart();
            case TRUCK:
                return truck();
            default:
                return airplane();
        }
    }

    public StackPane lock() {
        return lock(DEFAULT_SIZE);
    }

    public StackPane lock(double sizeMultiplier) {
        String svgContent = "M3.7,7.5c0-0.6,0.5-1,1-1h8.6c0.5,0,1,0.4,1,1V15c0,0.6-0.5,1-1,1H4.7c-0.6,0-1-0.4-1-1V7.5z";
        StackPane svgStackPane = createIcon(svgContent, colorSecondary, 26 * sizeMultiplier, 22 * sizeMultiplier);
        svgStackPane.setPadding(new Insets(13 * sizeMultiplier, 0, 0, 0));

        String svgContent1 = "M13.5,6h-0.75V4.5c0-2.07-1.68-3.75-3.75-3.75S5.25,2.43,5.25,4.5V6H4.5C3.68,6,3,6.67,3,7.5V15c0,0.83,0.68,1.5,1.5,1.5h9\n" +
                "c0.83,0,1.5-0.67,1.5-1.5V7.5C15,6.67,14.33,6,13.5,6z M6.75,4.5c0-1.24,1.01-2.25,2.25-2.25s2.25,1.01,2.25,2.25V6h-4.5V4.5z\n" +
                " M13.5,15h-9V7.5h9V15z M9,12.75c0.83,0,1.5-0.67,1.5-1.5S9.83,9.75,9,9.75s-1.5,0.67-1.5,1.5S8.17,12.75,9,12.75z";
        StackPane svgStackPane1 = createIcon(svgContent1, COLOR_PRIMARY, 32.0 * sizeMultiplier, 42.0 * sizeMultiplier);

        StackPane svgContainer = new StackPane(svgStackPane, svgStackPane1);
        svgContainer.setPrefSize(32.0 * sizeMultiplier, 42.0 * sizeMultiplier);
        return svgContainer;
    }

    public StackPane airplane() {
        return airplane(DEFAULT_SIZE);
    }

    public StackPane airplane(double sizeMultiplier) {
        String svgContent = "M22,16v-2l-8.5-5V3.5C13.5,2.67,12.83,2,12,2s-1.5,0.67-1.5,1.5V9L2,14v2l8.5-2.5V19L8,20.5L8,22l4-1l4,1l0-1.5L13.5,19 v-5.5L22,16z";
        StackPane svgStackPane = createIcon(svgContent, COLOR_PRIMARY, 40 * sizeMultiplier, 40 * sizeMultiplier);

        StackPane svgContainer = new StackPane(svgStackPane);
        svgContainer.setPrefSize(40 * sizeMultiplier, 40 * sizeMultiplier);
        return svgContainer;
    }

    public StackPane shoppingCart() {
        return shoppingCart(DEFAULT_SIZE);
    }

    public StackPane shoppingCart(double sizeMultiplier) {
        String svgContent = "M15.6,11l2.8-5H6.2l2.4,5H15.6z";
        StackPane svgStackPane = createIcon(svgContent, colorSecondary, 30 * sizeMultiplier, 12 * sizeMultiplier);
        svgStackPane.setPadding(new Insets(-15 * sizeMultiplier, 0, 0, 8 * sizeMultiplier));

        String svgContent1 = "M15.6,13c0.7,0,1.4-0.4,1.7-1l3.6-6.5C21.3,4.8,20.8,4,20,4H5.2L4.3,2H1v2h2l3.6,7.6L5.3,14c-0.7,1.3,0.2,3,1.8,3h12v-2H7\n" +
                "l1.1-2H15.6z M6.2,6h12.1l-2.8,5h-7L6.2,6z M7,18c-1.1,0-2,0.9-2,2s0.9,2,2,2s2-0.9,2-2S8.1,18,7,18z M17,18c-1.1,0-2,0.9-2,2\n" +
                "s0.9,2,2,2s2-0.9,2-2S18.1,18,17,18z";
        StackPane svgStackPane1 = createIcon(svgContent1, COLOR_PRIMARY, 40 * sizeMultiplier, 40 * sizeMultiplier);

        StackPane svgContainer = new StackPane(svgStackPane, svgStackPane1);
        svgContainer.setPrefSize(40 * sizeMultiplier, 40 * sizeMultiplier);
        return svgContainer;
    }

    public StackPane key() {
        return key(DEFAULT_SIZE);
    }

    public StackPane key(double sizeMultiplier) {
        String svgContent = "M11.7,10.3C11,8.3,9.1,7,7,7c-2.8,0-5,2.2-5,5s2.2,5,5,5c2.1,0,4-1.3,4.7-3.3l0.2-0.7H18v4h2v-4h2v-2H11.9\n" +
                "L11.7,10.3z M7,15c-1.7,0-3-1.4-3-3s1.3-3,3-3s3,1.4,3,3S8.6,15,7,15z";
        StackPane svgStackPane = createIcon(svgContent, colorSecondary, 40 * sizeMultiplier, 20 * sizeMultiplier);

        String svgContent1 = "M7,5c-3.9,0-7,3.1-7,7s3.1,7,7,7c2.7,0,5.2-1.6,6.3-4H16v4h6v-4h2V9H13.3C12.2,6.6,9.7,5,7,5z M22,13h-2v4h-2v-4h-6.1\n" +
                "l-0.2,0.7C11,15.7,9.1,17,7,17c-2.8,0-5-2.2-5-5s2.2-5,5-5c2.1,0,4,1.3,4.7,3.3l0.2,0.7H22V13z M7,9c-1.7,0-3,1.4-3,3s1.3,3,3,3\n" +
                "s3-1.4,3-3S8.6,9,7,9z M7,13c-0.6,0-1-0.4-1-1s0.4-1,1-1s1,0.4,1,1S7.6,13,7,13z";
        StackPane svgStackPane1 = createIcon(svgContent1, COLOR_PRIMARY, 48 * sizeMultiplier, 28 * sizeMultiplier);

        StackPane svgContainer = new StackPane(svgStackPane, svgStackPane1);
        svgContainer.setPrefSize(48 * sizeMultiplier, 28 * sizeMultiplier);
        return svgContainer;
    }

    public StackPane car() {
        return car(DEFAULT_SIZE);
    }

    public StackPane car(double sizeMultiplier) {
        String svgContent = "M4.4,11.5h15.1v6H4.4V11.5z";
        StackPane svgStackPane = createIcon(svgContent, colorSecondary, 30 * sizeMultiplier, 12 * sizeMultiplier);
        svgStackPane.setPadding(new Insets(5.5 * sizeMultiplier, 0, 0, 0));

        StackPane svgStackPane1 = createCircle(sizeMultiplier);
        svgStackPane1.setPadding(new Insets(5.5 * sizeMultiplier, 15 * sizeMultiplier, 0, 0));

        StackPane svgStackPane2 = createCircle(sizeMultiplier);
        svgStackPane2.setPadding(new Insets(5.5 * sizeMultiplier, 0, 0, 15 * sizeMultiplier));

        String svgContent1 = "M18.9,6c-0.2-0.6-0.8-1-1.4-1h-11C5.8,5,5.3,5.4,5.1,6L3,12v8c0,0.5,0.5,1,1,1h1c0.6,0,1-0.5,1-1v-1h12v1c0,0.5,0.5,1,1,1h1\n" +
                "c0.5,0,1-0.5,1-1v-8L18.9,6z M6.8,7h10.3l1.1,3.1H5.8L6.8,7z M19,17H5v-5h14V17z";
        StackPane svgStackPane3 = createIcon(svgContent1, COLOR_PRIMARY, 36 * sizeMultiplier, 32 * sizeMultiplier);

        StackPane svgContainer = new StackPane(svgStackPane, svgStackPane1, svgStackPane2, svgStackPane3);
        svgContainer.setPrefSize(36 * sizeMultiplier, 32 * sizeMultiplier);
        return svgContainer;
    }

    public StackPane truck() {
        return truck(DEFAULT_SIZE);
    }

    public StackPane truck(double sizeMultiplier) {
        String svgContent = "M16.2,15.4h-14V5.1h14V15.4z";
        StackPane svgStackPane = createIcon(svgContent, colorSecondary, 26 * sizeMultiplier, 20 * sizeMultiplier);
        svgStackPane.setPadding(new Insets(0, 13 * sizeMultiplier, 6 * sizeMultiplier, 0));

        String svgContent1 = "M17,8V4H3C1.9,4,1,4.9,1,6v11h2c0,1.7,1.3,3,3,3s3-1.3,3-3h6c0,1.7,1.3,3,3,3s3-1.3,3-3h2v-5l-3-4H17z M6,18\n" +
                "c-0.6,0-1-0.5-1-1s0.4-1,1-1s1,0.5,1,1S6.6,18,6,18z M15,15H8.2c-0.6-0.6-1.3-1-2.2-1s-1.7,0.4-2.2,1H3V6h12V15z M18,18\n" +
                "c-0.5,0-1-0.5-1-1s0.5-1,1-1s1,0.5,1,1S18.5,18,18,18z M17,12V9.5h2.5l2,2.5H17z";
        StackPane svgStackPane1 = createIcon(svgContent1, COLOR_PRIMARY, 44 * sizeMultiplier, 32 * sizeMultiplier);

        StackPane svgContainer = new StackPane(svgStackPane, svgStackPane1);
        svgContainer.setPrefSize(44 * sizeMultiplier, 32 * sizeMultiplier);
        return svgContainer;
    }

    public StackPane arrow() {
        return arrow(DEFAULT_SIZE);
    }

    public StackPane arrow(double sizeMultiplier) {
        String svgContent = "M7.7,17.7l3.5-1.5l0.8-0.4l0.8,0.4l3.5,1.5L12,7.3L7.7,17.7z";
        StackPane svgStackPane = createIcon(svgContent, colorSecondary, 20 * sizeMultiplier, 22 * sizeMultiplier);
        svgStackPane.setPadding(new Insets(5 * sizeMultiplier, 0, 0, 0));

        String svgContent1 = "M4.5,20.3L5.2,21l6.8-3l6.8,3l0.7-0.7L12,2L4.5,20.3z M12.8,16.2L12,15.8l-0.8,0.4l-3.5,1.5L12,7.3l4.3,10.4\n" +
                "C16.3,17.7,12.8,16.2,12.8,16.2z";
        StackPane svgStackPane1 = createIcon(svgContent1, COLOR_PRIMARY, 30 * sizeMultiplier, 38 * sizeMultiplier);

        StackPane svgContainer = new StackPane(svgStackPane, svgStackPane1);
        svgContainer.setPrefSize(30 * sizeMultiplier, 38 * sizeMultiplier);
        return svgContainer;
    }

    public StackPane pin() {
        return pin(DEFAULT_SIZE);
    }

    public StackPane pin(double sizeMultiplier) {
        String svgContent = "M18.5,10.2c0,2.6-2.1,5.8-6.2,9.5L12,20l-0.3-0.3c-4.1-3.7-6.2-6.9-6.2-9.5c0-3.8,2.8-6.7,6.5-6.7\n" +
                "S18.5,6.3,18.5,10.2z";
        StackPane svgStackPane = createIcon(svgContent, colorSecondary, 26 * sizeMultiplier, 34 * sizeMultiplier);

        String svgContent1 = "M12,2c4.2,0,8,3.2,8,8.2c0,3.3-2.7,7.3-8,11.8c-5.3-4.5-8-8.5-8-11.8C4,5.2,7.8,2,12,2z M18,10.2C18,6.6,15.4,4,12,4\n" +
                "s-6,2.6-6,6.2c0,2.3,1.9,5.4,6,9.1C16,15.6,18,12.5,18,10.2z M12,12c-1.1,0-2-0.9-2-2s0.9-2,2-2s2,0.9,2,2S13.1,12,12,12z";
        StackPane svgStackPane1 = createIcon(svgContent1, COLOR_PRIMARY, 32 * sizeMultiplier, 40 * sizeMultiplier);

        StackPane svgContainer = new StackPane(svgStackPane, svgStackPane1);
        svgContainer.setPrefSize(32 * sizeMultiplier, 40 * sizeMultiplier);
        return svgContainer;
    }

    private StackPane createIcon(String content, String color, double width, double height) {
        SVGPath svg = new SVGPath();
        svg.setContent(content);
        svg.setFill(Color.valueOf(color));
        resize(svg, width, height);

        StackPane svgStackPane = new StackPane();
        svgStackPane.getChildren().add(svg);
        return svgStackPane;
    }

    private StackPane createCircle(double sizeMultiplier) {
        String svgContent = "M7.5,13C8.3,13,9,13.7,9,14.5S8.3,16,7.5,16S6,15.3,6,14.5S6.7,13,7.5,13z";
        return createIcon(svgContent, COLOR_PRIMARY, 6 * sizeMultiplier, 6 * sizeMultiplier);
    }

    private void resize(SVGPath svg, double width, double height) {
        double originalWidth = svg.prefWidth(-1);
        double originalHeight = svg.prefHeight(originalWidth);

        double scaleX = width / originalWidth;
        double scaleY = height / originalHeight;

        svg.setScaleX(scaleX);
        svg.setScaleY(scaleY);
    }
}

package com.example.test.spring.ui;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Span;

public class StreetComponent extends Span {
    // TODO in future this should be reference to Street
    private final int index;

    public StreetComponent(int index) {
        getStyle().set("color", "yellow");
        this.index = index;
        setText(String.valueOf(index));
    }

    public int getIndex() {
        return index;
    }
}

package ee.taltech.monopoly.gamelogic.street.purchasable;

import ee.taltech.monopoly.gamelogic.Board;
import ee.taltech.monopoly.gamelogic.Player;
import ee.taltech.monopoly.gamelogic.street.StreetGroup;

import java.util.Map;

public class RailroadStreet extends PurchasableStreet {
    public RailroadStreet() {}

    /**
     * Creates Purchasable street based on the real Railroad Street street.
     *
     * @param name street name.
     * @param price set price for street.
     * @param streetGroup set StreetGroup.
     * @param index location on board.
     */
    public RailroadStreet(String name, Integer price, StreetGroup streetGroup, int index) {
        this.name = name;
        this.price = price;
        this.streetGroup = streetGroup;
        this.rent = 25;
        this.index = index;
    }

    /*
     * Rent is calculated by how many railroad streets the owner has, base is 25 * owned amount.
     */
    @Override
    public int getRent(Player payer, int diceRoll, Board board) {
        if (getOwner(board) != null) {
            Map<StreetGroup, Integer> ownerStreets = getOwner(board).getPlayerStreetGroups(board);
            int multiplier = ownerStreets.get(StreetGroup.RAILROAD);
            return multiplier == 1 ? 25 : multiplier == 2 ? 50 : multiplier == 3 ? 100 : multiplier == 4 ? 200 : 0;
        }
        return 0;
    }
}

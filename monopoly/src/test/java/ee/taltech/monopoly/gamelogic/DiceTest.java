package ee.taltech.monopoly.gamelogic;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class DiceTest {

    @Test
    void roll() {
        int rolled = Dice.rollDouble();
        assertTrue(12 >= rolled  && rolled >= 2);
    }
}

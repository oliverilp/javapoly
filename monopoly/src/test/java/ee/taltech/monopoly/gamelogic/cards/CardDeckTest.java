package ee.taltech.monopoly.gamelogic.cards;

import ee.taltech.monopoly.gamelogic.Board;
import ee.taltech.monopoly.gamelogic.LocalPlayer;
import ee.taltech.monopoly.gamelogic.Player;
import ee.taltech.monopoly.gamelogic.StaticStringKeeper;
import ee.taltech.monopoly.gamelogic.street.StreetGroup;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

class CardDeckTest {
    CardDeck cardDeck;
    Player player;
    Board board = new Board();

    @BeforeEach
    void setUp() {
        cardDeck = new CardDeck();
        player = new Player("Name", "id", board);
        board  = new Board();
        board.setAllPlayers(Collections.singletonList(player));
        LocalPlayer.setInstance(player);
    }

    @Test
    void setStuff() {
        cardDeck.setStuff(CardDeck.Type.COMMUNITY);
        cardDeck.setStuff(CardDeck.Type.CHANCE);
    }

    @Test
    void drawCardCorrect() {
        cardDeck.setCurrentCard(new Card("Yes",Arrays.asList("banana", "orange", "answer"), "no")) ;
        cardDeck.setReward(CardDeck.Reward.GO_TO_GO);
        cardDeck.setPunish(CardDeck.Punishment.GO_TO_JAIL);
        cardDeck.communityResult(player, board, true);
        assertEquals(board.getStreetAt(0), player.getLocation(board));
    }

    @Test
    void drawCardWrong() {
        cardDeck.setCurrentCard(new Card("Yes",Arrays.asList("banana", "orange", "answer"), "no")) ;
        cardDeck.setReward(CardDeck.Reward.GO_TO_GO);
        cardDeck.setPunish(CardDeck.Punishment.GO_TO_JAIL);
        cardDeck.communityResult(player, board, false);
        assertEquals(board.getStreetAt(10), player.getLocation(board));
    }

    @Test
    void drawChanceCard() {
        for (int i= 0; i < 55; i++) {
            player.setLocation(board.getStreetAt(30));
            assertEquals(String.class, cardDeck.drawChanceCard(player, board).getClass());
        }
    }

    @Test
    void quePunishAndQueRewardReturnNUll() {
        assertNull(cardDeck.quePunish(CardDeck.Punishment.TEST_ONLY, player, board));
        assertNull(cardDeck.queReward(CardDeck.Reward.TEST_ONLY, player, board));
    }

    @Test
    void drawCommunityCard() {
        assertEquals(Card.class, cardDeck.drawCommunityCard().getClass());
    }

    @Test
    void queRewardCaseGoToGo() {
        cardDeck.queReward(CardDeck.Reward.GO_TO_GO, player, board);
        assertEquals(board.getStreetAt(0), player.getLocation(board));
        assertEquals(StaticStringKeeper.GO_TO_GO_OUTPUT_MESSAGE, cardDeck.queReward(CardDeck.Reward.GO_TO_GO, player, board));
    }

    @Test
    void queRewardCaseCollectXEap() {
        cardDeck.queReward(CardDeck.Reward.COLLECT_X_EAP, player, board);
        assertTrue(1700 >= player.getPlayerBalance() && player.getPlayerBalance() >= 1525);
    }

    @Test
    void queRewardCaseCollectXEapFromOtherPlayers() {
        LocalPlayer.nullLocalPlayer();
        Player player2 = new Player("Name2", "id2", board);
        board.setAllPlayers(Collections.singletonList(player2));
        LocalPlayer.initialize("Name", "id");
        cardDeck.queReward(CardDeck.Reward.GET_X_EAP_FROM_EVERY_PLAYER, player, board);
        assertTrue(1475 >= player2.getPlayerBalance() && player2.getPlayerBalance() >= 1450);
        assertTrue(1550 >= player.getPlayerBalance() && player.getPlayerBalance() >= 1525);
        LocalPlayer.nullLocalPlayer();
    }

    @Test
    void queRewardGoToUtility() {
        assertEquals(board.getStreetAt(0), player.getLocation(board));
        cardDeck.queReward(CardDeck.Reward.GO_TO_NEXT_UTILITY, player, board);
        assertEquals(board.getStreetAt(12), player.getLocation(board));
        assertEquals(StreetGroup.UTILITY, player.getLocation(board).getStreetGroup());
        assertEquals(StaticStringKeeper.GO_TO_NEXT_UTILITY_OUTPUT_MESSAGE,
                cardDeck.queReward(CardDeck.Reward.GO_TO_NEXT_UTILITY, player, board));
        player.setLocation(board.getStreetAt(35));
        assertEquals(StaticStringKeeper.GO_TO_NEXT_UTILITY_OUTPUT_MESSAGE_PASS_GO,
                cardDeck.queReward(CardDeck.Reward.GO_TO_NEXT_UTILITY, player, board));
        assertEquals(1700, player.getPlayerBalance());

    }

    @Test
    void queRewardGoToRailroad() {
        player.setLocation(board.getStreetAt(2));
        assertEquals(board.getStreetAt(2), player.getLocation(board));
        cardDeck.queReward(CardDeck.Reward.GO_TO_NEXT_RAILROAD, player, board);
        assertEquals(StreetGroup.RAILROAD, player.getLocation(board).getStreetGroup());
        assertEquals(5, player.getLocation(board).getIndex());
        assertEquals(StaticStringKeeper.GO_TO_NEXT_RAILROAD_OUTPUT_MESSAGE, cardDeck.queReward(CardDeck.Reward.GO_TO_NEXT_RAILROAD, player, board));

        player.setLocation(board.getStreetAt(36));
        assertEquals(StaticStringKeeper.GO_TO_NEXT_RAILROAD_OUTPUT_MESSAGE_PASS_GO, cardDeck.queReward(CardDeck.Reward.GO_TO_NEXT_RAILROAD, player, board));
        assertEquals(board.getStreetAt(5), player.getLocation(board));
        assertEquals(1700, player.getPlayerBalance());
    }

    @Test
    void queRewardGoToXStreet() {
        assertEquals(board.getStreetAt(0), player.getLocation(board));
        cardDeck.queReward(CardDeck.Reward.GO_TO_X_STREET, player, board);
        assertTrue(6 >= board.getStreets().indexOf(player.getLocation(board))
                && board.getStreets().indexOf(player.getLocation(board)) >= 2);
        assertEquals(StaticStringKeeper.GO_TO_X_STREET_OUTPUT_MESSAGE, cardDeck.queReward(CardDeck.Reward.GO_TO_X_STREET, player, board));
        player.setLocation(board.getStreetAt(39));
        assertEquals(StaticStringKeeper.GO_TO_X_STREET_OUTPUT_MESSAGE_PASS_GO, cardDeck.queReward(CardDeck.Reward.GO_TO_X_STREET, player, board));
        assertEquals(1700, player.getPlayerBalance());

    }

    @Test
    void quePunishGoToJail() {
        cardDeck.quePunish(CardDeck.Punishment.GO_TO_JAIL, player, board);
        assertEquals(board.getStreetAt(10), player.getLocation(board));
        assertEquals(StaticStringKeeper.GO_TO_JAIL_DECK_OUTPUT_MESSAGE, cardDeck.quePunish(CardDeck.Punishment.GO_TO_JAIL, player, board));
    }

    @Test
    void quePunishPayXEap() {
        cardDeck.quePunish(CardDeck.Punishment.PAY_X_EAP, player, board);
        assertTrue(1475 >= player.getPlayerBalance() && player.getPlayerBalance() >= 1350);
    }

    @Test
    void quePunishGoBack3Times() {
        player.setLocation(board.getStreetAt(10));
        cardDeck.quePunish(CardDeck.Punishment.GO_BACK_THREE_TIMES, player, board);
        assertEquals(board.getStreetAt(7), player.getLocation(board));
        assertEquals(StaticStringKeeper.GO_BACK_3_STEPS_OUTPUT_MESSAGE, cardDeck.quePunish(CardDeck.Punishment.GO_BACK_THREE_TIMES, player, board));
    }

    @Test
    void quePunishPayEveryPlayerXEap() {
        LocalPlayer.nullLocalPlayer();
        Player player2 = new Player("Name2", "id2", board);
        board.setAllPlayers(Collections.singletonList(player2));
        LocalPlayer.initialize("Name", "id");
        cardDeck.quePunish(CardDeck.Punishment.PAY_EVERY_PLAYER_X_EAP, player, board);
        assertEquals(1550, player2.getPlayerBalance());
        assertEquals(1450, player.getPlayerBalance());
        LocalPlayer.nullLocalPlayer();
    }

    @Test
    void getRandomItemPun() {
        assertEquals(CardDeck.Punishment.GO_TO_JAIL, CardDeck.getRandomItemPun(Collections.singletonList(CardDeck.Punishment.GO_TO_JAIL)));
    }

    @Test
    void getRandomItemRew() {
        assertEquals(CardDeck.Reward.GO_TO_GO, CardDeck.getRandomItemRew(Collections.singletonList(CardDeck.Reward.GO_TO_GO)));
    }

    @Test
    void getRandomInt() {
        assertEquals(CardDeck.Reward.GO_TO_GO, CardDeck.getRandomItemRew(Collections.singletonList(CardDeck.Reward.GO_TO_GO)));
    }

    @Test
    void getRandomCard() {
        Card card = new Card("Yes",Arrays.asList("banana", "orange", "answer"), "no");
        assertEquals(card, CardDeck.getRandomCard(Collections.singletonList(card)));
    }
}

package ee.taltech.monopoly.gamelogic.street.purchasable;

import ee.taltech.monopoly.gamelogic.Board;
import ee.taltech.monopoly.gamelogic.Player;
import ee.taltech.monopoly.gamelogic.street.StreetGroup;

import java.util.Map;

public class UtilityStreet extends PurchasableStreet {
    public UtilityStreet() {}

    /**
     * Creates Purchasable street based on the real Utility Street street.
     *
     * @param name street name.
     * @param price set price for street.
     * @param streetGroup set StreetGroup.
     * @param index location on board.
     */
    public UtilityStreet(String name, Integer price, StreetGroup streetGroup, int index) {
        this.name = name;
        this.price = price;
        this.streetGroup = streetGroup;
        this.index = index;
    }

    /**
     * Rent for this type of street is determined by you dice roll and how many streets the owner has.
     * If you own 1 then 4 * you dice roll, if 2 then 10 * dice roll
     *
     * @param diceRoll dice rolled to land on this street.
     * @param payer player who pays rent.
     */
    public int getRent(Player payer, int diceRoll, Board board) {
        if (getOwner(board) != null) {
            Map<StreetGroup, Integer> ownerStreets = getOwner(board).getPlayerStreetGroups(board);
            int multiplier = ownerStreets.get(StreetGroup.UTILITY);
            if (multiplier == 1) {
                return 4 * diceRoll;
            } else if (multiplier == 2) {
                return 10 * diceRoll;
            }
        }
        return 0;
    }

}

package ee.taltech.monopoly.gamelogic.street.nonpurchasable;

public class MockNonPurchasableStreet extends NonPurchasableStreet {
    /*
     * Only used for testing, has to be in this package.
     * Just ignore.
     */
}

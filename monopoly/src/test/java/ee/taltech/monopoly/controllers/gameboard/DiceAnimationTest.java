package ee.taltech.monopoly.controllers.gameboard;

import javafx.embed.swing.JFXPanel;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DiceAnimationTest {
    private JFXPanel panel = new JFXPanel();
    private StackPane leftDiceStackPane;
    private StackPane rightDiceStackPane;
    private Duration scaleTransitionDuration;
    private DiceAnimation diceAnimation;

    @BeforeEach
    void setUp() {
        leftDiceStackPane = new StackPane();
        rightDiceStackPane = new StackPane();
        scaleTransitionDuration = Duration.seconds(1);
        diceAnimation = new DiceAnimation(leftDiceStackPane, rightDiceStackPane, scaleTransitionDuration);
    }

    @Test
    void playBeginningAnimation() {
        diceAnimation.playBeginningAnimation(1, 1);
    }

    @Test
    void playEndAnimation() {
        diceAnimation.playEndAnimation();
    }
}

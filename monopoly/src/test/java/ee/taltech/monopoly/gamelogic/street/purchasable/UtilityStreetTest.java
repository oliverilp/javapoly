package ee.taltech.monopoly.gamelogic.street.purchasable;

import ee.taltech.monopoly.gamelogic.Board;
import ee.taltech.monopoly.gamelogic.Player;
import ee.taltech.monopoly.gamelogic.street.StreetGroup;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;


public class UtilityStreetTest {
    Board board = new Board();
    UtilityStreet utilityStreetEmpty = new UtilityStreet();
    UtilityStreet utilityStreet = new UtilityStreet("Name1", 50, StreetGroup.UTILITY, 12);
    UtilityStreet utilityStreet2 = new UtilityStreet("Name2", 50, StreetGroup.UTILITY, 28);
    Player owner = new Player("Name1", "id_1", board);
    Player payer = new Player("Name2", "id_2", board);

    @BeforeEach
    void setup() {
        board.getStreets().set(12, utilityStreet);
        board.getStreets().set(28, utilityStreet2);

        if (board.getAllPlayers().isEmpty()) {
            board.setAllPlayers(Arrays.asList(owner, payer));
        }
    }

    @Test
    public void canBuySellHouse() {
        assertFalse(utilityStreet.canBuyHouses(board));
        assertFalse(utilityStreet.canSellHouses(board));
    }

    @Test
    public void getRent() {
        assertEquals(0, utilityStreet.getRent(payer, 2, board));
        utilityStreet.purchaseStreet(owner, board);
        System.out.println(board.getAllPlayers());
        System.out.println(board.getAllPlayers().get(0).getPlayerStreets(board));
        System.out.println(board.getPlayer(owner.getID()));
        System.out.println(utilityStreet.getOwner(board));
        System.out.println(utilityStreet.getOwner(board));
        assertEquals(8, utilityStreet.getRent(payer, 2, board));
        utilityStreet2.purchaseStreet(owner, board);
        assertEquals(20, utilityStreet.getRent(payer, 2, board));

    }
}

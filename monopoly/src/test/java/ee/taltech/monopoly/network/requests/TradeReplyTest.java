package ee.taltech.monopoly.network.requests;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TradeReplyTest {
    TradeReply tradeReplyEmpty = new TradeReply();
    TradeReply tradeReply = new TradeReply("id", true);

    @Test
    void isAccepted() {
        assertTrue(tradeReply.isAccepted());
    }
}

package ee.taltech.monopoly.gamelogic;

import ee.taltech.monopoly.gamelogic.street.nonpurchasable.GoStreet;
import ee.taltech.monopoly.gamelogic.street.nonpurchasable.GoToJailStreet;
import ee.taltech.monopoly.gamelogic.street.nonpurchasable.JailStreet;
import ee.taltech.monopoly.gamelogic.street.nonpurchasable.ParkingStreet;
import ee.taltech.monopoly.gamelogic.street.purchasable.RailroadStreet;
import ee.taltech.monopoly.gamelogic.street.purchasable.RegularStreet;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

class PlayerTest {
    Board board = new Board();
    Player player = new Player("Name", "id", board);
    Player player2 = new Player("Name2", "id2", board);
    GoStreet goStreet = new GoStreet(0);
    GoToJailStreet goToJailStreet = new GoToJailStreet(30);
    GoToJailStreet goToJailStreetEmpty = new GoToJailStreet();
    JailStreet jailStreet = new JailStreet(10);
    JailStreet jailStreetEmpty = new JailStreet();
    ParkingStreet parkingStreet = new ParkingStreet(20);
    ParkingStreet parkingStreetEmpty = new ParkingStreet();

    @Test
    void getID() {
        assertEquals("id", player.getID());
    }

    @Test
    void getNameAndToString() {
        assertEquals("Name", player.getName());
        assertEquals("Name", player.toString());
    }

    @Test
    void getLocation() {
        player.setLocation(goStreet);
        assertEquals(goStreet, player.getLocation(board));

        player.setLocation(goToJailStreet);
        assertEquals(goToJailStreet, player.getLocation(board));

        player.setLocation(jailStreet);
        assertEquals(jailStreet, player.getLocation(board));

        player.setLocation(parkingStreet);
        assertEquals(parkingStreet, player.getLocation(board));
    }

    @SuppressWarnings("SimplifiableJUnitAssertion")
    @Test
    void testEqualsAndHash() {
        Player playerX = new Player("Name", "id", board);
        Player playerY = new Player("Name", "id", board);
        assertTrue(playerX.equals(playerY) && playerY.equals(playerX));
        assertTrue(playerX.hashCode() == playerY.hashCode());
    }

    @Test
    void trade() {
        /*
         * Set stuff.
         */
        board.setAllPlayers(Arrays.asList(player, player2));
        RegularStreet purchasableStreet = (RegularStreet) board.getStreetAt(1);
        RailroadStreet purchasableStreet2 = (RailroadStreet) board.getStreetAt(5);

        /*
         * Normal purchase as ownership.
         */
        purchasableStreet.purchaseStreet(player, board);
        purchasableStreet2.purchaseStreet(player2, board);
        assertEquals(1440, player.getPlayerBalance());
        assertEquals(1300, player2.getPlayerBalance());
        assertEquals(player, purchasableStreet.getOwner(board));
        assertEquals(player2, purchasableStreet2.getOwner(board));

        /*
         * Ownership and balance after trade.
         */
        Board.trade(board, player, player2,
                Collections.singletonList(purchasableStreet), Collections.singletonList(purchasableStreet2), 75, 100);
        assertEquals(player2, purchasableStreet.getOwner(board));
        assertEquals(player, purchasableStreet2.getOwner(board));
        assertEquals(1465, player.getPlayerBalance());
        assertEquals(1275, player2.getPlayerBalance());
    }

    @Test
    void decreaseJailCounter() {
        assertFalse(player.isInJail());
        assertEquals(0, player.getJailCounter());
        player.enterJail(board, false);
        assertTrue(player.isInJail());
        assertEquals(3, player.getJailCounter());
        player.decreaseJailCounter();
        player.decreaseJailCounter();
        assertTrue(player.isInJail());
        assertEquals(1, player.getJailCounter());
        player.decreaseJailCounter();
        assertFalse(player.isInJail());
        assertEquals(0, player.getJailCounter());
    }

    @Test
    void getPieceTypeAndColor() {
        assertNull(player.getPieceType());
        player.setPieceType(PieceType.AIRPLANE);
        player.setPieceColor(PieceColor.BLUE);
        assertEquals(PieceType.AIRPLANE, player.getPieceType());
        assertEquals(PieceColor.BLUE, player.getPieceColor());
        assertEquals("#90caf9", player.getPieceColor().toString());
    }

    @Test
    void setPieceType() {
        player.setPieceType(PieceType.AIRPLANE);
        assertEquals(PieceType.AIRPLANE, player.getPieceType());
    }
}

package ee.taltech.monopoly.gamelogic.street.nonpurchasable;

import ee.taltech.monopoly.gamelogic.Board;
import ee.taltech.monopoly.gamelogic.Player;
import ee.taltech.monopoly.gamelogic.street.StreetGroup;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MockNonPurchasableStreetTest {
    NonPurchasableStreet nonPurchasableStreet = new MockNonPurchasableStreet();
    NonPurchasableStreet nonPurchasableStreet2 = new MockNonPurchasableStreet();
    Board board = new Board();
    Player player = new Player("Name", "id", board);


    @Test
    void getStreetGroup() {
        assertEquals(StreetGroup.OTHER, nonPurchasableStreet.getStreetGroup());
    }

    @Test
    void getName() {
        assertNull(nonPurchasableStreet.getName());
        assertNull(nonPurchasableStreet2.getName());
    }

    @SuppressWarnings("SimplifiableJUnitAssertion")
    @Test
    void testEqualsAndHash() {
        NonPurchasableStreet nonPurchasableStreetX = new TaxStreet("Name", true, 4);
        NonPurchasableStreet nonPurchasableStreetY = new TaxStreet("Name", true, 4);
        assertTrue(nonPurchasableStreetX.equals(nonPurchasableStreetY)
                && nonPurchasableStreetY.equals(nonPurchasableStreetX));
        assertTrue(nonPurchasableStreetX.hashCode() == nonPurchasableStreetY.hashCode());
        assertFalse(nonPurchasableStreet.equals(nonPurchasableStreetX));
        assertFalse(nonPurchasableStreet.equals(null));
    }

    @Test
    void toStringTest() {
        assertEquals("(0) null", nonPurchasableStreet.toString());
    }
}

package ee.taltech.monopoly.ai;

import ee.taltech.monopoly.gamelogic.Board;
import ee.taltech.monopoly.gamelogic.Player;
import ee.taltech.monopoly.network.requests.TradeRequest;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class AIPlayerTest {

    @Test
    void playTurn() {
        final Board board = new Board("gameID");
        final AIPlayer aiPlayer = new AIPlayer("name", "ai-id", board);
        final Player humanPlayer = new Player("name", "human-player-id", board);
        board.setAllPlayers(List.of(aiPlayer, humanPlayer));
        board.setCurrentPlayer(aiPlayer);
        aiPlayer.playTurn(board);
        // assert that the turn has been given to next player
        assertNotEquals(board.getCurrentPlayer(), aiPlayer);
    }

    @Test
    void considerTrade() {
        final Board board = new Board("gameID");
        final AIPlayer aiPlayer = new AIPlayer("name", "ai-id", board);
        final Player humanPlayer = new Player("name", "human-player-id", board);
        // human wants to give 50 and get 100
        final boolean decision = aiPlayer.considerTrade(new TradeRequest("gameId", humanPlayer, aiPlayer,
                new HashMap<String, Integer>(),
                new HashMap<String, Integer>(),
                50,
                100
        ), board);
        // AI declines
        assertFalse(decision);
    }
}

package com.example.test.spring.ui;

import com.example.test.spring.ChatMessage;
import com.example.test.spring.MessageList;
import com.example.test.spring.gamelogic.Player;
import com.example.test.spring.gamelogic.State;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.PWA;

import org.springframework.beans.factory.annotation.Autowired;
import reactor.core.publisher.Flux;
import reactor.core.publisher.UnicastProcessor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Route("")
@StyleSheet("frontend://styles/styles.css")
@Push
@PWA(name = "Vaadin Chat", shortName = "Chat")
public class MainView extends VerticalLayout {

    private final State state;

    private final UnicastProcessor<Boolean> joinGamePublisher;
    private final Flux<Boolean> joinGameFlux;

    private final UnicastProcessor<Integer> startGamePublisher;
    private final Flux<Integer> startGameFlux;
    private final UnicastProcessor<ChatMessage> messagePublisher;
    private final Flux<ChatMessage> messagesFlux;

    private String username;

    private final List<StreetComponent> streets = Collections.synchronizedList(new ArrayList<StreetComponent>());

    public MainView(
            @Autowired State state,
            @Autowired UnicastProcessor<Boolean> joinGamePublisher,
            @Autowired Flux<Boolean> joinGameFlux,
            @Autowired UnicastProcessor<Integer> startGamePublisher,
            @Autowired Flux<Integer> startGameFlux,
            @Autowired UnicastProcessor<ChatMessage> messagePublisher,
            @Autowired Flux<ChatMessage> messagesFlux
    ) {
        this.state = state;
        this.joinGamePublisher = joinGamePublisher;
        this.joinGameFlux = joinGameFlux;
        this.startGamePublisher = startGamePublisher;
        this.startGameFlux = startGameFlux;
        this.messagePublisher = messagePublisher;
        this.messagesFlux = messagesFlux;
        addClassName("main-view");
        setSizeFull();
        setDefaultHorizontalComponentAlignment(Alignment.CENTER);
        H1 header = new H1("Monoploy");
        header.getElement().getThemeList().add("dark");
        add(header);

        showLoginScreen();

    }

    private void showLoginScreen() {
        HorizontalLayout loginForm = new HorizontalLayout();

        TextField usernameField = new TextField();
        Button startButton = new Button("Join game");

        startButton.addClickListener(click -> {
            username = usernameField.getValue();
            final Player player = new Player(username);
            state.addPlayer(player);
            joinGamePublisher.onNext(true);
            remove(loginForm);
            showLobby();
        });

        loginForm.add(usernameField, startButton);
        add(loginForm);
    }

    private void showLobby() {
        final VerticalLayout lobbyLayout = new VerticalLayout();
        final VerticalLayout playerNames = new VerticalLayout();
        lobbyLayout.add(new Button("Start game", (event) -> {startGamePublisher.onNext(0);}), playerNames);
        add(lobbyLayout);
        state.getAllPlayers().forEach(player -> playerNames.add(new Paragraph(player.getName())));
        state.getAllPlayers().forEach(player -> playerNames.add(new Paragraph(player.getName())));
        joinGameFlux.subscribe(p -> {
            getUI().ifPresent(ui -> ui.access(
                    () -> {
                        playerNames.removeAll();
                        state.getAllPlayers().forEach(player -> playerNames.add(new Paragraph(player.getName())));
                    }));
        });
        startGameFlux.subscribe(i -> {
            remove(lobbyLayout);
            showGameBoard();
        });
    }

    private void showGameBoard() {
        add(new H2("Gameboard"));
        // Board
        final HorizontalLayout gameView = new HorizontalLayout();
        add(gameView);
        final VerticalLayout board = new VerticalLayout();
        gameView.add(board);
        final HorizontalLayout row1 = new HorizontalLayout();
        board.add(row1);
        for (int i = 0; i< 11; i++) {
            final StreetComponent street = new StreetComponent(21+i);
            street.getStyle().set("color", "red");
            streets.add(street);
            row1.add(street);
        }

        for (int j = 0; j< 9; j++) {
            final HorizontalLayout row = new HorizontalLayout();
            board.add(row);
            final StreetComponent street1 = new StreetComponent(20-j);
            streets.add(street1);
            row.add(street1);

            for (int i = 0; i< 9; i++) {
                row.add(new Span("-"));
            }
            final StreetComponent street2 = new StreetComponent(31+j);
            streets.add(street2);
            row.add(street2);
        }
        final HorizontalLayout row11 = new HorizontalLayout();
        board.add(row11);
        for (int i = 0; i< 11; i++) {
            final StreetComponent street = new StreetComponent(11-i);
            streets.add(street);
            row11.add(street);
        }

        // Chat
        MessageList messageList = new MessageList();

        gameView.add(messageList, createInputLayout());

        messagesFlux.subscribe(m -> {
            getUI().ifPresent(ui -> ui.access(
                    () -> messageList.add(new Paragraph(m.getFrom() + ": " + m.getMessage()))));
        });

        expand(messageList);
    }


    private Component createInputLayout() {
        HorizontalLayout layout = new HorizontalLayout();
        layout.setWidth("100%");

        TextField messageField = new TextField();
        Button send = new Button("Send");
        send.setThemeName(ButtonVariant.LUMO_PRIMARY.getVariantName());
        send.addClickShortcut(Key.ENTER);

        send.addClickListener(click -> {
            //....
            messagePublisher.onNext(new ChatMessage(username, messageField.getValue()));

            messageField.clear();
            messageField.focus();
        });
        messageField.focus();

        layout.add(messageField, send);
        layout.expand(messageField);
        return layout;
    }

}
